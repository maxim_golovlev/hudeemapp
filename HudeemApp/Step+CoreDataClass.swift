//
//  Step+CoreDataClass.swift
//  HudeemApp
//
//  Created by Admin on 26.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData


public class Step: NSManagedObject {

    static func create(withDict dict: [String: AnyObject], context: NSManagedObjectContext) -> Step {
        
        let step: Step = Step.createOrReturn(withId: dict["id"] as? Int, context: context)
        step.initWithDict(dict: dict, context: context)
        
        return step
    }
    
    private func initWithDict(dict: [String: AnyObject], context: NSManagedObjectContext) {
        
        self.text = dict["text"] as? String
        self.imageUrl = dict["image"] as? String
        
        if let id = dict["id"] as? Int32 {
            self.id = id
        }
        
        NSManagedObject.attemptToSave(context: context)
    }
    
}
