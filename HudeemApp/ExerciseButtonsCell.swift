//
//  ExerciseDoneCell.swift
//  HudeemApp
//
//  Created by Admin on 27.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import TableKit

class ExerciseButtonsCell : BaseTableCell, ConfigurableCell {
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let separator: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.8431372549, green: 0.8431372549, blue: 0.8431372549, alpha: 1)
        return view
    }()
    
    lazy var doneButton: RoundedButton = {
        let button = RoundedButton(type: .system)
        button.backgroundColor = purpleColor
        button.setTitle("Да", for: .normal)
        button.titleLabel?.font = AppFont.medium(size: 12)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(doneHandler), for: .touchUpInside)
        return button
    }()
    
    lazy var notDoneButton: RoundedButton = {
        let button = RoundedButton(type: .system)
        button.backgroundColor = turquoise
        button.setTitle("Нет", for: .normal)
        button.titleLabel?.font = AppFont.medium(size: 12)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(notDoneHandler), for: .touchUpInside)
        return button
    }()
    
    
    let buttonsHorizontalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fillProportionally
        stackView.spacing = 8
        return stackView
    }()
    
    static var estimatedHeight: CGFloat? {
        return 20
    }
    
    static var defaultHeight: CGFloat? {
        return UITableViewAutomaticDimension
    }
    
    func configure(with data: (title: String?, font: UIFont)) {
        titleLabel.text = data.title
        titleLabel.font = data.font
        
    }
    
    override func setupViews() {
        
        addSubview(titleLabel)
        titleLabel.anchor(topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 22, leftConstant: 23, bottomConstant: 0, rightConstant: 23, widthConstant: 0, heightConstant: 0)
        
        addSubview(buttonsHorizontalStackView)
        buttonsHorizontalStackView.anchor(titleLabel.bottomAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 20, leftConstant: 23, bottomConstant: 24, rightConstant: 100, widthConstant: 0, heightConstant: 37)
        
        buttonsHorizontalStackView.addArrangedSubview(doneButton)
        buttonsHorizontalStackView.addArrangedSubview(notDoneButton)
        
        addSubview(separator)
        separator.anchor(nil, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: Screen.pixel)
    }
    
    func doneHandler() {
        TableCellAction(key: TrainCellActions.doneBtnClicked, sender: self, userInfo: ["status": CompleteStatus.yes]).invoke()
    }
    
    func notDoneHandler() {
        TableCellAction(key: TrainCellActions.doneBtnClicked, sender: self, userInfo: ["status": CompleteStatus.no]).invoke()
    }
}

