//
//  Training+CoreDataProperties.swift
//  HudeemApp
//
//  Created by Admin on 23.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData


extension Training {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Training> {
        return NSFetchRequest<Training>(entityName: "Training")
    }

    @NSManaged public var title: String?
    @NSManaged public var id: Int32
    @NSManaged public var sort: Int32
    @NSManaged public var plan: TrainingPlan?
    @NSManaged public var exrcises: NSSet?

}

// MARK: Generated accessors for exrcises
extension Training {

    @objc(addExrcisesObject:)
    @NSManaged public func addToExrcises(_ value: Exercise)

    @objc(removeExrcisesObject:)
    @NSManaged public func removeFromExrcises(_ value: Exercise)

    @objc(addExrcises:)
    @NSManaged public func addToExrcises(_ values: NSSet)

    @objc(removeExrcises:)
    @NSManaged public func removeFromExrcises(_ values: NSSet)

}
