//
//  Meal+CoreDataProperties.swift
//  HudeemApp
//
//  Created by Admin on 10.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData


extension Meal {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Meal> {
        return NSFetchRequest<Meal>(entityName: "Meal")
    }

    @NSManaged public var id: Int32
    @NSManaged public var title: String?
    @NSManaged public var sort: Int32
    @NSManaged public var dishes: NSSet?
    @NSManaged public var mealPlan: MealPlan?

}

// MARK: Generated accessors for dishes
extension Meal {

    @objc(addDishesObject:)
    @NSManaged public func addToDishes(_ value: Dish)

    @objc(removeDishesObject:)
    @NSManaged public func removeFromDishes(_ value: Dish)

    @objc(addDishes:)
    @NSManaged public func addToDishes(_ values: NSSet)

    @objc(removeDishes:)
    @NSManaged public func removeFromDishes(_ values: NSSet)

}
