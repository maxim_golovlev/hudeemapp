//
//  DishButtonsDefaultCell.swift
//  HudeemApp
//
//  Created by Admin on 03.05.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import TableKit

class DishButtonsDefaultCell: BaseTableCell, ConfigurableCell {
    
    lazy var eatenButton: RoundedButton = {
        let button = RoundedButton(type: .system)
        button.backgroundColor = purpleColor
        button.setTitle("Я это уже съела", for: .normal)
        button.titleLabel?.font = DishCellSizes.buttonTitleFont
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(eatenHandler), for: .touchUpInside)
        return button
    }()
    
    lazy var notEatenButton: RoundedButton = {
        let button = RoundedButton(type: .system)
        button.backgroundColor = turquoise
        button.setTitle("Я это не съела", for: .normal)
        button.titleLabel?.font = DishCellSizes.buttonTitleFont
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(notEatenHandler), for: .touchUpInside)
        return button
    }()
    
    let buttonsHorizontalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fillProportionally
        stackView.spacing = 8
        return stackView
    }()
    
    static var estimatedHeight: CGFloat? {
        return 20
    }
    
    static var defaultHeight: CGFloat? {
        return UITableViewAutomaticDimension
    }
    
    
    func configure(with data:(leftTitle: String?, rightTitle: String?)) {
        self.eatenButton.setTitle(data.leftTitle, for: .normal)
        self.notEatenButton.setTitle(data.rightTitle, for: .normal)
    }
    
    override func setupViews() {
        
        addSubview(buttonsHorizontalStackView)
        buttonsHorizontalStackView.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 10, leftConstant: 18, bottomConstant: 25, rightConstant: 36, widthConstant: 0, heightConstant: 37)
        
        buttonsHorizontalStackView.addArrangedSubview(eatenButton)
        buttonsHorizontalStackView.addArrangedSubview(notEatenButton)
    }
    
    func eatenHandler() {
        TableCellAction(key: DishCellActions.rateBtnClicked, sender: self, userInfo: ["status": CompleteStatus.yes]).invoke()
    }
    
    func notEatenHandler() {
        TableCellAction(key: DishCellActions.rateBtnClicked, sender: self, userInfo: ["status": CompleteStatus.no]).invoke()
    }
    
}
