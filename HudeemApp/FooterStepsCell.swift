//
//  FooterStepsCell.swift
//  HudeemApp
//
//  Created by Admin on 16.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class FooterCookingCell: BaseCollectionCell {
    
    static let title = "Вкусно?"
    static let subtitle = "Поделитесь с друзьями рецептом"
    
    let imageView: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "heart_icon"))
        return imageView
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = AppFont.bold(size: 15)
        label.textColor = purpleColor
        label.text = title
        return label
    }()
    
    let subTitleLabel: UILabel = {
        let label = UILabel()
        label.font = AppFont.roman(size: 15)
        label.text = subtitle
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    weak var shareDelegate: Shareable?
    
    lazy var fbButton: UIButton = {
        let button = UIButton(type: .system)
        button.setBackgroundImage(#imageLiteral(resourceName: "Fb_icon"), for: .normal)
        button.tag = 0
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return button
    }()
    
    lazy var vkButton: UIButton = {
        let button = UIButton(type: .system)
        button.setBackgroundImage(#imageLiteral(resourceName: "Vk_icon"), for: .normal)
        button.tag = 1
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return button
    }()
    
    lazy var instButton: UIButton = {
        let button = UIButton(type: .system)
        button.setBackgroundImage(#imageLiteral(resourceName: "Instagram_icon"), for: .normal)
        button.tag = 2
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return button
    }()
    
    lazy var odnButton: UIButton = {
        let button = UIButton(type: .system)
        button.setBackgroundImage(#imageLiteral(resourceName: "odnoklassniki_icon"), for: .normal)
        button.tag = 3
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return button
    }()
    
    lazy var whatsButton: UIButton = {
        let button = UIButton(type: .system)
        button.setBackgroundImage(#imageLiteral(resourceName: "WhatsApp_icon"), for: .normal)
        button.tag = 4
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return button
    }()
    
    lazy var twitButton: UIButton = {
        let button = UIButton(type: .system)
        button.setBackgroundImage(#imageLiteral(resourceName: "twitter_icon"), for: .normal)
        button.tag = 5
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return button
    }()
    
    let horizontalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.spacing = 8
        return stackView
    }()
    
    override func setupViews() {
        
        addSubview(imageView)
        imageView.anchor(topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: CookingStepCell.circleLeft, bottomConstant: 0, rightConstant: 0, widthConstant: CookingStepCell.circleHeight, heightConstant: CookingStepCell.circleHeight)
        
        addSubview(titleLabel)
        titleLabel.anchor(topAnchor, left: imageView.rightAnchor, bottom: nil, right: rightAnchor, topConstant: CookingStepCell.titleTop, leftConstant: CookingStepCell.titleLeft, bottomConstant: 0, rightConstant: CookingStepCell.titleRight, widthConstant: 0, heightConstant: 0)
        
        addSubview(subTitleLabel)
        subTitleLabel.anchor(titleLabel.bottomAnchor, left: titleLabel.leftAnchor, bottom: nil, right: titleLabel.rightAnchor, topConstant: 10, leftConstant:0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        addSubview(horizontalStackView)
        horizontalStackView.anchor(subTitleLabel.bottomAnchor, left: imageView.rightAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 6, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 34)
        
        horizontalStackView.rightAnchor.constraint(lessThanOrEqualTo: rightAnchor).isActive = true
        
        horizontalStackView.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor, constant: 16).isActive = true
        
        horizontalStackView.addArrangedSubview(fbButton)
        horizontalStackView.addArrangedSubview(vkButton)
        horizontalStackView.addArrangedSubview(instButton)
        horizontalStackView.addArrangedSubview(odnButton)
        horizontalStackView.addArrangedSubview(whatsButton)
        horizontalStackView.addArrangedSubview(twitButton)
    }
    
    static func getHeight() -> CGFloat {
        
        let textWidth = UIScreen.main.bounds.width - CookingStepCell.circleLeft - CookingStepCell.circleHeight - CookingStepCell.titleLeft - CookingStepCell.titleRight
        let textSize = CGSize(width: textWidth, height: 1000)
        
        let footerTitleHeight = String.estimageTextSize(text: FooterCookingCell.title,
                                                        size: textSize,
                                                        font: AppFont.bold(size: 15))?.height ?? 0
        let footerSubtitleHeight = String.estimageTextSize(text: FooterCookingCell.subtitle,
                                                           size: textSize,
                                                           font: AppFont.roman(size: 15))?.height ?? 0
        let iconHeight: CGFloat = 34
        let footerHeight: CGFloat = CookingStepCell.titleTop + footerTitleHeight + 10 + footerSubtitleHeight + 10 + iconHeight + 16
        
        return footerHeight
    }

    func buttonTapped(sender: UIButton) {
        
        self.shareDelegate?.share(textToShare: nil)
    }
}
