//
//  ApiSrvice.swift
//  HudeemApp
//
//  Created by Admin on 25.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import CoreData

class ApiService {
    
    let authUrl = "http://www.hudeem-s-profi.ru/api/login"
    let dietPlanUrl = "http://www.hudeem-s-profi.ru/api/get_diet_plan"
    let recieptUrl = "http://www.hudeem-s-profi.ru/api/get_recipe"
    let completeDishUrl = "http://www.hudeem-s-profi.ru/api/set_complete_dish"
    let trainingsPlanUrl = "http://www.hudeem-s-profi.ru/api/get_training_plan?developer_mode=1"
    
    static let sharedService = ApiService()
    
    func login(withEmail email: String?, password: String?, completion: @escaping (User?, Error?) -> ()) {
        
        guard let email = email, let password = password else {
            completion(nil, ResponseError.withMessage("auth params incorrect"))
            return
        }
        
        if let url = URL(string: authUrl) {
            
            let postString = "email=\(email)&password=\(password)"
            
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.httpBody = postString.data(using: .utf8)
            
            URLSession.shared.dataTask(with: request, completionHandler: { (data, responce, error) in

                let userData = User.createUser(withData: data)
                completion(userData.user, userData.error)
                
            }).resume()
        }
    }
    
    func fetchDietPlan(forSelectedDate date: Date?, completion: @escaping (MealPlan?, NSManagedObjectContext?, Error?) -> ()) {
        
        guard let date = date?.stringFromDate(), let currentUser = User.currentUser(), let token = currentUser.token else {
            completion(nil, nil, ResponseError.withMessage("you are not autorised"))
            return
        }
        
        let userId = currentUser.id
        
        if let url = URL(string: dietPlanUrl) {
            
            let postString = "user_id=\(userId)&token=\(token)&day=\(date)"
            
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.httpBody = postString.data(using: .utf8)
            
            URLSession.shared.dataTask(with: request, completionHandler: { (data, responce, error) in
                
                AppDelegate.appPersistentContainer.performBackgroundTask({ (context) in
                    let mealPlan = MealPlan.createMealPlan(withData: data, context: context)
                    
                    NSManagedObject.attemptToSave()
                    completion(mealPlan, context, error)
                })
                
            }).resume()
        }
    }
    
    func fetchReciept(forDishId dishId: Int32?, planId: Int32?, completion: @escaping (Reciept?, Error?) -> ()) {
        
        guard let dishId = dishId, let planId = planId, let currentUser = User.currentUser(), let token = currentUser.token else { return }
        
        let userId = currentUser.id
        
        if let url = URL(string: recieptUrl) {
            
            let postString = "user_id=\(userId)&token=\(token)&dish_id=\(dishId)&plan_id=\(planId)"
            
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.httpBody = postString.data(using: .utf8)
            
            URLSession.shared.dataTask(with: request, completionHandler: { (data, responce, error) in

                AppDelegate.appPersistentContainer.performBackgroundTask({ (context) in
                    let reciept = Reciept.create(withData: data, context: context)
                    
                    NSManagedObject.attemptToSave()
                    
                    completion(reciept, error)
                })
                
            }).resume()
        }
    }
    
    
    func postSendStatus(status: CompleteStatus, planId: Int32?, comment: String? = nil, completion: @escaping (Error?) -> ()) {
        
        guard let planId = planId, let currentUser = User.currentUser(), let token = currentUser.token else { return }
        
        let userId = currentUser.id
        
        if let url = URL(string: completeDishUrl) {
            
            let postString = "user_id=\(userId)&token=\(token)&plan_id=\(planId)&complete=\(status)&comment=\(comment ?? "")"
            
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.httpBody = postString.data(using: .utf8)
            
            URLSession.shared.dataTask(with: request, completionHandler: { (data, responce, error) in
                
                completion(error)
                
            }).resume()
        }
    }
    
    func fetchTrainingPlan(forSelectedDate date: Date?, completion: @escaping (TrainingPlan?, NSManagedObjectContext?, Error?) -> ()) {
        
        guard let date = date?.stringFromDate(), let currentUser = User.currentUser(), let token = currentUser.token else {
            completion(nil, nil, ResponseError.withMessage("you are not autorised"))
            return
        }
        
        let userId = currentUser.id
        
        if let url = URL(string: trainingsPlanUrl) {
            
            let postString = "user_id=\(userId)&token=\(token)&day=\(date)"
            
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.httpBody = postString.data(using: .utf8)
            
            URLSession.shared.dataTask(with: request, completionHandler: { (data, responce, error) in
                
                AppDelegate.appPersistentContainer.performBackgroundTask({ (context) in
                    let trainingPlan = TrainingPlan.create(withData: data, context: context)
                    completion(trainingPlan, context, error)
                })
                
            }).resume()
        }
    }
}

enum CompleteStatus: String {
    case yes = "yes"
    case no = "no"
    case none = "default"
    
    static func from(string: String?) -> CompleteStatus {
    
        guard let string = string else { return .none }
        
        switch string {
        case "yes":
            return .yes
        case "no":
            return .no
        default:
            return .none
        }
    }
    
}
