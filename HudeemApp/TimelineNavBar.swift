//
//  TimelineNavBar.swift
//  HudeemApp
//
//  Created by Admin on 30.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

protocol TimelineNavBarDeleagte: class {
    func didSelectetItem(atIndex index: Int, withDate date: Date)
}

class TimelineNavBar: CustomView {
    
    let cellId = "cellId"
    
    var days = [Date]()
    
    weak var delegate: TimelineNavBarDeleagte?
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.register(CalendarCell.self, forCellWithReuseIdentifier: self.cellId)
        cv.dataSource = self
        cv.delegate = self
        cv.backgroundColor = .white
        cv.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        cv.bounces = false
        cv.showsHorizontalScrollIndicator = false
        return cv
    }()
    
    
    override func setupViews() {
        
        
        initDaysArray()
        
        addSubview(collectionView)
        collectionView.fillSuperview()
        
        collectionView.performBatchUpdates(nil) { (finished) in
            
            self.scrollToStartingPoint()
            
        }
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
    }
    
    func initDaysArray() {
     
        let calendar = Calendar.current
        var date = Date().startOfMonth()
        
        // Calculate start and end of the current year (or month with `.month`):
        let interval = calendar.dateInterval(of: .month, for: date)!
        
        // Compute difference in days:
        if let count = calendar.dateComponents([.day], from: interval.start, to: interval.end).day {
            for _ in 0...(count - 1) {
                days.append(date)
                date = calendar.date(byAdding: .day, value: 1, to: date)!
            }
        }
    }
    
    func scrollToStartingPoint() {
        let date = Calendar.current.startOfDay(for: Date())
        if let currentIndex = days.index(of: date) {
            let indexPath = IndexPath(row: currentIndex, section: 0)
            
            collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .right)
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            
            self.delegate?.didSelectetItem(atIndex: indexPath.item, withDate: date)
        }
    }
}

extension TimelineNavBar: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return days.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! CalendarCell
        cell.date = days[indexPath.item]
        
        return cell
    }
}

extension TimelineNavBar: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
        let date = days[indexPath.item]
        self.delegate?.didSelectetItem(atIndex: indexPath.item, withDate: date)
    }    
}

extension TimelineNavBar: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 60, height: 80)
    }
}

