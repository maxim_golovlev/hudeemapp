//
//  Ingredient+CoreDataProperties.swift
//  HudeemApp
//
//  Created by Admin on 07.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData


extension Ingredient {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Ingredient> {
        return NSFetchRequest<Ingredient>(entityName: "Ingredient")
    }

    @NSManaged public var id: Int32
    @NSManaged public var photoUrl: String?
    @NSManaged public var size: Int32
    @NSManaged public var title: String?
    @NSManaged public var receipt: Reciept?

}
