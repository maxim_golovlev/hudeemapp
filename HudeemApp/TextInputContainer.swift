//
//  TextInputContainer.swift
//  ElvisProject
//
//  Created by Admin on 08.02.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class MessageContainer: UIView {
    
    var mediaSelector: Selector?
    var sendSelector: Selector?
    weak var target: AnyObject?
    
    lazy var imputTextField:UITextField = {
        let textFiled = UITextField()
        textFiled.delegate = self
        return textFiled
    }()
    
    lazy var sendButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Send", for: UIControlState())
        let titleColor = UIColor(red: 0, green: 137/255, blue: 249/255, alpha: 1)
        button.setTitleColor(titleColor, for: UIControlState())
        button.titleLabel?.font = .boldSystemFont(ofSize: 16)
        
        if let sendSelector = self.sendSelector {
          button.addTarget(self.target, action: sendSelector, for: .touchUpInside)
        }
        return button
    }()
    
    let borderLine: UIView = {
        let view = UIView()
        view.backgroundColor = .lightGray
        return view
    }()
    
    lazy var sendMediaButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "metal_clip").withRenderingMode(.alwaysOriginal), for: .normal)
        
        if let mediaSelector = self.mediaSelector {
            button.addTarget(self.target, action: mediaSelector, for: .touchUpInside)
        }
        
        return button
    }()
    
    init(sendSelector: Selector?, mediaSelector: Selector?, target: AnyObject?) {
        
        self.mediaSelector = mediaSelector
        self.sendSelector = sendSelector
        self.target = target
        
        super.init(frame: .zero)
    
        setupViews()
    }
    
    func setupViews() {
        backgroundColor = .white
        
        addSubview(sendMediaButton)
        addConstraintsWithFormat("V:[v0(32)]", views: sendMediaButton)
        sendMediaButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        addSubview(borderLine)
        addConstraintsWithFormat("H:|[v0]|", views: borderLine)
        addConstraintsWithFormat("V:|[v0(1)]", views: borderLine)
        
        addSubview(sendButton)
        addConstraintsWithFormat("V:|[v0]|", views: sendButton)
        
        addSubview(imputTextField)
        addConstraintsWithFormat("H:|-8-[v0(32)]-8-[v1][v2(48)]|", views: sendMediaButton, imputTextField, sendButton)
        addConstraintsWithFormat("V:|[v0]|", views: imputTextField)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension MessageContainer: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
     //   chatViewController?.handleMessageSend()
        return true
    }
}
