//
//  TrainController.swift
//  HudeemApp
//
//  Created by Admin on 21.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import TableKit
import CoreData

class TrainController: CenterController {

    lazy var timelineNavBar: TimelineNavBar = {
        let bar = TimelineNavBar()
        bar.delegate = self
        return bar
    }()
    
    var currentTrainingPlanId: Int?
    var currentExerciseId: Int?
    var currentContext: NSManagedObjectContext?
    var currentDate: Date?
    var currentSelectedDayIndex: Int?
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .white
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.allowsSelection = false
        self.tableDirector = TableDirector(tableView: tableView)
        tableView.contentInset = UIEdgeInsetsMake(-15, 0, 0, 0)
        return tableView
    }()
    
    let errorLabel: UILabel = {
        let label = UILabel()
        label.font = AppFont.medium(size: 18)
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    var trainings: [Training]? {
        
        let predicate = NSPredicate(format: "plan.id = %d", self.currentTrainingPlanId ?? 0)
        let sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        
        let trainings: [Training]? = Training.fetch(predicate: predicate, sortDescriptors: sortDescriptors, context: self.currentContext ?? AppDelegate.currentContext)
        
        return trainings
    }
    
    var tableDirector: TableDirector?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(timelineNavBar)
        timelineNavBar.anchor(topLayoutGuide.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 80)
        
        view.addSubview(errorLabel)
        errorLabel.anchor(nil, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 0)
        errorLabel.anchorCenterYToSuperview()
        
        view.addSubview(tableView)
        tableView.anchor(timelineNavBar.bottomAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
    }
    
    func updateTableView() {
        
        guard let trainings = self.trainings else { return }
        
        tableDirector?.clear()
        
        for training in trainings {
            
            let section = TableSection()
            
            let titleHeader = TableRow<SelfSizedCell>(item: (title: training.title, font: AppFont.medium(size: 22), topInset: 30, botInset: 30, withSeparator: true))
            
            section.append(row: titleHeader)
            
            let showTechnic = TableRowAction<ExerciseCell>(.custom(TrainCellActions.openTecnicClicked)) { (cellOption) -> Void in
                
                  if let _ = cellOption.cell, let userInfo = cellOption.userInfo, let id = Array(userInfo.values).first as? Int32  {
                    
                    let vc = TechnicController()
                        vc.exerciseId = id
                        self.navigationController?.pushViewController(vc, animated: true)
                 }
            }

            
            if let exercises = training.getOrderedExercises(context: self.currentContext) {
                
                for exercise in exercises {
                    
                    let exerciseRow = TableRow<ExerciseCell>(item: exercise.id, actions: [showTechnic])
                    section.append(row: exerciseRow)
                    
                    if exercise.pauseTime > 0 {
                        let pauseRow = TableRow<BreakTimeCell>(item: (min: Int(exercise.pauseTime), sec: Int(exercise.pauseTimeSec)))
                        section.append(row: pauseRow)
                    }
                    
                    if let params = exercise.getOrderedParams(context: self.currentContext), params.count > 0 {
                        
                        let paramsTitle = params[0].getTitleHeader()
                        
                        let paramsTite = TableRow<ParamSectionCell>(item: (title: "Подход", subtitle: paramsTitle, font: AppFont.medium(size: 15), backColor: .white))
                        section.append(row: paramsTite)
                        
                        for (i, param) in params.enumerated() {
                            
                            let color = i % 2 == 0 ? #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1) : .white
                            
                            let paramsTite = TableRow<ParamSectionCell>(item: (title: "\(i*1 + 1)-й подход", subtitle: param.getSubtitle(), font: AppFont.medium(size: 15), backColor: color))
                            section.append(row: paramsTite)
                            
                        }
                    }
                    
                    if let completeString = exercise.complete, let comment = exercise.comment {
                    
                        let complete = CompleteStatus.from(string: completeString)
                        
                        let buttonPressed = TableRowAction<ExerciseButtonsCell>(.custom(TrainCellActions.doneBtnClicked)) { (cellOption) -> Void in
                            
                          /*  if let cell = cellOption.cell, let userInfo = cellOption.userInfo, let status = Array(userInfo.values).first as? CompleteStatus  {
                            }*/
                        }
                        
                        switch complete {
                        case .no:
                            let doneRow = TableRow<ExerciseResultCell>(item: (title: "Сделано полностью", subtitle: comment, picture: #imageLiteral(resourceName: "green_checkmark")))
                            section.append(row: doneRow)
                        case .yes:
                            let doneRow = TableRow<ExerciseResultCell>(item: (title: "Сделано не полностью :(", subtitle: comment, picture: #imageLiteral(resourceName: "abort_checkmark")))
                            section.append(row: doneRow)
                        default:
                            let doneRow = TableRow<ExerciseButtonsCell>(item: (title: "Сделано полностью?", font: AppFont.medium(size: 17)), actions: [buttonPressed])
                            section.append(row: doneRow)
                        }
                    }
                }
            }
            
            tableDirector?.append(section: section)
        }

        if trainings.count > 0 {
            let section = TableSection()
            let footerRow = TableRow<TrainingPlanFooterCell>(item: (title: "Понравилась тренировка?", subtitle: "Поделитесь с друзьями упражнениями"))
            section.append(row: footerRow)
            tableDirector?.append(section: section)
        }
        
        tableDirector?.reload()
        
        if tableView.numberOfSections > 0 {
            tableView.layoutIfNeeded()
            let indexPath = IndexPath(row: 0, section: 0)
            tableView.scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
}


extension TrainController: TimelineNavBarDeleagte {

    func didSelectetItem(atIndex index: Int, withDate date: Date) {
        
        self.currentDate = date
        
        if index == currentSelectedDayIndex { return }
        
        currentSelectedDayIndex = index
        
        startLoading()
        
        ModelManager.sharedManager.getTrainingPlan(forSelectedDate: date)
            .then { (planId, context) -> Void in
                
                self.currentTrainingPlanId = Int(planId!)
                self.currentContext = context
                
                DispatchQueue.main.async {
                    self.updateTableView()
                }
                
            }
            .always {
                self.stopLoading()
                
            }.catch { error in
                
                self.currentTrainingPlanId = nil
                self.currentContext = nil
                
                DispatchQueue.main.async {
                    self.updateTableView()
                }
                
                if case let ResponseError.withMessage(msg) = error {
                    self.errorLabel.text = msg
                }
        }
    }
}
