//
//  Reciept+CoreDataProperties.swift
//  HudeemApp
//
//  Created by Admin on 09.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData


extension Reciept {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Reciept> {
        return NSFetchRequest<Reciept>(entityName: "Reciept")
    }

    @NSManaged public var activeCookTime: Int32
    @NSManaged public var calories: Int32
    @NSManaged public var carbohydrates: Int32
    @NSManaged public var cookTime: Int32
    @NSManaged public var dishId: Int32
    @NSManaged public var fats: Int32
    @NSManaged public var id: Int32
    @NSManaged public var level: String?
    @NSManaged public var photoUrl: String?
    @NSManaged public var proteins: Int32
    @NSManaged public var size: Int32
    @NSManaged public var title: String?
    @NSManaged public var cookOnDays: Int32
    @NSManaged public var ingredients: NSSet?
    @NSManaged public var steps: NSSet?
    @NSManaged public var tools: NSSet?

}

// MARK: Generated accessors for ingredients
extension Reciept {

    @objc(addIngredientsObject:)
    @NSManaged public func addToIngredients(_ value: Ingredient)

    @objc(removeIngredientsObject:)
    @NSManaged public func removeFromIngredients(_ value: Ingredient)

    @objc(addIngredients:)
    @NSManaged public func addToIngredients(_ values: NSSet)

    @objc(removeIngredients:)
    @NSManaged public func removeFromIngredients(_ values: NSSet)

}

// MARK: Generated accessors for steps
extension Reciept {

    @objc(addStepsObject:)
    @NSManaged public func addToSteps(_ value: Step)

    @objc(removeStepsObject:)
    @NSManaged public func removeFromSteps(_ value: Step)

    @objc(addSteps:)
    @NSManaged public func addToSteps(_ values: NSSet)

    @objc(removeSteps:)
    @NSManaged public func removeFromSteps(_ values: NSSet)

}

// MARK: Generated accessors for tools
extension Reciept {

    @objc(addToolsObject:)
    @NSManaged public func addToTools(_ value: Tool)

    @objc(removeToolsObject:)
    @NSManaged public func removeFromTools(_ value: Tool)

    @objc(addTools:)
    @NSManaged public func addToTools(_ values: NSSet)

    @objc(removeTools:)
    @NSManaged public func removeFromTools(_ values: NSSet)

}
