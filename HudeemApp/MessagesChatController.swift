//
//  MessagesChatController.swift
//  HudeemApp
//
//  Created by Admin on 21.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import CoreData
import MobileCoreServices

class MessagesChatController: UICollectionViewController {

    let cellId = "chatCellId"
    
    var friend: Friend? {
        didSet {
            navigationItem.title = friend?.name
        }
    }
    
    var blockOperations = [BlockOperation]()
    var bottomConstraint: NSLayoutConstraint?
    
    let currentContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    lazy var fetchViewController: NSFetchedResultsController<NSFetchRequestResult> = {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Message")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
        fetchRequest.predicate = NSPredicate(format: "friend.name = %@", self.friend!.name!)
        let fetchController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.currentContext, sectionNameKeyPath: nil, cacheName: nil)
        fetchController.delegate = self
        return fetchController
    }()
    
    let textInputContainer: MessageContainer  = {
        let container = MessageContainer(sendSelector: #selector(handleSend), mediaSelector: #selector(handleMediaUpload), target: self as AnyObject?)
        return container
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        
        do {
            try fetchViewController.performFetch()
            print(fetchViewController.sections![0].numberOfObjects)
        } catch let error {
            print(error)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboard), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboard), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func setupViews() {
        collectionView?.backgroundColor = .white
        collectionView?.alwaysBounceVertical = true
        collectionView?.register(ChatCell.self, forCellWithReuseIdentifier: cellId)
        collectionView?.contentInset = UIEdgeInsetsMake(0, 0, 48 + 8, 0)
        collectionView?.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 48, 0)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Simulate", style: .plain, target: self, action: #selector(simulate))
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_arrow"), style: .plain, target: self, action: #selector(handleBackButton) )
        tabBarController?.tabBar.isHidden = true
        
        view.addSubview(textInputContainer)
        view.addConstraintsWithFormat("H:|[v0]|", views: textInputContainer)
        view.addConstraintsWithFormat("V:[v0(48)]", views: textInputContainer)
        bottomConstraint = NSLayoutConstraint(item: textInputContainer, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
        view.addConstraint(bottomConstraint!)
    }
    
    func handleBackButton() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func handleKeyboard(notification: Notification) {
        
        guard let userInfo = notification.userInfo as? [String: AnyObject] else { return }
        guard let keyboardFrame = userInfo[UIKeyboardFrameEndUserInfoKey]?.cgRectValue else { return }
        let isKeyboardShowing = notification.name == NSNotification.Name.UIKeyboardWillShow
        
        bottomConstraint?.constant = isKeyboardShowing ? -keyboardFrame.height : 0
        
        UIView.animate(withDuration: 0, delay: 0, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
            
        }, completion: { _ in
            if isKeyboardShowing {
                let lastItem = self.fetchViewController.sections![0].numberOfObjects - 1
                let indexPath = IndexPath(item: lastItem, section: 0)
                self.collectionView?.scrollToItem(at: indexPath, at: .bottom, animated: true)
            }
        })
    }
    
    func handleMediaUpload() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.mediaTypes = [kUTTypeImage as String]
        present(imagePicker, animated: true, completion: nil)
    }
    
    func handleSend() {
        
        guard let message = textInputContainer.imputTextField.text, message != "" else { return }
        
        _ = MessagesFeedController.addMessage(withText: message, fromFriend: friend!, minutesAgo: 0, context: currentContext)
        
        do {
            try currentContext.save()
            textInputContainer.imputTextField.text = nil
            
        } catch let error {
            print(error)
        }
    }
    
    func simulate () {
        
        _ = MessagesFeedController.addMessage(withText: "No I am busy right now but thanks", fromFriend: friend!, minutesAgo: 0, context: currentContext, isSender: false)
        _ = MessagesFeedController.addMessage(withText: "See you!", fromFriend: friend!, minutesAgo: 0, context: currentContext, isSender: false)
        
        do {
            try currentContext.save()
            
        } catch let error {
            print(error)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return fetchViewController.sections![0].objects?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ChatCell
        
        let message = fetchViewController.object(at: indexPath) as? Message
        
        cell.message = message
        
        if let estimatedWidth = String.estimageTextSize(text: message?.text)?.width {
            cell.bubleWidthAnchor?.constant = estimatedWidth + 0
            cell.messageImageViewHeightConstraint?.constant = 0
        }
        
        if message?.image != nil {
            cell.bubleWidthAnchor?.constant = 200
            cell.messageImageViewHeightConstraint?.constant = 100
        }
        
        if message?.isSender == true {
            cell.bubleImageView.image = ChatCell.outgoingBubleImage
            cell.bubleLeftAnchor?.isActive = false
            cell.bubleRightAnchor?.isActive = true
            cell.textViewLeftAnchor?.constant = 0
            cell.textViewRightAnchor?.constant = -22
            cell.profileImageRightAnchor?.isActive = true
            cell.profileImageLeftAnchor?.isActive = false
        } else {
            cell.bubleImageView.image = ChatCell.incomingBubleImage
            cell.bubleLeftAnchor?.isActive = true
            cell.bubleRightAnchor?.isActive = false
            cell.textViewLeftAnchor?.constant = 22
            cell.textViewRightAnchor?.constant = 0
            cell.profileImageRightAnchor?.isActive = false
            cell.profileImageLeftAnchor?.isActive = true
        }
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        textInputContainer.imputTextField.endEditing(true)
    }
}

extension MessagesChatController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let message = fetchViewController.object(at: indexPath) as! Message

        if let size = String.estimageTextSize(text: message.text) {
            var size = CGSize(width: view.frame.width, height: size.height + 50 )
            
            if message.image != nil {
                size.height = size.height + 100
            }

            return size
        }
        
        return CGSize(width: view.frame.width, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(8, 0, 0, 0)
    }
}

extension MessagesChatController: NSFetchedResultsControllerDelegate {
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        if type == .insert {
            blockOperations.append(BlockOperation.init(block: { [unowned self] in
                self.collectionView?.insertItems(at: [newIndexPath!])
            }))
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        
        collectionView?.performBatchUpdates({ [unowned self] in
            for operation in self.blockOperations {
                operation.start()
            }
        }, completion: { result in
            let lastItem = self.fetchViewController.sections![0].numberOfObjects - 1
            let indexPath = IndexPath(item: lastItem, section: 0)
            self.collectionView?.scrollToItem(at: indexPath, at: .bottom, animated: true)
        })
    }
}

extension MessagesChatController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if (info[UIImagePickerControllerMediaURL] as? URL) != nil {

        } else {
            handleImageSelected(withInfo: info)
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func handleImageSelected(withInfo info:[String: Any]) {
        
        var selectedImageFromPicker: UIImage?
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            selectedImageFromPicker = editedImage
            
        } else if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            selectedImageFromPicker = originalImage
        }
        
        if selectedImageFromPicker != nil {

        }
    }
}


