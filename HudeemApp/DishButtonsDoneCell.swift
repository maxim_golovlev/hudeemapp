//
//  DishButtonsDoneCell.swift
//  HudeemApp
//
//  Created by Admin on 03.05.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import TableKit


class DishButtonsDoneCell: BaseTableCell, ConfigurableCell {
    
    lazy var doneEatenButton: RoundedButton = {
        let button = RoundedButton(type: .system)
        button.backgroundColor = grayColor
        button.titleLabel?.font = DishCellSizes.buttonTitleFont
        button.setTitleColor(.white, for: .normal)
        return button
    }()
    
    lazy var doneClearButton: RoundedButton = {
        let button = RoundedButton(type: .system)
        button.backgroundColor = .clear
        button.setTitle("Я это не съела", for: .normal)
        button.titleLabel?.font = DishCellSizes.buttonTitleFont
        button.setTitleColor(.clear, for: .normal)
        return button
    }()
    
    let buttonsHorizontalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fillProportionally
        stackView.spacing = 8
        return stackView
    }()
    
    static var estimatedHeight: CGFloat? {
        return 20
    }
    
    static var defaultHeight: CGFloat? {
        return UITableViewAutomaticDimension
    }
    
    
    func configure(with buttonTitle: String?) {        
        self.doneEatenButton.setTitle(buttonTitle, for: .normal)
    }
    
    override func setupViews() {
        
        addSubview(buttonsHorizontalStackView)
        buttonsHorizontalStackView.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 10, leftConstant: 18, bottomConstant: 25, rightConstant: 36, widthConstant: 0, heightConstant: 37)
        
        buttonsHorizontalStackView.addArrangedSubview(doneEatenButton)
        buttonsHorizontalStackView.addArrangedSubview(doneClearButton)
    }
    
}
