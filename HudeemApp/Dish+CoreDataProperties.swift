//
//  Dish+CoreDataProperties.swift
//  HudeemApp
//
//  Created by Admin on 01.05.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData


extension Dish {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Dish> {
        return NSFetchRequest<Dish>(entityName: "Dish")
    }

    @NSManaged public var activeCookTime: Int32
    @NSManaged public var calories: Int32
    @NSManaged public var carbohydrates: Int32
    @NSManaged public var cookOnDays: Int32
    @NSManaged public var cookTime: Int32
    @NSManaged public var fats: Int32
    @NSManaged public var id: Int32
    @NSManaged public var isEaten: String?
    @NSManaged public var level: String?
    @NSManaged public var percCarbohydrates: Float
    @NSManaged public var percFats: Float
    @NSManaged public var percProteins: Float
    @NSManaged public var photoUrl: String?
    @NSManaged public var planId: Int32
    @NSManaged public var proteins: Int32
    @NSManaged public var size: Int32
    @NSManaged public var sizeScale: String?
    @NSManaged public var sort: Int32
    @NSManaged public var title: String?
    @NSManaged public var dishId: Int32
    @NSManaged public var meal: Meal?

}
