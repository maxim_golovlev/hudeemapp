//
//  StoredImage+CoreDataClass.swift
//  HudeemApp
//
//  Created by Admin on 26.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData


public class StoredImage: NSManagedObject {

    static func save(data: Data?, urlString: String?, context: NSManagedObjectContext) {
        
        let imageObject: StoredImage = NSManagedObject.createOrReturn(withId: urlString, context: context)
        if let data = data as NSData? {
            imageObject.imageData = data
        }
        
        imageObject.id = urlString
        
        NSManagedObject.attemptToSave(context: context)
    }
}
