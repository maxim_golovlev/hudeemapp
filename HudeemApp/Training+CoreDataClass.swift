//
//  Training+CoreDataClass.swift
//  HudeemApp
//
//  Created by Admin on 23.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData


public class Training: NSManagedObject {

    static func create(withDict dict: [String: AnyObject], context: NSManagedObjectContext) -> Training {
        
        let training: Training = Training.createOrReturn(withId: dict["block_id"] as? Int, context: context)
        training.initWithDict(dict: dict, context: context)
        
        return training
    }
    
    private func initWithDict(dict: [String: AnyObject], context: NSManagedObjectContext) {
        
        self.title = dict["title"] as? String
        
        if let id = dict["block_id"] as? Int32 {
            self.id = id
        }
        
        if let sort = dict["sort"] as? Int32 {
            self.sort = sort
        }
        
        NSManagedObject.attemptToSave(context: context)
    }
    
    func getOrderedExercises(context: NSManagedObjectContext?) -> [Exercise]? {
        
        guard let context = context else { return nil }
        
        let predicate = NSPredicate(format: "training.id = %d", self.id)
        let sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        
        let exercises: [Exercise]? = Exercise.fetch(predicate: predicate, sortDescriptors: sortDescriptors, context: context)
        
        return exercises
    }
}
