//
//  RecieptController.swift
//  HudeemApp
//
//  Created by Admin on 05.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import CoreData
import TableKit

class RecieptController: UIViewController, RateCellDelegate, Loadable, Alertable, Shareable {
    
    let nutritionCellId = "nutritionCellId"
    let applianceCellId = "applianceCellId"
    let ingridientsCellId = "ingridientsCellId"
    let cookingCellId = "cookingCellId"
    
    let headerHeight: CGFloat = 150
    
    lazy var streachyHeader: CustomImageView = {
        let imageView = CustomImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        
        return imageView
    }()
    
    var currentReciept: Reciept?
    
    var recieptId: Int32? {
        didSet {
            
        }
    }
    
    var reciept: Reciept? {
        
        guard let recieptId = recieptId else {return nil}
        
        let reciept: Reciept? = NSManagedObject.attemptToFetch(withId: Int(recieptId), context: AppDelegate.currentContext)
        return reciept
    }
    
    var data: (dishId: Int32?, planId: Int32?, dishIdForReciept: Int32?) {
        didSet {
            

        }
    }
    
    func fetchRecipet() {
        
        guard let dishId = data.dishIdForReciept, let planId = data.planId else { return }
        
        startLoading()
        
        ModelManager.sharedManager.getReciept(forDishId: dishId, planId: planId)
            .then { (recieptId) -> Void in
        
                let reciept: Reciept? = NSManagedObject.attemptToFetch(withId: Int(recieptId!), context: AppDelegate.currentContext)
        
                self.streachyHeader.loadImageUsingUrlString(reciept?.photoUrl)
                self.recieptId = reciept?.id
                self.currentReciept = reciept
        
                let dish:Dish? = Dish.attemptToFetch(withId: Int(planId), context: AppDelegate.currentContext)
                //self.navigationItem.title = dish?.meal?.title
                self.titleLabel.text = dish?.meal?.title
                
                DispatchQueue.main.async {
                    self.updateTableView()
                }

            }
            .always {
                self.stopLoading()
        
            }.catch { (error) in
                self.showErrorAlert(message: error.localizedDescription)
            }
    }


    var tableDirector: TableDirector?
    
    lazy var tableView: UITableView = {
        
        let tableView = UITableView()
        tableView.backgroundColor = .white
        tableView.allowsSelection = false
       // tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        
        self.tableDirector = TableDirector(tableView: tableView, scrollDelegate: self, shouldUseAutomaticCellRegistration: true, shouldUsePrototypeCellHeightCalculation: false)
        return tableView
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UIApplication.shared.statusBarStyle = .lightContent
        
       // navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
         navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    lazy var backButton: UIButton = {
        let button = UIButton(type: .system)
        button.setBackgroundImage(#imageLiteral(resourceName: "back_arrow"), for: .normal)
        button.addTarget(self, action: #selector(handlePop), for: .touchUpInside)
        return button
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = AppFont.light(size: 16)
        label.textColor = .white
        return label
    }()
    
    let gradientImageView: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "gradient_view"))
        imageView.contentMode = .scaleToFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(tableView)
        tableView.fillSuperview()
        
        view.addSubview(gradientImageView)
        gradientImageView.anchor(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 64)
        
        view.addSubview(backButton)
        backButton.anchor(view.topAnchor, left: view.leftAnchor, bottom: nil, right: nil, topConstant: 36, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 20, heightConstant: 17)
        
        view.addSubview(titleLabel)
        titleLabel.anchorCenterXToSuperview()
        titleLabel.centerYAnchor.constraint(equalTo: backButton.centerYAnchor).isActive = true

        
        updateView()
        
        
        fetchRecipet()
    }
    
    func updateTableView() {
        
        tableDirector?.clear()
        
        let section = TableSection()
        
        let rateDish = TableRowAction<NutritionRecieptCell>(.custom(RecieptCellActions.rateBtnClicked)) { (cellOption) -> Void in
            
            if let cell = cellOption.cell, let dishId = cell.dishId, let planId = cell.planId, let userInfo = cellOption.userInfo, let status = Array(userInfo.values).first as? CompleteStatus  {
                
                self.eatingStatusChanged(status: status, planId: planId, dishId: dishId) { status in
                    cell.toggleEatButtons(status: status == true ? CompleteStatus.yes.rawValue : CompleteStatus.no.rawValue)
                }
            }
        }
        
        let nutritionRow = TableRow<NutritionRecieptCell>.init(item: (planId: data.planId, dishId: data.dishIdForReciept), actions: [rateDish])
        section.append(row: nutritionRow)
        
        let reciept = self.reciept
        
        if let tools = reciept?.tools, tools.count > 0 {
        
            let coockingToolRow = TableRow<CookingToolsCell>.init(item: recieptId)
            section.append(row: coockingToolRow)
        }
        
        if let ingredients = reciept?.ingredients, ingredients.count > 0 {
        
            let ingridientsRow = TableRow<IngridientsCell>.init(item: recieptId)
            section.append(row: ingridientsRow)
        }
        
        if let steps = reciept?.steps, steps.count > 0 {
        
            let cookingStepsRow = TableRow<CookingStepsCell>.init(item: recieptId)
            section.append(row: cookingStepsRow)
        }
        
        tableDirector?.append(section: section)
        
        tableDirector?.reload()
        
    }
    
    
    func handlePop() {
        navigationController?.popViewController(animated: true)
    }
    
    func updateView(){

        tableView.addSubview(streachyHeader)
        tableView.contentInset = UIEdgeInsets(top: headerHeight, left: 0, bottom: 0, right: 0)
        tableView.contentOffset = CGPoint(x: 0, y: -headerHeight)
        setNewView()
    }
    
    func setNewView(){
        
        let getHeaderFrame =  CGRect(x: 0, y: tableView.contentOffset.y, width: UIScreen.main.bounds.width, height: max(64, -tableView.contentOffset.y) )
        streachyHeader.frame = getHeaderFrame
    }
}

extension RecieptController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        setNewView()
    }
}
