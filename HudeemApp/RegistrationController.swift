//
//  RegistrationController.swift
//  HudeemApp
//
//  Created by Admin on 30.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class RegistrationController: UIViewController {

    let stepOneCellId = "stepOneCellId"
    let stepTwoCellId = "stepTwoCellId"
    let stepThreeCellId = "stepThreeCellId"
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "     Регистрация"
        label.font = .boldSystemFont(ofSize: 18)
        return label
    }()
    
    lazy var proceedBar: ProceedBar = {
        let bar = ProceedBar()
        bar.delegate = self
        return bar
    }()
    
    let emailTextField: RoundTextFeld = {
        let tf = RoundTextFeld(frame: .zero)
        tf.placeholder = "E-mail"
        return tf
    }()
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.register(StepOneRegCell.self, forCellWithReuseIdentifier: self.stepOneCellId)
        cv.register(StepOneRegCell.self, forCellWithReuseIdentifier: self.stepTwoCellId)
        cv.register(StepOneRegCell.self, forCellWithReuseIdentifier: self.stepThreeCellId)
        cv.dataSource = self
        cv.delegate = self
        cv.backgroundColor = .white
        return cv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}

extension RegistrationController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let _: UITableViewCell
        _ = indexPath.item
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: stepOneCellId, for: indexPath)
        
        return cell
    }
}

extension RegistrationController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 200, height: 150)
    }
}

extension RegistrationController: ProceedBarDeleagte {
    
    func didSelectetItem(atIndex index: Int) {
        
        
    }
}
