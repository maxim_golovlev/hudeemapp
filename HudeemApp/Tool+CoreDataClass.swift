//
//  Tool+CoreDataClass.swift
//  HudeemApp
//
//  Created by Admin on 07.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData


public class Tool: NSManagedObject {

    static func create(withDict dict: [String: AnyObject], context: NSManagedObjectContext) -> Tool {
        
        let tool: Tool = Tool.createOrReturn(withId: dict["id"] as? Int, context: context)
        tool.initWithDict(dict: dict, context: context)
        
        return tool
    }
    
    private func initWithDict(dict: [String: AnyObject], context: NSManagedObjectContext) {
        
        self.title = dict["title"] as? String
        
        if let id = dict["id"] as? Int32 {
            self.id = id
        }
        
        NSManagedObject.attemptToSave(context: context)
    }
    
}
