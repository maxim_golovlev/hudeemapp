//
//  Extensions.swift
//  LenvendoTest
//
//  Created by Admin on 20.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import UIKit
import DrawerController
import CoreData

let purpleColor = UIColor.rgb(226, green: 90, blue: 135)
let turquoise = UIColor.rgb(59, green: 187, blue: 237)
let lightGreen = UIColor.rgb(104, green: 164, blue: 60)
let saladGreen = UIColor.rgb(159, green: 179, blue: 68)
let paleColor = UIColor.rgb(241, green: 237, blue: 226)
let ocherColor = UIColor.rgb(240, green: 195, blue: 110)
let whiteGreen = UIColor.rgb(247, green: 251, blue: 241)
let grayColor = UIColor.rgb(209, green: 209, blue: 209)

extension UIColor {
    static func rgb(_ red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
}

extension Date {
    
    func stringFromDate() -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        
        let resultString = dateFormatter.string(from: self)
        
        return resultString
    }
    

    func dayOfTheWeek(rus: Bool = true) -> String? {
        let weekdaysEng = [
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Satudrday"
        ]
        
        let weekdaysRus = [
            "вс",
            "пн",
            "вт",
            "ср",
            "чт",
            "пт",
            "сб"
        ]
        
        let calendar = Calendar.current
        let components = calendar.component(.weekday, from: self)
        return rus ? weekdaysRus[components - 1] : weekdaysEng[components - 1]
    }

    
    func dateFromDate() -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d"
        
        let resultString = dateFormatter.string(from: self)
        
        return resultString
    }
    
    func monthFromDate() -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM"
        dateFormatter.locale = Locale(identifier: "ru")
        
        let resultString = dateFormatter.string(from: self)
        
        return resultString
    }
    
    func startOfMonth() -> Date {
        let a = Calendar.current.startOfDay(for: self)
        let b = Calendar.current.dateComponents([.year, .month], from: a)
        return Calendar.current.date(from: b)!
    }
    
    func endOfMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
    }
}

extension String {
    
    func dateFromString() -> Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        
        let resultDate = dateFormatter.date(from: self)
        return resultDate
    }
    
    static func estimageTextSize(text: String?, size: CGSize = CGSize(width: 200, height: 1000), font: UIFont = UIFont.systemFont(ofSize: 14)) -> CGRect? {
        
        guard let text = text else { return nil }

        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let estimatedFrame = NSString(string: text).boundingRect(with: size, options: options, attributes:
            [NSFontAttributeName: font], context: nil)
        
        return estimatedFrame
    }
}

extension String {
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSFontAttributeName: font]
        let size = self.size(attributes: fontAttributes)
        return size.width
    }
    
    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSFontAttributeName: font]
        let size = self.size(attributes: fontAttributes)
        return size.height
    }
}

extension UIViewController {
    
    func drawerController() -> DrawerController? {
        
        var parentVC = self.parent
        
        while parentVC != nil {
            
            if parentVC!.isKind(of: DrawerController.self) {
                return parentVC as? DrawerController
            }
            
            parentVC = parentVC?.parent
        }
        
        return nil
    }
}

func MD5(string: String) -> Data {
    let messageData = string.data(using:.utf8)!
    var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
    
    _ = digestData.withUnsafeMutableBytes {digestBytes in
        messageData.withUnsafeBytes {messageBytes in
            CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
        }
    }
    
    return digestData
}

let imageCache = NSCache<AnyObject, AnyObject>()

class CustomImageView: UIImageView {
    
    var imageUrlString: String?
    
    static var sharedInstance = CustomImageView()
    
    func loadImageUsingUrlString(_ urlString: String?) {
        
        guard let urlString = urlString else { return }
        
        imageUrlString = urlString
        
        let url = URL(string: urlString)
        
        image = nil
        
        if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = imageFromCache
            return
        }
        
        if let imageFromStorage = PhotoManager.sharedManager.getPhoto(urlString: urlString) {
            self.image = imageFromStorage
            return
        }
        
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, respones, error) in
            
            if error != nil {
                print(error!)
                return
            }
            
           // if self.imageUrlString == urlString {
                PhotoManager.sharedManager.savePhoto(data: data, urlString: urlString)
         //   }
            
            DispatchQueue.main.async(execute: {
                
                if let imageToCache = UIImage(data: data!) {
                
                    if self.imageUrlString == urlString {
                        self.image = imageToCache
                    }
                    
                    imageCache.setObject(imageToCache, forKey: urlString as AnyObject)
                }
            })
            
        }).resume()
    }
    
    func saveImageInCashe(urlString: String?) {
        
        guard let urlString = urlString else { return }
        
    //    imageUrlString = urlString
        
        if PhotoManager.sharedManager.getPhoto(urlString: urlString) != nil {
            return
        }
            
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url, completionHandler: { (data, respones, error) in
            
            if error != nil {
                print(error!)
                return
            }
            
            if let imageToCache = UIImage(data: data!) {
                
              //  if self.imageUrlString == urlString {
                    imageCache.setObject(imageToCache, forKey: urlString as AnyObject)
                    
                    PhotoManager.sharedManager.savePhoto(data: data, urlString: urlString)
                    print("image \(urlString) saved")
             //   }
            }
            
        }).resume()
    }
}

extension UIView {
    
    func dropShadow() {
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 8
        
       // self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
       // self.layer.shouldRasterize = true
    }
}
extension UIColor {
    func as1ptImage() -> UIImage? {
        UIGraphicsBeginImageContext(CGSize(width: 0.5, height: 0.5))
        let ctx = UIGraphicsGetCurrentContext()
        self.setFill()
        ctx?.fill(CGRect(x: 0, y: 0, width: 0.5, height: 0.5))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}

extension String {
    
    func testWithPredicateType(type: Int) -> Bool {
    
        let emailRegex: String
    
        switch type {
        case 1:
            emailRegex = "^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$"
        case 2:
            emailRegex = "^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$"
        case 3:
            emailRegex = "(?:[a-z0-9!#$%\\&amp;'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&amp;'*+/=?\\^_`{|}" +
            "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" +
            "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-" +
            "z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5" +
            "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" +
            "9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" +
            "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        default:
            emailRegex = String()
        }
        
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
    
        return emailTest.evaluate(with: self)
    }
    
    
    func isValidEmail() -> Bool {
        
        let lenght = self.characters.count
        if lenght < 0 { return false }
        
        let entireRange = NSMakeRange(0, lenght)
        
        var detector: NSDataDetector?
        
        do {
            detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        } catch let error {
            print("failed to create data detector", error)
        }
        
        let matches = detector?.matches(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: entireRange)
        
        // should only a single match
        if matches?.count != 1 { return false}
        
        guard let result = matches?.first else { return false }
        
        // result should be a link
        if result.resultType != NSTextCheckingResult.CheckingType.link {
            return false
        }
        
        // result should be a recognized mail address
        if result.url?.scheme != "mailto" {
            return false
        }
        
        // match must be entire string
        if !NSEqualRanges(result.range, entireRange) {
            return false
        }
        
        // but schould not have the mail URL scheme
        if self.hasPrefix("mailto:") {
            return false
        }
        
        if !self.testWithPredicateType(type: 1) {
            return false
        }
        
        if !self.testWithPredicateType(type: 2) {
            return false
        }
        
      /*  if !self.testWithPredicateType(type: 3) {
            return false
        }*/
         
         // no complaints, string is valid email address
         return true
    }
}
