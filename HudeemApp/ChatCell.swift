//
//  ChatCell.swift
//  ElvisProject
//
//  Created by Admin on 04.02.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class ChatCell: BaseCollectionCell {
    
    var message: Message? {
        didSet {
            textView.text = message?.text
            if let imageData = message?.image as Data? {
                messageImageView.image = UIImage(data: imageData)
            }
        }
    }
    
    static let incomingBubleImage = #imageLiteral(resourceName: "incoming_buble").resizableImage(withCapInsets: UIEdgeInsetsMake(10, 40, 40, 26))
    
    static let outgoingBubleImage = #imageLiteral(resourceName: "outgoing_buble").resizableImage(withCapInsets: UIEdgeInsetsMake(10, 26, 40, 40))
    
    let bubleImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.masksToBounds = true
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 16
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.image = #imageLiteral(resourceName: "avatar_icon")
        return imageView
    }()
    
    let textView: UITextView = {
        let textView = UITextView()
        textView.font = AppFont.roman(size: 13)
        textView.backgroundColor = .clear
        textView.isEditable = false
        textView.isScrollEnabled = false
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.backgroundColor = .green
        return textView
    }()
    
    lazy var messageImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 16
        imageView.layer.masksToBounds = true
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleImageTap)))
        imageView.backgroundColor = .blue
        return imageView
    }()
    
    lazy var playPauseButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.tintColor = UIColor.white
        button.setImage(#imageLiteral(resourceName: "play"), for: .normal)
        button.setImage(#imageLiteral(resourceName: "pause"), for: .selected)
        button.addTarget(self, action: #selector(handlePlay), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    
    let activityIndicatorView: UIActivityIndicatorView = {
        let aiv = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        aiv.translatesAutoresizingMaskIntoConstraints = false
        aiv.hidesWhenStopped = true
        return aiv
    }()
    
    let videoProgressView: UIProgressView = {
        let pv = UIProgressView()
        pv.translatesAutoresizingMaskIntoConstraints = false
        return pv
    }()
    
    var bubleWidthAnchor: NSLayoutConstraint?
    var bubleRightAnchor: NSLayoutConstraint?
    var bubleLeftAnchor: NSLayoutConstraint?
    var textViewLeftAnchor: NSLayoutConstraint?
    var textViewRightAnchor: NSLayoutConstraint?
    var profileImageLeftAnchor: NSLayoutConstraint?
    var profileImageRightAnchor: NSLayoutConstraint?
    var messageImageLeftAnchor: NSLayoutConstraint?
    var messageImageRightAnchor: NSLayoutConstraint?
    var messageImageViewHeightConstraint: NSLayoutConstraint?
    
    override func setupViews() {
        
        backgroundColor = .gray
        
        addSubview(profileImageView)
        profileImageLeftAnchor = profileImageView.leftAnchor.constraint(equalTo: leftAnchor, constant:8)
        profileImageRightAnchor = profileImageView.rightAnchor.constraint(equalTo: rightAnchor, constant:-8)
        profileImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant:0).isActive = true
        profileImageView.heightAnchor.constraint(equalToConstant: 32).isActive = true
        profileImageView.widthAnchor.constraint(equalToConstant: 32).isActive = true
        
        addSubview(bubleImageView)
        bubleLeftAnchor = bubleImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 40)
        bubleRightAnchor = bubleImageView.rightAnchor.constraint(equalTo: rightAnchor, constant: -40)
        bubleImageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        bubleImageView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        bubleWidthAnchor = bubleImageView.widthAnchor.constraint(equalToConstant: 200)
        bubleWidthAnchor?.isActive = true
        
        bubleImageView.addSubview(textView)
        textViewLeftAnchor = textView.leftAnchor.constraint(equalTo: bubleImageView.leftAnchor)
        textViewLeftAnchor?.isActive = true
        textViewRightAnchor = textView.rightAnchor.constraint(equalTo: bubleImageView.rightAnchor)
        textViewRightAnchor?.isActive = true
        textView.topAnchor.constraint(equalTo: bubleImageView.topAnchor, constant: 0).isActive = true
        
        bubleImageView.addSubview(messageImageView)
        messageImageLeftAnchor = messageImageView.leftAnchor.constraint(equalTo: textView.leftAnchor)
        messageImageLeftAnchor?.isActive = true
        messageImageRightAnchor = messageImageView.rightAnchor.constraint(equalTo: textView.rightAnchor)
        messageImageRightAnchor?.isActive = true
        messageImageViewHeightConstraint = messageImageView.heightAnchor.constraint(equalToConstant: 100)
        messageImageViewHeightConstraint?.isActive = true
        messageImageView.topAnchor.constraint(greaterThanOrEqualTo: textView.bottomAnchor, constant: 8).isActive = true
        messageImageView.bottomAnchor.constraint(equalTo: bubleImageView.bottomAnchor).isActive = true
        
        messageImageView.addSubview(playPauseButton)
        playPauseButton.centerXAnchor.constraint(equalTo: messageImageView.centerXAnchor).isActive = true
        playPauseButton.centerYAnchor.constraint(equalTo: messageImageView.centerYAnchor).isActive = true
        playPauseButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        playPauseButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        messageImageView.addSubview(activityIndicatorView)
        activityIndicatorView.centerXAnchor.constraint(equalTo: messageImageView.centerXAnchor).isActive = true
        activityIndicatorView.centerYAnchor.constraint(equalTo: messageImageView.centerYAnchor).isActive = true
        activityIndicatorView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        activityIndicatorView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        activityIndicatorView.stopAnimating()
        player?.pause()
        playerLayer?.removeFromSuperlayer()
        videoProgressView.removeFromSuperview()
    }
    
    var player: AVPlayer?
    var playerLayer: AVPlayerLayer?
    
    var isPlaying: Bool?
    
    func handlePlay() {
        
        switch isPlaying {
        case .some(true):
            player?.pause()
            playPauseButton.setImage( #imageLiteral(resourceName: "play"), for: UIControlState())
        case .some(false):
            player?.play()
            playPauseButton.setImage(#imageLiteral(resourceName: "pause"), for: UIControlState())
        default:
            configurePlayer()
        }
        
        if isPlaying != nil {
            isPlaying! = !isPlaying!
        }
    }
    
    func configurePlayer() {
      /*  if let videoUrl = userMessage?.videoUrl, let url = URL(string: videoUrl) {
            
            player = AVPlayer(url: url)
            playerLayer = AVPlayerLayer(player: player)
            playerLayer?.frame = messageImageView.bounds
            playerLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
            messageImageView.layer.addSublayer(playerLayer!)
            
            player?.play()
            player?.addObserver(self, forKeyPath: "currentItem.loadedTimeRanges", options: .new, context: nil)
            activityIndicatorView.startAnimating()
            playPauseButton.removeFromSuperview()
            
            let interval = CMTime(value: 1, timescale: 2)
            player?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main, using: { (progressCMTime) in
                
                let seconds = CMTimeGetSeconds(progressCMTime)
                
                if let duration = self.player?.currentItem?.duration {
                    let durationSeconds = CMTimeGetSeconds(duration)
                    
                    let progress = Float(seconds / durationSeconds)
                    
                    self.videoProgressView.progress = progress
                    if progress != 0 {
                        self.videoProgressView.isHidden = false
                    }
                }
            })
            
            NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd), name: .AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
        }*/
    }
    
    func playerItemDidReachEnd(notification: NSNotification) {
        if let playerItem = notification.object as? AVPlayerItem {
            playerItem.seek(to: kCMTimeZero)
            handlePlay()
            videoProgressView.isHidden = true
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        //this is when the player is ready and rendering frames
        if keyPath == "currentItem.loadedTimeRanges" {
            activityIndicatorView.stopAnimating()
            isPlaying = true
            
            messageImageView.addSubview(playPauseButton)
            playPauseButton.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
            playPauseButton.centerXAnchor.constraint(equalTo: messageImageView.centerXAnchor).isActive = true
            playPauseButton.centerYAnchor.constraint(equalTo: messageImageView.centerYAnchor).isActive = true
            playPauseButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
            playPauseButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
            
            messageImageView.addSubview(videoProgressView)
            videoProgressView.leftAnchor.constraint(equalTo: messageImageView.leftAnchor).isActive = true
            videoProgressView.rightAnchor.constraint(equalTo: messageImageView.rightAnchor).isActive = true
            videoProgressView.bottomAnchor.constraint(equalTo: messageImageView.bottomAnchor, constant: -5).isActive = true
            videoProgressView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        }
    }
    
    func handleImageTap() {
        
     //   if userMessage?.videoUrl != nil { return }
    //    self.chatViewController?.performZoomIn(forImageView: messageImageView)
    }
}
