//
//  SelfSizedCell.swift
//  HudeemApp
//
//  Created by Admin on 23.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import TableKit

class SelfSizedCell: BaseTableCell, ConfigurableCell {

    let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let separator: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        return view
    }()
    
    static var estimatedHeight: CGFloat? {
        return 60
    }
    
    static var defaultHeight: CGFloat? {
        return UITableViewAutomaticDimension
    }
    
    func configure(with data: (title: String?, font: UIFont, topInset: CGFloat?, botInset: CGFloat?, withSeparator: Bool?)) {
        titleLabel.text = data.title
        titleLabel.font = data.font
        titleLabelTopConstraint?.constant = data.topInset ?? 0
        titleLabelBotConstraint?.constant = -(data.botInset ?? 0)
        
        if data.withSeparator == true {
            addSubview(separator)
            separator.anchor(nil, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: Screen.pixel)
        }
    }
    
    private var titleLabelTopConstraint: NSLayoutConstraint?
    private var titleLabelBotConstraint: NSLayoutConstraint?
    
    override func setupViews() {
        
        addSubview(titleLabel)
        titleLabel.anchor(nil, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 18, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        titleLabelTopConstraint = titleLabel.topAnchor.constraint(equalTo: topAnchor)
        titleLabelTopConstraint?.isActive = true
        
        titleLabelBotConstraint = titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
        titleLabelBotConstraint?.isActive = true
        
    }
}
