//
//  ExerciseResultCell.swift
//  HudeemApp
//
//  Created by Admin on 29.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import TableKit

class ExerciseResultCell : BaseTableCell, ConfigurableCell {
    
    let resultImageView: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = AppFont.medium(size: 12)
        return label
    }()
    
    let subtitleLabel: UILabel = {
        let label = UILabel()
        label.font = AppFont.roman(size: 12)
        return label
    }()
    
    static var estimatedHeight: CGFloat? {
        return 20
    }
    
    static var defaultHeight: CGFloat? {
        return UITableViewAutomaticDimension
    }
    
    func configure(with data: (title: String?, subtitle: String?, picture: UIImage?)) {
        titleLabel.text = data.title
        subtitleLabel.text = data.subtitle
        imageView?.image = data.picture
    }
    
    override func setupViews() {
        
        addSubview(resultImageView)
        resultImageView.anchor(topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 30, leftConstant: 22, bottomConstant: 0, rightConstant: 0, widthConstant: 35, heightConstant: 35)
        
        addSubview(titleLabel)
        titleLabel.anchor(nil, left: resultImageView.leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 11, bottomConstant: 0, rightConstant: 22, widthConstant: 0, heightConstant: 0)
        
        
        addSubview(subtitleLabel)
        subtitleLabel.anchor(titleLabel.topAnchor, left: titleLabel.leftAnchor, bottom: bottomAnchor, right: titleLabel.rightAnchor, topConstant: 15, leftConstant: 0, bottomConstant: 30, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
    }
    
    func doneHandler() {
        
    }
    
    func notDoneHandler() {
        
        
    }
}

