//
//  Comment+CoreDataClass.swift
//  HudeemApp
//
//  Created by Admin on 21.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData


public class Comment: NSManagedObject {

    func initWith(status: CompleteStatus, planId: Int32?, comment: String? = nil) {        
        self.status = status.rawValue
        if let planId = planId {
            self.planId = planId
        }
        self.comment = comment
    }
    
}
