//
//  PostCell.swift
//  LenvendoTest
//
//  Created by Admin on 20.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

protocol MenuCellDelegate: class {
    }

class MenuCell: UITableViewCell {
    
    override var isSelected: Bool {
        didSet {
            
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            
        }
    }
    
    weak var delegate: MenuCellDelegate?
    
    let menuImageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.clipsToBounds = true
        iv.tintColor = .black
        return iv
    }()
    
    let label: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = AppFont.light(size: 20)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    var buttonHeightContraint: NSLayoutConstraint?
    var buttonBottomConnstraint: NSLayoutConstraint?
    var iconWidthConstraint: NSLayoutConstraint?
    var iconLeftConstraint: NSLayoutConstraint?
    
    var menuTitle: MenuTitle? {
        didSet {
            
            label.text = menuTitle?.title
            menuImageView.image = menuTitle?.icon
        }
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()

    }
    
    func setupViews() {
        
        backgroundColor = .white
        
        addSubview(menuImageView)
        addSubview(label)

        addConstraintsWithFormat("H:[v0]-20-[v1]->=8-|", views: menuImageView, label)
        
        iconLeftConstraint = menuImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 20)
        iconLeftConstraint?.isActive = true
        
        iconWidthConstraint = menuImageView.widthAnchor.constraint(equalToConstant: 22)
        iconWidthConstraint?.isActive = true
        
        menuImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
    }    
    
}

