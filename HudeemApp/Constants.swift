//
//  Constants.swift
//  HudeemApp
//
//  Created by Admin on 14.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

struct Screen {
    static let scale = UIScreen.main.scale
    static let height = UIScreen.main.bounds.size.height
    static let width = UIScreen.main.bounds.size.width
    static let pixel = 1 / UIScreen.main.scale
    static let navbarHeight: CGFloat = 64
    static let tabbarHeight: CGFloat = 49
}

struct AppFont {
    
    static func medium(size: CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeueCyr-Medium", size: size) ?? .systemFont(ofSize: 2)
    }
    
    static func light(size: CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeueCyr-Light", size: size) ?? .systemFont(ofSize: 2)
    }
    
    static func black(size: CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeueCyr-Black", size: size) ?? .systemFont(ofSize: 2)
    }
    
    static func roman(size: CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeueCyr-Roman", size: size) ?? .systemFont(ofSize: 2)
    }
    
    static func bold(size: CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeueCyr-Bold", size: size) ?? .systemFont(ofSize: 2)
    }

}

struct DishCellActions {
    
    static let openRecieptClicked = "openRecieptClicked"
    static let rateBtnClicked = "DishrateBtnClicked"
}

struct RecieptCellActions {
    
    static let rateBtnClicked = "RecieptrateBtnClicked"
    
}

struct TrainCellActions {
    
    static let openTecnicClicked = "openTecnicClicked"
    static let doneBtnClicked = "doneBtnClicked"
    static let openVideoClicked = "openVideoClicked"
}

struct Plural {
    
    static func from(index: Int?, postfix: String?) -> String? {
        
        guard let index = index else { return "" }
        
        switch index {
        case 0:
            return ""
        default:
            return ""
        }
        
    }
}

struct DishCellSizes {
    static let mealTitleFont = AppFont.medium(size: 22)
    static let dishTitleFont = AppFont.medium(size: 17)
    static let nutritionLableTitleFont = AppFont.medium(size: 14)
    static let nutritionLableValueFont = AppFont.black(size: 14)
    static let cookingTimeLabelFont = AppFont.medium(size: 13)
    static let recieptTitleFont = AppFont.medium(size: 11)
    static let buttonTitleFont = AppFont.medium(size: 12)
    static let greenLabelsFont = AppFont.medium(size: 10)
}

struct AlertMsg {
    static let unknownError = "Something goes wrong. Try again later."
}

struct AppColors {
    static let purpleColor = UIColor.rgb(226, green: 90, blue: 135)
    static let turquoise = UIColor.rgb(59, green: 187, blue: 237)
    static let lightGreen = UIColor.rgb(104, green: 164, blue: 60)
    static let saladGreen = UIColor.rgb(159, green: 179, blue: 68)
    static let paleColor = UIColor.rgb(241, green: 237, blue: 226)
    static let ocherColor = UIColor.rgb(240, green: 195, blue: 110)
    static let whiteGreen = UIColor.rgb(247, green: 251, blue: 241)
    static let grayColor = UIColor.rgb(209, green: 209, blue: 209)
}
