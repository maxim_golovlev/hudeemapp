//
//  BreakTimeCell.swift
//  HudeemApp
//
//  Created by Admin on 30.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import TableKit

class BreakTimeCell: BaseTableCell, ConfigurableCell {
    
    let titleLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    static var estimatedHeight: CGFloat? {
        return 60
    }
    
    static var defaultHeight: CGFloat? {
        return UITableViewAutomaticDimension
    }
    
    func configure(with time: (min: Int?, sec: Int?)) {
        
        if time.min == nil && time.sec == nil { return }
        
        let textAttr = [NSFontAttributeName : AppFont.medium(size: 11)]
        
        let valueAttr = [NSForegroundColorAttributeName : purpleColor,
                         NSFontAttributeName : AppFont.bold(size: 11)]
        
        let clockIcon = CentredAttachment()
        clockIcon.image = #imageLiteral(resourceName: "clock_icon")
        
        let combine = NSMutableAttributedString()
        combine.append(NSAttributedString(attachment: clockIcon))
        combine.append(NSAttributedString(string: " Время переыва между подходами: ", attributes: textAttr))
        
        let min = time.min == 0 ? "" : "\(time.min ?? 0) мин."
        let sec = time.sec == 0 ? "" : "\(time.sec ?? 0) сек."
        
        combine.append(NSAttributedString(string: min + sec, attributes: valueAttr))
        titleLabel.attributedText = combine
    }
    
    private var titleLabelTopConstraint: NSLayoutConstraint?
    private var titleLabelBotConstraint: NSLayoutConstraint?
    
    override func setupViews() {
        
        addSubview(titleLabel)
        titleLabel.anchor(nil, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 18, bottomConstant: 0, rightConstant: 18, widthConstant: 0, heightConstant: 0)
        
        titleLabel.anchorCenterYToSuperview()

    }
}

