//
//  TechnicHeaderCell.swift
//  HudeemApp
//
//  Created by Admin on 30.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import TableKit

class TechnicCell : BaseTableCell, ConfigurableCell {
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = AppFont.medium(size: 17)
        label.numberOfLines = 0
        return label
    }()
    
    let subtitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = AppFont.roman(size: 15)
        label.numberOfLines = 0
        return label
    }()

    let playerImageView: CustomImageView = {
        let imageView = CustomImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    lazy var playButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "playButton"), for: .normal)
        button.addTarget(self, action: #selector(openPlayer), for: .touchUpInside)
        return button
    }()
    
    let separator: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        return view
    }()
    
    static var estimatedHeight: CGFloat? {
        return 200
    }
    
    static var defaultHeight: CGFloat? {
        return UITableViewAutomaticDimension
    }
    
    func openPlayer() {
        TableCellAction(key: TrainCellActions.openVideoClicked, sender: self).invoke()
    }
    
    func configure(with data: (title: String?, subtitle: String?, image: UIImage?, withSeparator: Bool?)) {
        
        if data.image != nil {
            playerHeightConstraint?.constant = 185
            playerBotConstraint?.constant = -20
            playerImageView.image = data.image
        } else {
            playerHeightConstraint?.constant = 0
            playerBotConstraint?.constant = 0
        }
        
        titleLabel.text = data.title
        
        subtitleLabel.text = data.subtitle
        
        if data.withSeparator == true {
            addSubview(separator)
            separator.anchor(nil, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: Screen.pixel)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        separator.removeFromSuperview()
    }
    
    private var titleLabelTopConstraint: NSLayoutConstraint?
    private var titleLabelBotConstraint: NSLayoutConstraint?
    private var playerHeightConstraint: NSLayoutConstraint?
    private var playerBotConstraint: NSLayoutConstraint?
    
    override func setupViews() {
        
        addSubview(titleLabel)
        titleLabel.anchor(topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 22, leftConstant: 21, bottomConstant: 0, rightConstant: 21, widthConstant: 0, heightConstant: 0)
        
        addSubview(subtitleLabel)
        subtitleLabel.anchor(titleLabel.bottomAnchor, left: titleLabel.leftAnchor, bottom: nil, right: titleLabel.rightAnchor, topConstant: 25, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        addSubview(playerImageView)
        playerImageView.anchor(subtitleLabel.bottomAnchor, left: subtitleLabel.leftAnchor, bottom: nil, right: subtitleLabel.rightAnchor, topConstant: 25, leftConstant: 0, bottomConstant: 20, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        playerHeightConstraint = playerImageView.heightAnchor.constraint(equalToConstant: 185)
        playerHeightConstraint?.isActive = true
        
        playerBotConstraint = playerImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20)
        playerBotConstraint?.isActive = true
        
        playerImageView.addSubview(playButton)
        playButton.anchor(nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 60, heightConstant: 60)
        playButton.anchorCenterSuperview()
        
    }


}
