//
//  Ingredient+CoreDataClass.swift
//  HudeemApp
//
//  Created by Admin on 26.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData


public class Ingredient: NSManagedObject {

    static func create(withDict dict: [String: AnyObject], context: NSManagedObjectContext) -> Ingredient {
        
        let ingredient: Ingredient = Ingredient.createOrReturn(withId: dict["id"] as? Int, context: context)
        ingredient.initWithDict(dict: dict, context: context)
        
        return ingredient
    }
    
    private func initWithDict(dict: [String: AnyObject], context: NSManagedObjectContext) {
        
        self.title = dict["title"] as? String
        self.photoUrl = dict["photo"] as? String
        
        if let size = dict["size"] as? Int32 {
            self.size = size
        }
        
        if let id = dict["id"] as? Int32 {
            self.id = id
        }
        
        NSManagedObject.attemptToSave(context: context)
    }
    
}
