//
//  CookingCell.swift
//  HudeemApp
//
//  Created by Admin on 06.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import TableKit

class CookingStepsCell: BaseTableCell, ConfigurableCell {
    
    let cellId = "cellId"
    let headerId = "headerId"
    let footerId = "footerId"
    
    static var estimatedHeight: CGFloat? {
        return 60
    }
    
    static var defaultHeight: CGFloat? {
        return UITableViewAutomaticDimension
    }
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 5
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.register(CookingStepCell.self, forCellWithReuseIdentifier: self.cellId)
        cv.register(HeaderCookingStepCell.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: self.headerId)
        cv.register(FooterCookingCell.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: self.footerId)
        cv.dataSource = self
        cv.delegate = self
        cv.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        cv.backgroundColor = .white
        cv.isScrollEnabled = false
        return cv
    }()
    
    var coockingSteps: [Step]?
    
    weak var shareDelegate: Shareable?

    var recieptId: Int32? {
        didSet {
            guard let id = recieptId else { return }
            
            if let reciept: Reciept = Reciept.attemptToFetch(withId: Int(id), context: AppDelegate.currentContext) {
            
                if let steps = reciept.steps {
                    self.coockingSteps = Array(steps) as? [Step]
                    
                    collectionViewHeightConstraint?.constant = getCollectionHeight(reciept: reciept)
                    
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }
            }
        }
    }
    
    var collectionViewHeightConstraint: NSLayoutConstraint?
    
    override func setupViews() {
        
        addSubview(collectionView)
        collectionView.fillSuperview()
        
        collectionViewHeightConstraint = collectionView.heightAnchor.constraint(equalToConstant: 0)
        collectionViewHeightConstraint?.isActive = true
    }
    
    func configure(with recieptId: Int32?) {
        self.recieptId = recieptId
    }
}

extension CookingStepsCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return coockingSteps?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! CookingStepCell
        let step = (coockingSteps?[indexPath.row], indexPath.row)
        cell.step = step
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let view:UICollectionReusableView
        
        switch kind {
        case UICollectionElementKindSectionHeader:
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerId, for: indexPath) as! HeaderCookingStepCell
            view = header
            
        case UICollectionElementKindSectionFooter:
            let footer = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerId, for: indexPath) as! FooterCookingCell
            footer.shareDelegate = self
            view = footer
        default:
            view = UICollectionReusableView()
        }
        return view
    }
    
    func getCollectionHeight(reciept: Reciept) -> CGFloat {
        
        guard let steps = reciept.steps else { return 0 }
        guard let stepsArray = Array(steps) as? [Step] else { return 0 }
        if stepsArray.count <= 0 { return 0 }
        
        var cellsHeight: CGFloat = 0
        
        for step in stepsArray {
            let height = CookingStepCell.getHeight(fromStep: step)
            cellsHeight = cellsHeight + height + 5
        }
        
        let headerHeight = HeaderCookingStepCell.getHeight()
        let footerHeight = FooterCookingCell.getHeight()
        
        return cellsHeight + headerHeight + footerHeight - 5
    }
}

extension CookingStepsCell: UICollectionViewDelegate {
    
}

extension CookingStepsCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let currentStep = coockingSteps?[indexPath.row]
        let height = CookingStepCell.getHeight(fromStep: currentStep)
        
        return CGSize(width: UIScreen.main.bounds.width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        let headerHeight = HeaderCookingStepCell.getHeight()
        
        return CGSize(width: UIScreen.main.bounds.width, height: headerHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        
        let footerHeight = FooterCookingCell.getHeight()
        
        return CGSize(width: UIScreen.main.bounds.width, height: footerHeight)
    }
}

extension CookingStepsCell: Shareable {
    
    func share(textToShare: String?) {        
        self.shareDelegate?.share(textToShare: textToShare)
    }    
}

