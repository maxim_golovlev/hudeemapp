//
//  DishNutritionsCell.swift
//  HudeemApp
//
//  Created by Admin on 03.05.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import TableKit

class DishNutritionsCell: BaseTableCell, ConfigurableCell {    

    var pieModels: [PieSliceModel]?
    
    let caloryLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    let portionsLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    let nutritionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    var createModels: (([PieSliceModel]) -> ())?
    
    
    var pieChart: PieChart? {
        didSet {
            
            guard let pieChart = pieChart else { return }
            
            pieChart.innerRadius = 5
            pieChart.outerRadius = 20
            pieChart.selectedOffset = 30
            pieChart.animDuration = 0.5
            pieChart.referenceAngle = 270
            
            self.pieChart?.models = pieModels ?? []
            
            pieChart.translatesAutoresizingMaskIntoConstraints = false
            pieChart.heightAnchor.constraint(equalToConstant: 40).isActive = true
            pieChart.widthAnchor.constraint(equalToConstant: 40).isActive = true
            pieChart.isUserInteractionEnabled = false
            
            nutritionsHorizontalStackView.addArrangedSubview(pieChart)
        }
    }
    
    let nutritionsHorizontalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .equalCentering
        stackView.spacing = 8
        stackView.alignment = .center
        return stackView
    }()
    
    
    static var estimatedHeight: CGFloat? {
        return 20
    }
    
    static var defaultHeight: CGFloat? {
        return UITableViewAutomaticDimension
    }
    
    
    func configure(with data:(calories: Int32?,
        percFats: Float?,
        percCarbohydrates: Float?,
        percProteins: Float?,
        cookTime: Int32?,
        proteins: Int32?,
        fats: Int32?,
        carbohydrates: Int32?,
        size: Int32? )) {
        
        pieModels = [
            PieSliceModel(value: Double(data.percFats ?? 0), color: ocherColor),
            PieSliceModel(value: Double(data.percCarbohydrates ?? 0), color: saladGreen),
            PieSliceModel(value: Double(data.percProteins ?? 0), color: paleColor),
        ]
        
        pieChart = PieChart(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        
        let paragraphStyle1 = NSMutableParagraphStyle()
        paragraphStyle1.paragraphSpacing = 0.75 * DishCellSizes.nutritionLableTitleFont.lineHeight
        let headerAttr = [NSFontAttributeName : DishCellSizes.nutritionLableTitleFont,
                          NSParagraphStyleAttributeName:paragraphStyle1]
        let valueAttr = [NSForegroundColorAttributeName : purpleColor,
                         NSFontAttributeName : DishCellSizes.nutritionLableValueFont,
                         NSParagraphStyleAttributeName:paragraphStyle1]
        let combine1 = NSMutableAttributedString()
        combine1.append(NSAttributedString(string: "Ккал:\n", attributes: headerAttr))
        combine1.append(NSAttributedString(string: "\(data.calories ?? 0)", attributes: valueAttr))
        caloryLabel.attributedText = combine1
        let combine2 = NSMutableAttributedString()
        combine2.append(NSAttributedString(string: "Б/Ж/У:\n", attributes: headerAttr))
        combine2.append(NSAttributedString(string: "\(data.proteins ?? 0)/\(data.fats ?? 0)/\(data.carbohydrates ?? 0) г.", attributes: valueAttr))
        nutritionLabel.attributedText = combine2
        let combine3 = NSMutableAttributedString()
        combine3.append(NSAttributedString(string: "Порция:\n", attributes: headerAttr))
        combine3.append(NSAttributedString(string: "\(data.size ?? 0) г.", attributes: valueAttr))
        portionsLabel.attributedText = combine3
        
    }
    
    override func setupViews() {
        
        addSubview(nutritionsHorizontalStackView)
        nutritionsHorizontalStackView.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 20, leftConstant: 18, bottomConstant: 10, rightConstant: 18, widthConstant: 0, heightConstant: 40)
        
        nutritionsHorizontalStackView.addArrangedSubview(caloryLabel)
        nutritionsHorizontalStackView.addArrangedSubview(portionsLabel)
        nutritionsHorizontalStackView.addArrangedSubview(nutritionLabel)

    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        pieChart?.removeFromSuperview()
        pieModels?.removeAll()
    }
    
}
