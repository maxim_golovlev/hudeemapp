//
//  Tool+CoreDataProperties.swift
//  HudeemApp
//
//  Created by Admin on 09.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData


extension Tool {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Tool> {
        return NSFetchRequest<Tool>(entityName: "Tool")
    }

    @NSManaged public var id: Int32
    @NSManaged public var title: String?
    @NSManaged public var reciept: Reciept?

}
