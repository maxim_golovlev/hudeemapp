//
//  CentredAttachment.swift
//  HudeemApp
//
//  Created by Admin on 14.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class CentredAttachment: NSTextAttachment {

    override func attachmentBounds(for textContainer: NSTextContainer?, proposedLineFragment lineFrag: CGRect, glyphPosition position: CGPoint, characterIndex charIndex: Int) -> CGRect {
        
        bounds.origin = CGPoint(x: 0, y: -1)
        bounds.size = self.image?.size ?? .zero
        return bounds
    }
}
