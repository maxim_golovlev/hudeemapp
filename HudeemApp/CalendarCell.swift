//
//  CalendarCell.swift
//  HudeemApp
//
//  Created by Admin on 30.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class CalendarCell: BaseCollectionCell {
    
    static let roundHeight: CGFloat = 58

    override var isSelected: Bool {
        didSet {
            toggleColor(date: date)
        }
    }
    
    let roundView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = roundHeight/2
        view.layer.masksToBounds = true
        view.backgroundColor = .white
        return view
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = AppFont.roman(size: 21)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.heightAnchor.constraint(equalToConstant: label.font.lineHeight * 1).isActive = true
        return label
    }()
    
    let subtitleLabel: UILabel = {
        let label = UILabel()
        label.font = AppFont.roman(size: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.heightAnchor.constraint(equalToConstant: label.font.lineHeight * 0.9).isActive = true
        return label
    }()
    
    let weekTitleLabel: UILabel = {
        let label = UILabel()
        label.font = AppFont.roman(size: 12)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let verticalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 2
        stackView.backgroundColor = .green
        stackView.alignment = .center
        stackView.distribution = .fillProportionally
        return stackView
    }()
    
    let currentDate = Calendar.current.startOfDay(for: Date())
    
    var date: Date? {
        didSet {
            titleLabel.text = date?.dateFromDate()
            subtitleLabel.text = date?.monthFromDate()
            weekTitleLabel.text = date?.dayOfTheWeek()
            
            toggleColor(date: date)
        }
    }
    
    override func prepareForReuse() {
        roundView.backgroundColor = .white
        titleLabel.textColor = .black
        subtitleLabel.textColor = .black
        self.isSelected = false
    }
    
    override func setupViews() {
        super.setupViews()
        
        addSubview(roundView)
        roundView.heightAnchor.constraint(equalToConstant: CalendarCell.roundHeight).isActive = true
        roundView.widthAnchor.constraint(equalToConstant: CalendarCell.roundHeight).isActive = true
        roundView.anchorCenterSuperview()
        
        addSubview(verticalStackView)
        verticalStackView.anchorCenterSuperview()
        
        verticalStackView.addArrangedSubview(titleLabel)
        verticalStackView.addArrangedSubview(subtitleLabel)
        verticalStackView.addArrangedSubview(weekTitleLabel)
        
    }
    
    func toggleColor(date: Date?) {
        
        if let date = date {
            titleLabel.textColor = date < currentDate ? .lightGray : .black
            subtitleLabel.textColor = date < currentDate ? .lightGray : .black
            weekTitleLabel.textColor = date < currentDate ? .lightGray : .black
        }
        
        titleLabel.textColor = isSelected ? .white : titleLabel.textColor
        subtitleLabel.textColor = isSelected ? .white : subtitleLabel.textColor
        weekTitleLabel.textColor = isSelected ? .white : weekTitleLabel.textColor
        roundView.backgroundColor = isSelected ? purpleColor : .white
    }

}
