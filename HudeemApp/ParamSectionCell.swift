//
//  ParamSectionTitle.swift
//  HudeemApp
//
//  Created by Admin on 27.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import TableKit

class ParamSectionCell : BaseTableCell, ConfigurableCell {
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var titleView: UIView = {
        
        let view = UIView()
        view.addSubview(self.titleLabel)
        self.titleLabel.anchor(nil, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        self.titleLabel.anchorCenterYToSuperview()
        
        return view
    }()
    
    lazy var subtitleView: UIView = {
        
        let view = UIView()
        view.addSubview(self.subTitleLabel)
        self.subTitleLabel.anchor(nil, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        self.subTitleLabel.anchorCenterYToSuperview()
        
        return view
    }()
    
    let subTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let horizontalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.alignment = .fill
        stackView.spacing = 0
        return stackView
    }()
    
    let separator: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.968627451, blue: 0.968627451, alpha: 1)
        return view
    }()
    
    static var estimatedHeight: CGFloat? {
        return 20
    }
    
    static var defaultHeight: CGFloat? {
        return UITableViewAutomaticDimension
    }
    
    func configure(with data: (title: String?, subtitle: String?, font: UIFont, backColor: UIColor?)) {
        titleLabel.text = data.title
        titleLabel.font = data.font
        
        subTitleLabel.text = data.subtitle
        subTitleLabel.font = data.font
        
        titleView.backgroundColor = data.backColor ?? .white
        subtitleView.backgroundColor = data.backColor ?? .white
    }
    
    override func setupViews() {
        
        addSubview(horizontalStackView)
        horizontalStackView.anchor(nil, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 23, bottomConstant: 0, rightConstant: 23, widthConstant: 0, heightConstant: 44)
        horizontalStackView.anchorCenterYToSuperview()
        
        horizontalStackView.addArrangedSubview(titleView)
        horizontalStackView.addArrangedSubview(subtitleView)
        
        addSubview(separator)
        separator.anchor(nil, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 23, bottomConstant: 0, rightConstant: 23, widthConstant: 0, heightConstant: Screen.pixel)
    }
}
