//
//  DietController.swift
//  HudeemApp
//
//  Created by Admin on 21.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import CoreData
import ReachabilitySwift
import TableKit

class DietController: CenterController {

    lazy var timelineNavBar: TimelineNavBar = {
        let bar = TimelineNavBar()
        bar.delegate = self
        return bar
    }()
    
    var delegate:CollectionViewFetchedResultsControllerDelegate?
    
    let cellId = "cellId"
    let footerId = "footerId"
    var currentMealPlanId: Int?
    var currentContext: NSManagedObjectContext?
    var currentSelectedDayIndex: Int?
    var currentDate: Date?
    
    var dishCount: Int?
    
    let reachability = Reachability()!
    
    var tableDirector: TableDirector?
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .white
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.backgroundColor = .clear
        self.tableDirector = TableDirector(tableView: tableView)
        return tableView
    }()
    
    let errorLabel: UILabel = {
        let label = UILabel()
        label.font = AppFont.medium(size: 18)
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()

    
    var predicate: NSPredicate?
    
    var meals: [Meal]? {
        
        let predicate = NSPredicate(format: "mealPlan.id = %d", self.currentMealPlanId ?? 0)
        let sortDescriptors = [NSSortDescriptor(key: "sort", ascending: true)]
        let meals: [Meal]? = Meal.fetch(predicate: predicate, sortDescriptors: sortDescriptors, context: self.currentContext ?? AppDelegate.currentContext)
        
        return meals
    }
    
    var dishes: [Dish]? {
        
        let predicate = NSPredicate(format: "meal.mealPlan.id = %d", self.currentMealPlanId ?? 0)
        let sortDescriptors = [NSSortDescriptor(key: "planId", ascending: true)]
        let dishes: [Dish]? = Dish.fetch(predicate: predicate, sortDescriptors: sortDescriptors, context: self.currentContext ?? AppDelegate.currentContext)
        
        return dishes
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: false)
        
        fetchMeals(withDate: self.currentDate ?? Date())
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Мой план питания"
        
        view.backgroundColor = .white
        
        UIApplication.shared.statusBarStyle = .lightContent
        setupViews()
    }
    
    func setupViews() {
        
        view.addSubview(timelineNavBar)
        timelineNavBar.anchor(topLayoutGuide.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 80)
        
        view.addSubview(errorLabel)
        errorLabel.anchor(nil, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 40, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 0)
        errorLabel.anchorCenterYToSuperview()
        
        view.addSubview(tableView)
        tableView.anchor(timelineNavBar.bottomAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    func updateTableView(withScroll: Bool = false) {
        
        guard let meals = meals else { return }
        
        tableDirector?.clear()
        
        for meal in meals {
            
            let section = TableSection()
            
            let titleHeader = TableRow<SelfSizedCell>(item: (title: meal.title, font: AppFont.medium(size: 22), topInset: nil, botInset: nil, withSeparator: false))
            
            section.append(row: titleHeader)
            
            if let dishes = meal.getOrderedDishes(context: currentContext) {
                
                for dish in dishes {
                    
                    if dish.getReciept() != nil {
                        
                        let openReciept = TableRowAction<DishClickableImageTitleCell>(.custom(DishCellActions.openRecieptClicked)) { (cellOption) -> Void in
                            self.showRecieptDidTap(dishId: dish.id, planId: dish.planId, dishIdForReciept: dish.dishId)
                        }
                        
                        let clickableImageRow = TableRow<DishClickableImageTitleCell>.init(item: (imageUrl: dish.photoUrl, dishTitle: dish.title), actions: [openReciept])
                        section.append(row: clickableImageRow)
                        
                    } else {
                        let imageRow = TableRow<DishImageTitleCell>.init(item: (imageUrl: dish.photoUrl, dishTitle: dish.title))
                        section.append(row: imageRow)
                    }
                    
                    if dish.cookOnDays > 0 {
                        let attentionRow = TableRow<DishAttentionPlateCell>.init(item: Int(dish.cookOnDays))
                        section.append(row: attentionRow)
                    }
                    
                    if dish.calories > 0 {
                        let nutritionRow = TableRow<DishNutritionsCell>.init(item: (calories: dish.calories,
                                                                                    percFats: dish.percFats,
                                                                                    percCarbohydrates: dish.percCarbohydrates,
                                                                                    percProteins: dish.percProteins,
                                                                                    cookTime: dish.cookTime,
                                                                                    proteins: dish.proteins,
                                                                                    fats: dish.fats,
                                                                                    carbohydrates: dish.carbohydrates,
                                                                                    size: dish.size))
                        section.append(row: nutritionRow)
                    }
                    
                    if dish.cookTime > 0 {
                        let cookTimeRow = TableRow<DishCookingTimeCell>.init(item: (cookTime: dish.cookTime, activeCookTime: dish.activeCookTime))
                        section.append(row: cookTimeRow)
                    }
                    
                    let rateDish = TableRowAction<DishButtonsDefaultCell>(.custom(DishCellActions.rateBtnClicked)) { (cellOption) -> Void in
                        
                        if let userInfo = cellOption.userInfo, let status = Array(userInfo.values).first as? CompleteStatus  {
                            self.eatingStatusChanged(status: status, planId: dish.planId, dishId: dish.id, completion: {_ in })
                        }
                        self.updateTableView()
                    }
                    
                    if dish.isEaten == CompleteStatus.none.rawValue {
                        
                        let eatButtonsRow = TableRow<DishButtonsDefaultCell>.init(item: (leftTitle: "Я это уже съела", rightTitle: "Я это не съела"), actions: [rateDish])
                        section.append(row: eatButtonsRow)
                        
                    } else if dish.isEaten == CompleteStatus.yes.rawValue {
                        
                        let doneButtonsRow = TableRow<DishButtonsDoneCell>.init(item: "Вы это уже съели")
                        section.append(row: doneButtonsRow)
                        
                    } else {
                        
                        let doneButtonsRow = TableRow<DishButtonsDoneCell>.init(item: "Вы это не съели")
                        section.append(row: doneButtonsRow)
                    }
                }
            }
            
            tableDirector?.append(section: section)
        }
        
        if meals.count > 0 {
            
            let section = TableSection()
            let footerRow = TableRow<DietPlanFooter>(item: currentMealPlanId)
            section.append(row: footerRow)
            
            tableDirector?.append(section: section)
        }
        
        tableDirector?.reload()
        
        if withScroll {
            
            if tableView.numberOfSections > 0 {
                tableView.layoutIfNeeded()
                let indexPath = IndexPath(row: 0, section: 0)
                tableView.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
    }
    
    func showRecieptDidTap(dishId: Int32?, planId: Int32?, dishIdForReciept: Int32?) {
        
        guard let id = dishIdForReciept else { return }
        
        let savedReciept: Reciept? = Reciept.attemptToFetch(withId: Int(id), context: AppDelegate.currentContext)
        
        if reachability.isReachable || savedReciept != nil {
            let vc = RecieptController()
            vc.data = (dishId, planId, dishIdForReciept)
            navigationController?.pushViewController(vc, animated: true)
            
        } else {
            showErrorAlert(message: "The Internet connection appears to be offline")
        }
    }

}

extension DietController: TimelineNavBarDeleagte {
    
    func didSelectetItem(atIndex index: Int, withDate date: Date) {
        
        self.currentDate = date
        
        if index != currentSelectedDayIndex {
            fetchMeals(withDate: date, withScroll: true)
        }
        
        currentSelectedDayIndex = index
    }
    
    func fetchMeals(withDate date: Date, withScroll: Bool = false) {
        
        startLoading()
        
        ModelManager.sharedManager.getDietPlan(forSelectedDate: date)
            .then { (mealPlanId, context) -> Void in
                
                self.currentMealPlanId = Int(mealPlanId!)
                self.currentContext = context
                
                DispatchQueue.main.async {
                    self.updateTableView(withScroll: withScroll)
                }
            }
            .always {
                self.stopLoading()
                
            }.catch { (error) in

                self.currentMealPlanId = nil
                self.currentContext = nil
                
                DispatchQueue.main.async {
                    self.updateTableView(withScroll: withScroll)
                }
                
                if case let ResponseError.withMessage(msg) = error {
                    //self.showErrorAlert(message: msg)
                    self.errorLabel.text = msg
                }
            }
        }
}

extension DietController {
    
    typealias StatusCompletion = (Bool) -> ()
    
    func updateDish(planId: Int32?, status: CompleteStatus) {
        
        guard let planId = planId else { return }
        
        let dish: Dish? = Dish.attemptToFetch(withId: Int(planId), context: AppDelegate.currentContext)
        dish?.isEaten = status.rawValue
        NSManagedObject.attemptToSave()
    }
    
    func eatingStatusChanged(status: CompleteStatus, planId: Int32?, dishId: Int32?, completion: @escaping StatusCompletion) {
        
        if status == .yes {
            
            let alertView = CustomAlertView()
            alertView.showRateView()
            alertView.sendRatePressed = { comment in
                
                completion(true)
                
                self.updateDish(planId: planId, status: status)
                
                ApiService.sharedService.postSendStatus(status: status, planId: planId, comment: comment) { error in
                    
                    if let error = error {
                        CommentsManager.sharedManager.queueComment(status: status, planId: planId, comment: comment)
                        print(error)
                        DispatchQueue.main.async {
                            alertView.hideRateView()
                        }
                        return
                    }
                    
                    if let date = self.currentDate {
                        self.fetchMeals(withDate: date)
                    }
                    DispatchQueue.main.async {
                        alertView.hideRateView()
                    }
                }
            }
            
        } else if status == .no {
            
            let alertView = CustomAlertView()
            alertView.showAnswerView()
            alertView.sendRatePressed = { comment in
                
                completion(false)
                
                self.updateDish(planId: planId, status: status)
                
                ApiService.sharedService.postSendStatus(status: status, planId: planId, comment: comment) { error in
                    
                    if let error = error {
                        CommentsManager.sharedManager.queueComment(status: status, planId: planId, comment: comment)
                        print(error)
                        DispatchQueue.main.async {
                            alertView.hideRateView()
                        }
                        return
                    }
                    
                    if let date = self.currentDate {
                        self.fetchMeals(withDate: date)
                    }
                    DispatchQueue.main.async {
                        alertView.hideAnswerView()
                    }
                }
            }
        }
    }
}
