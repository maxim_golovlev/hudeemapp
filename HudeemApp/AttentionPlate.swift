//
//  AttentionPlate.swift
//  HudeemApp
//
//  Created by Admin on 16.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class AttentionPlate: UIView {
    
    let backView: UIView = {
        let backView = UIView()
        backView.translatesAutoresizingMaskIntoConstraints = false
        backView.layer.cornerRadius = 19
        backView.backgroundColor = whiteGreen
        return backView
    }()
    
    let iImageView = UIImageView(image: #imageLiteral(resourceName: "exclamation_green"))
    
    let label: UILabel = {
        let label = UILabel()
        label.font = AppFont.bold(size: 11)
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 2
        label.textColor = lightGreen
        return label
    }()
    
    var days: Int? {
        didSet {
            let dateString: String
            switch days {
            case .some(1): dateString = "1 день"
            case .some(2): dateString = "2 дня"
            case .some(3): dateString = "3 дня"
            case .some(4): dateString = "4 дня"
            case _ where days ?? 0 > 4: dateString = "5 дней"
            default: dateString = ""
            }
            
            label.text = "Обратите внимание, это блюдо готовим сразу на \(dateString)!"
            
            iImageView.isHidden = days ?? 0 > 0 ? false : true
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        addSubview(backView)
        backView.fillSuperview()
        
        backView.addSubview(iImageView)
        iImageView.anchor(nil, left: backView.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 17, heightConstant: 17)
        iImageView.centerYAnchor.constraint(equalTo: backView.centerYAnchor).isActive = true
        
        backView.addSubview(label)
        label.anchor(backView.topAnchor, left: iImageView.rightAnchor, bottom: backView.bottomAnchor, right: backView.rightAnchor, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

