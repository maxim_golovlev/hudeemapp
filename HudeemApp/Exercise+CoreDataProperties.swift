//
//  Exercise+CoreDataProperties.swift
//  HudeemApp
//
//  Created by Admin on 29.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData


extension Exercise {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Exercise> {
        return NSFetchRequest<Exercise>(entityName: "Exercise")
    }

    @NSManaged public var comment: String?
    @NSManaged public var complete: String?
    @NSManaged public var id: Int32
    @NSManaged public var pauseTime: Int32
    @NSManaged public var pauseTimeSec: Int32
    @NSManaged public var photoUrl: String?
    @NSManaged public var time: Int32
    @NSManaged public var title: String?
    @NSManaged public var trainerComment: String?
    @NSManaged public var planId: Int32
    @NSManaged public var params: NSSet?
    @NSManaged public var training: Training?

}

// MARK: Generated accessors for params
extension Exercise {

    @objc(addParamsObject:)
    @NSManaged public func addToParams(_ value: ExerciseParams)

    @objc(removeParamsObject:)
    @NSManaged public func removeFromParams(_ value: ExerciseParams)

    @objc(addParams:)
    @NSManaged public func addToParams(_ values: NSSet)

    @objc(removeParams:)
    @NSManaged public func removeFromParams(_ values: NSSet)

}
