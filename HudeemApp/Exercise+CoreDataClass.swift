//
//  Exercise+CoreDataClass.swift
//  HudeemApp
//
//  Created by Admin on 23.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData

public class Exercise: NSManagedObject {

    static func create(withData data: Data?, context: NSManagedObjectContext) -> Exercise? {
        
        guard let data = data else { return nil }
        
        do {
            if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject] {
                
                if let error = json["error"] {
                    print(error)
                    return nil
                }
                
                let exercise: Exercise = Exercise.createOrReturn(withId: json["training_exercise_id"] as? Int, context: context)
                exercise.initWithDict(dict: json, context: context)
                exercise.createAndAddRelationObjects(dict: json, context: context)
                return exercise
                
            }
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        return nil
    }
    
    static func create(withDict dict: [String: AnyObject], context: NSManagedObjectContext) -> Exercise {
        
        let exercise: Exercise = Exercise.createOrReturn(withId: dict["training_exercise_id"] as? Int, context: context)
        exercise.initWithDict(dict: dict, context: context)
        exercise.createAndAddRelationObjects(dict: dict, context: context)
        return exercise
    }
    
    func createAndAddRelationObjects(dict: [String: Any], context: NSManagedObjectContext) {
        
        if let paramsDict = dict["params"] as? [[String: AnyObject]] {
            
            for paramDict in paramsDict {
                
                let params = ExerciseParams.create(withDict: paramDict, context: context)
                params?.exercise = self
            }
        }
        
        NSManagedObject.attemptToSave(context: context)
    }
    
    private func initWithDict(dict: [String: AnyObject], context: NSManagedObjectContext) {
        
        if let id = dict["training_exercise_id"] as? Int32 {
            self.id = id
        }
        
        self.title = dict["title"] as? String
        self.photoUrl = dict["photo"] as? String
        
        if let pauseTime = dict["pause_time"] as? Int32 {
            self.pauseTime = pauseTime
        }
        
        if let pauseTimeSec = dict["pause_time_sec"] as? Int32 {
            self.pauseTimeSec = pauseTimeSec
        }
        
        if let time = dict["time"] as? Int32 {
            self.time = time
        }
        
        self.trainerComment = dict["trainer_comment"] as? String
        
        self.complete = dict["complete"] as? String
        
        self.comment = dict["comment"] as? String
        
        if let planId = dict["plan_id"] as? Int32 {
            self.planId = planId
        }
        
        NSManagedObject.attemptToSave(context: context)
    }
    
    func getOrderedParams(context: NSManagedObjectContext?) -> [ExerciseParams]? {
        
        guard let context = context else { return nil }
        
        let predicate = NSPredicate(format: "exercise.id = %d", self.id)
        let sortDescriptors = [NSSortDescriptor(key: "sort", ascending: true)]
        
        let params: [ExerciseParams]? = ExerciseParams.fetch(predicate: predicate, sortDescriptors: sortDescriptors, context: context)
        
        return params
    }
}
