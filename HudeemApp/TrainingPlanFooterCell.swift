//
//  TrainingPlanFooterCell.swift
//  HudeemApp
//
//  Created by Admin on 29.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import TableKit

class TrainingPlanFooterCell: BaseTableCell, ConfigurableCell {
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = AppFont.bold(size: 15)
        label.textColor = purpleColor
        return label
    }()
    
    let subtitleLabel: UILabel = {
        let label = UILabel()
        label.font = AppFont.roman(size: 15)
        label.numberOfLines = 0
        return label
    }()
    
    lazy var fbButton: UIButton = {
        let button = UIButton(type: .system)
        button.setBackgroundImage(#imageLiteral(resourceName: "Fb_icon"), for: .normal)
        button.tag = 0
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return button
    }()
    
    lazy var vkButton: UIButton = {
        let button = UIButton(type: .system)
        button.setBackgroundImage(#imageLiteral(resourceName: "Vk_icon"), for: .normal)
        button.tag = 1
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return button
    }()
    
    lazy var instButton: UIButton = {
        let button = UIButton(type: .system)
        button.setBackgroundImage(#imageLiteral(resourceName: "Instagram_icon"), for: .normal)
        button.tag = 2
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return button
    }()
    
    lazy var odnButton: UIButton = {
        let button = UIButton(type: .system)
        button.setBackgroundImage(#imageLiteral(resourceName: "odnoklassniki_icon"), for: .normal)
        button.tag = 3
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return button
    }()
    
    lazy var whatsButton: UIButton = {
        let button = UIButton(type: .system)
        button.setBackgroundImage(#imageLiteral(resourceName: "WhatsApp_icon"), for: .normal)
        button.tag = 4
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return button
    }()
    
    lazy var twitButton: UIButton = {
        let button = UIButton(type: .system)
        button.setBackgroundImage(#imageLiteral(resourceName: "twitter_icon"), for: .normal)
        button.tag = 5
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return button
    }()
    
    let scrollView: UIScrollView = {
        let sv = UIScrollView()
        sv.showsHorizontalScrollIndicator = false
        return sv
    }()
    
    let horizontalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.spacing = 15
        return stackView
    }()
    
    static var estimatedHeight: CGFloat? {
        return 60
    }
    
    static var defaultHeight: CGFloat? {
        return UITableViewAutomaticDimension
    }
    
    func configure(with data: (title: String?, subtitle: String?)) {
        titleLabel.text = data.title
        subtitleLabel.text = data.subtitle
    }
    
    
    override func setupViews() {
        
        addSubview(titleLabel)
        addSubview(subtitleLabel)
        addSubview(scrollView)
        scrollView.addSubview(horizontalStackView)
        
        horizontalStackView.addArrangedSubview(fbButton)
        horizontalStackView.addArrangedSubview(vkButton)
        horizontalStackView.addArrangedSubview(instButton)
        horizontalStackView.addArrangedSubview(odnButton)
        horizontalStackView.addArrangedSubview(whatsButton)
        horizontalStackView.addArrangedSubview(twitButton)
        
        _ = horizontalStackView.arrangedSubviews.map { $0.translatesAutoresizingMaskIntoConstraints = false; $0.widthAnchor.constraint(equalToConstant: 42).isActive = true }
        
        addConstraintsWithFormat("H:|-23-[v0]-23-|", views: titleLabel)
        addConstraintsWithFormat("H:|-23-[v0]-23-|", views: subtitleLabel)
        addConstraintsWithFormat("H:|-23-[v0]-23-|", views: scrollView)
        scrollView.addConstraintsWithFormat("H:|[v0]|", views: horizontalStackView)
        
        addConstraintsWithFormat("V:|-25-[v0]-10-[v1]-10-[v2(42)]-30-|", views: titleLabel, subtitleLabel, scrollView)
        scrollView.addConstraintsWithFormat("V:|[v0(42)]|", views: horizontalStackView)
    }
    
    func buttonTapped() {
        
        
    }
    
}
