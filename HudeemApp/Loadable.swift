//
//  Loadable.swift
//  HudeemApp
//
//  Created by Admin on 21.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import MBProgressHUD
import UIKit

protocol Loadable {
    func startLoading()
    func stopLoading()
}

extension Loadable where Self: UIViewController {
    func startLoading() {
        DispatchQueue.main.async {
            MBProgressHUD.showAdded(to: self.view, animated: false)
        }
    }
    
    func stopLoading() {
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: self.view, animated: false)
        }
    }
}

