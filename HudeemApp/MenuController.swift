//
//  MenuController.swift
//  HudeemApp
//
//  Created by Admin on 21.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class MenuController: UIViewController {
    
    let cellId = "cellId"
    
    var titles = [MenuTitle(icon: #imageLiteral(resourceName: "avatar_icon") ,title: "Константиновский"),
                  MenuTitle(icon: #imageLiteral(resourceName: "message_icon").withRenderingMode(.alwaysTemplate) ,title: "Сообщения"),
                  MenuTitle(icon: #imageLiteral(resourceName: "eating_icon").withRenderingMode(.alwaysTemplate) ,title: "План питания"),
                  MenuTitle(icon: #imageLiteral(resourceName: "train_icon").withRenderingMode(.alwaysTemplate) ,title: "План тренировок"),
                  MenuTitle(icon: #imageLiteral(resourceName: "results_icon").withRenderingMode(.alwaysTemplate),title: "Мои результаты"),
                  MenuTitle(icon: #imageLiteral(resourceName: "buy_icon").withRenderingMode(.alwaysTemplate) ,title: "План покупок"),
                  MenuTitle(icon: #imageLiteral(resourceName: "sub_icon").withRenderingMode(.alwaysTemplate) ,title: "Продление подписки")]
    
    lazy var tableView: UITableView = {
        let tv = UITableView()
        tv.register(MenuCell.self, forCellReuseIdentifier: self.cellId)
        tv.dataSource = self
        tv.delegate = self
        tv.tableFooterView = UIView()
        tv.separatorStyle = .none
        tv.selectRow(at: IndexPath(row: 2, section: 0) , animated: true, scrollPosition: .top)
        return tv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        setupViews()
        
        
    }
    
    func setupViews() {
        
        view.addSubview(tableView)
        
        view.addConstraintsWithFormat("H:|[v0]|", views: tableView)
        view.addConstraintsWithFormat("V:|-40-[v0]|", views: tableView)
    }
    
}

extension MenuController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath) as! MenuCell
        cell.menuTitle = titles[indexPath.row]
        cell.iconWidthConstraint?.constant = indexPath.row == 0 ? 30 : 22
        cell.iconLeftConstraint?.constant = indexPath.row == 0 ? 12 : 20
        return cell
    }
}

extension MenuController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {        
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! MenuCell
        cell.menuImageView.tintColor = purpleColor
        
        let vc: UIViewController?
        
        switch indexPath.row {
        case 0:
            vc = ProfileController()
        case 1:
            vc = MessagesFeedController()
        case 2:
            vc = DietController()
        case 3:
            vc = TrainController()
        case 4:
            vc = ResultsController()
        case 5:
            vc = PurchaseController()
        case 6:
            vc = SubscribeController()
        default:
            vc = nil
        }
        
        if let vc = vc {
            let navVC = UINavigationController(rootViewController: vc)
            vc.title = cell.menuTitle?.title
            
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            let container = appdelegate.centerContainer
            container?.centerViewController = navVC
            container?.toggleDrawerSide(.left, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! MenuCell
        cell.menuImageView.tintColor = .black
    }
}
