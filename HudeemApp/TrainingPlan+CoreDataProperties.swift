//
//  TrainingPlan+CoreDataProperties.swift
//  HudeemApp
//
//  Created by Admin on 23.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData


extension TrainingPlan {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TrainingPlan> {
        return NSFetchRequest<TrainingPlan>(entityName: "TrainingPlan")
    }

    @NSManaged public var lastUpdated: NSDate?
    @NSManaged public var id: Int32
    @NSManaged public var date: NSDate?
    @NSManaged public var trainings: NSSet?

}

// MARK: Generated accessors for trainings
extension TrainingPlan {

    @objc(addTrainingsObject:)
    @NSManaged public func addToTrainings(_ value: Training)

    @objc(removeTrainingsObject:)
    @NSManaged public func removeFromTrainings(_ value: Training)

    @objc(addTrainings:)
    @NSManaged public func addToTrainings(_ values: NSSet)

    @objc(removeTrainings:)
    @NSManaged public func removeFromTrainings(_ values: NSSet)

}
