//
//  DishAttentionPlaneCell.swift
//  HudeemApp
//
//  Created by Admin on 03.05.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import TableKit

class DishAttentionPlateCell: BaseTableCell, ConfigurableCell {
    
    let attentionView: AttentionPlate = {
        let plate = AttentionPlate()
        return plate
    }()
    
    static var estimatedHeight: CGFloat? {
        return 20
    }
    
    static var defaultHeight: CGFloat? {
        return UITableViewAutomaticDimension
    }
    
    
    func configure(with days: Int? ) {
        attentionView.days = days
    }
    
    override func setupViews() {
        
        addSubview(attentionView)
        attentionView.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 20, leftConstant: 18, bottomConstant: 0, rightConstant: 18, widthConstant: 0, heightConstant: 38)
    }
}
