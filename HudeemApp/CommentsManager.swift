//
//  CommentsManager.swift
//  HudeemApp
//
//  Created by Admin on 21.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData
import ReachabilitySwift

class CommentsManager {
    
    static let sharedManager = CommentsManager.init()
    
    let reachability = Reachability()!
    
    static func uploadQueqedComments(context: NSManagedObjectContext = AppDelegate.currentContext) {
        
        let comments: [Comment]? = NSManagedObject.fetch(context: AppDelegate.currentContext)
        
        comments?.forEach({ (comment) in
            
            let status = CompleteStatus.from(string: comment.status)
            
            ApiService.sharedService.postSendStatus(status: status, planId: comment.planId, comment: comment.comment) { error in
                
                if let error = error {
                    print(error)
                    return
                }
                
                context.delete(comment)
            }
        })
    }
    
    func queueComment(status: CompleteStatus, planId: Int32?, comment: String?, context: NSManagedObjectContext = AppDelegate.currentContext) {
        
        let coment = NSEntityDescription.insertNewObject(forEntityName: "Comment", into: context) as! Comment
        coment.initWith(status: status, planId: planId, comment: comment)
        NSManagedObject.attemptToSave(context: context)
    }
    
    init() {
        
        if reachability.isReachable {
            CommentsManager.uploadQueqedComments()
        }
        
        reachability.whenReachable = { reachability in
            CommentsManager.uploadQueqedComments()
        }
    }
    
    deinit {
        
        
    }
}
