//
//  LoginController+Handler.swift
//  HudeemApp
//
//  Created by Admin on 25.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData

extension LoginController: Loadable, Alertable {
    
    
    func enterHandler() {
        
        guard let text = emailTextField.text else { return }
        if !text.isValidEmail() { return }
        
        startLoading()
        
        ApiService.sharedService.login(withEmail: emailTextField.text, password: passTextField.text) { [unowned self] (user, error) in
            
            if let error = error {
                
                if case let ResponseError.withMessage(msg) = error {
                    self.showErrorAlert(message: msg)
                    
                } else {
                    self.showErrorAlert(message: error.localizedDescription)
                }
                
                self.stopLoading()
                return
            }
            
            if user != nil {
                if let completion = self.dismissCompletion {
                    completion()
                }
                self.dismiss(animated: true, completion: nil)
            }
            
            print(user.debugDescription)
        }
        
    }
    
    func restorePasHandler() {        
        
        
    }
    
    func regHandler() {

        
    }
    
    
}
