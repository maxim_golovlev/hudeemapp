//
//  CenterController.swift
//  HudeemApp
//
//  Created by Admin on 21.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import DrawerController

class CenterController: UIViewController, Alertable, Loadable {

    var drawer: DrawerController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        automaticallyAdjustsScrollViewInsets = false
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "burger"), style: .plain, target: self, action: #selector(handleMenu))
        
        drawer = drawerController()
        drawer?.gestureCompletionBlock = { vc, gesture in
            self.refreshStatusBar()
        }
    }
    
    func refreshStatusBar() {
        UIApplication.shared.statusBarStyle = drawer?.visibleLeftDrawerWidth == 0 ? .lightContent : .default
    }

    func handleMenu() {
        guard let draver = self.drawerController() else { return }
        draver.toggleDrawerSide(.left, animated: true, completion: nil)
        refreshStatusBar()
    }

}
