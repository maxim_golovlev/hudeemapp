//
//  PhotoManager.swift
//  HudeemApp
//
//  Created by Admin on 27.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit


class PhotoManager {
    
    static let sharedManager = PhotoManager()
    
    func savePhoto(data: Data?, urlString: String?) {
        
        AppDelegate.appPersistentContainer.performBackgroundTask { (context) in
            
            StoredImage.save(data: data, urlString: urlString, context: context)
        }
    }
    
    func getPhoto(urlString: String?) -> UIImage? {
            
        if let imageObject: StoredImage = StoredImage.attemptToFetch(withId: urlString, context: AppDelegate.currentContext), let data = imageObject.imageData as Data? {
            
            let image = UIImage(data: data)
            return image
        }
        return nil
    }
    
    func removeAllPhotos() {
        
        AppDelegate.appPersistentContainer.performBackgroundTask { (context) in
        
            if let imageData: [StoredImage] = StoredImage.fetch(context: context) {
                
                imageData.forEach({ (imageData) in
                    context.delete(imageData)
                })
            }
        }
    }
}
