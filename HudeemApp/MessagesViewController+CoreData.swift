//
//  MessagesViewController+CoreData.swift
//  ElvisProject
//
//  Created by Admin on 07.02.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import CoreData

extension MessagesFeedController {
    
    func clearCoreData() {
        
        guard let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext else { return }
        
        do {
            let entities = ["Friend", "Message"]
            
            for entity in entities {
                
                let fetchReques = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
                let objects = try context.fetch(fetchReques) as? [NSManagedObject]
                for object in objects! {
                    context.delete(object)
                }
            }
            try context.save()
            
        } catch let error {
            print(error)
        }
    }
    
    func fillCoredataWithFriends() {
        
        let currentContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let Pavel = addFriend(withName: "Pavel Bocharov", imageName: nil, context: currentContext)
        _ = MessagesFeedController.addMessage(withText: "Hello", fromFriend: Pavel, minutesAgo: 5, context: currentContext)
        _ = MessagesFeedController.addMessage(withText: "I want to play in picaboo", fromFriend: Pavel, minutesAgo: 4, context: currentContext)
        _ = MessagesFeedController.addMessage(withText: "Pick up your phone", fromFriend: Pavel, minutesAgo: 1, context: currentContext)
        _ = MessagesFeedController.addMessage(withText: "Do you even listen to me?", fromFriend: Pavel, minutesAgo: 2, context: currentContext)
        
        let Ashot = addFriend(withName: "Ashot Asoyan", imageName: nil, context: currentContext)
        _ = MessagesFeedController.addMessage(withText: "Hello", fromFriend: Ashot, minutesAgo: 5, context: currentContext, image: #imageLiteral(resourceName: "sample_image"))
        _ = MessagesFeedController.addMessage(withText: "I want to play hide and seek", fromFriend: Ashot, minutesAgo: 4, context: currentContext)
        
        let Akhat = addFriend(withName: "Akhat Gaysin", imageName: nil, context: currentContext)
        _ = MessagesFeedController.addMessage(withText: "Hello", fromFriend: Akhat, minutesAgo: 5, context: currentContext)
        _ = MessagesFeedController.addMessage(withText: "I want to play some music", fromFriend: Akhat, minutesAgo: 4, context: currentContext)
        _ = MessagesFeedController.addMessage(withText: "Come to me and grab a guitar", fromFriend: Akhat, minutesAgo: 3, context: currentContext)
        
        
        do {
            try currentContext.save()
        } catch let error {
            print(error)
        }
    }
    
    func addFriend(withName name: String, imageName: String?, context: NSManagedObjectContext) -> Friend {
        
        let friend = NSEntityDescription.insertNewObject(forEntityName: "Friend", into: context) as! Friend
        friend.name = name
        friend.profileImage = imageName
        return friend
    }
    
    static func addMessage(withText text: String, fromFriend friend: Friend, minutesAgo: Double, context: NSManagedObjectContext, isSender: Bool = true, image: UIImage? = nil) -> Message {
        
        let message = NSEntityDescription.insertNewObject(forEntityName: "Message", into: context) as! Message
        message.friend = friend
        message.text = text
        message.date = Date().addingTimeInterval(-minutesAgo * 60) as NSDate?
        message.isSender = isSender
        if let image = image {
            message.image = UIImageJPEGRepresentation(image, 0.3) as NSData?
        }
        friend.lastMessage = message
        return message
    }
    
}
