//
//  Meal+CoreDataClass.swift
//  HudeemApp
//
//  Created by Admin on 26.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData


public class Meal: NSManagedObject {
    
    static func create(withDict dict: [String: AnyObject], context: NSManagedObjectContext) -> Meal {
        
        let meal: Meal = Meal.createOrReturn(withId: dict["mealtime_id"] as? Int, context: context)
        meal.initWithDict(dict: dict, context: context)
        
        return meal
    }
    
    private func initWithDict(dict: [String: AnyObject], context: NSManagedObjectContext) {
        
        self.title = dict["title"] as? String
        
        if let id = dict["mealtime_id"] as? Int32 {
            self.id = id
        }
        
        if let sort = dict["sort"] as? Int32 {
            self.sort = sort
        }
        
        NSManagedObject.attemptToSave(context: context)
    }
    
    func getOrderedDishes(context: NSManagedObjectContext?) -> [Dish]? {
        
        guard let context = context else { return nil }
        
        let predicate = NSPredicate(format: "meal.id = %d", self.id)
        let sortDescriptors = [NSSortDescriptor(key: "sort", ascending: true)]
        
        let dishes: [Dish]? = Dish.fetch(predicate: predicate, sortDescriptors: sortDescriptors, context: context)
        
        return dishes
    }
}
