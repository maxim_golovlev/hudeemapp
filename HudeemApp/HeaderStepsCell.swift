//
//  HeaderStepsCell.swift
//  HudeemApp
//
//  Created by Admin on 16.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class HeaderCookingStepCell: BaseCollectionCell {
    
    let imageView: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "cooking_hat"))
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    let verticalLine: UIView = {
        let view = UIView()
        view.backgroundColor = purpleColor
        return view
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = AppFont.medium(size: 22)
        label.text = "Приготовление"
        return label
    }()
    
    override func setupViews() {
        
        addSubview(imageView)
        addSubview(titleLabel)
        
        addConstraintsWithFormat("H:|-20-[v0(40)]-12-[v1]|", views: imageView, titleLabel)
        imageView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        imageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        titleLabel.anchorCenterYToSuperview()
        
        addSubview(verticalLine)
        verticalLine.anchor(imageView.bottomAnchor, left: nil, bottom: bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 1, heightConstant: 0)
        verticalLine.centerXAnchor.constraint(equalTo: imageView.centerXAnchor).isActive = true
    }
    
    static func getHeight() -> CGFloat {
        return 70
    }
}
