//
//  RoundedButton.swift
//  HudeemApp
//
//  Created by Admin on 24.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class RoundedButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 10)
        layer.cornerRadius = 37/2
        layer.masksToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
