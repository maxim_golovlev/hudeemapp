//
//  ExerciseParams+CoreDataClass.swift
//  HudeemApp
//
//  Created by Admin on 23.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData


public class ExerciseParams: NSManagedObject {
    
    static func create(withDict dict: [String: AnyObject], context: NSManagedObjectContext) -> ExerciseParams? {
        
        let params: ExerciseParams = ExerciseParams.createOrReturn(withId: dict["id"] as? String, context: context)
        params.initWithDict(dict: dict, context: context)
        return params
    }
    
    private func initWithDict(dict: [String: AnyObject], context: NSManagedObjectContext) {

        if let time = dict["exercise_time"] as? Int32 {
            self.time = time
        }
        
        if let weight = dict["weight"] as? Int32 {
            self.weight = weight
        }
        
        if let count = dict["count"] as? Int32 {
            self.count = count
        }
        
        if let id = dict["id"] as? String {
            self.id = id
        }
        
        if let sort = dict["sort"] as? Int32 {
            self.sort = sort
        }
        
        NSManagedObject.attemptToSave(context: context)
    }
    
    func getTitleHeader() -> String {
        
        if self.weight > 0, self.count > 0 {
            return "Вес/повторения"
        }
        
        if self.count > 0 {
            return "Повторения"
        }
        
        if self.time > 0 {
            return "Продолжительность"
        }
        
        return ""
    }
    
    func getSubtitle() -> String {
        
        if self.weight > 0, self.count > 0 {
            return "\(self.weight) кг. x \(self.count) раз"
        }
        
        if self.count > 0 {
            return "\(self.count) раз"
        }
        
        if self.time > 0 {
            return "\(self.time) мин."
        }
        
        return ""
    }
}
