//
//  Dish+CoreDataClass.swift
//  HudeemApp
//
//  Created by Admin on 26.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import CoreData

public class Dish: NSManagedObject {
    
    static func create(withData data: Data?, context: NSManagedObjectContext) -> Dish? {
        
        guard let data = data else { return nil }
        
        do {
            if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject] {
                
                if let error = json["error"] {
                    print(error)
                    return nil
                }

                let dish: Dish = Dish.createOrReturn(withId: json["dish_id"] as? Int, context: context)
                dish.initWithDict(dict: json, context: context)
                return dish

            }
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        return nil
    }
    
    static func create(withDict dict: [String: AnyObject], context: NSManagedObjectContext) -> Dish {
        
        let dish: Dish = Dish.createOrReturn(withId: dict["plan_id"] as? Int, context: context)
        dish.initWithDict(dict: dict, context: context)
        return dish
    }
    
    private func initWithDict(dict: [String: AnyObject], context: NSManagedObjectContext) {
        
        self.title = dict["title"] as? String
        self.photoUrl = dict["photo"] as? String
        self.level = dict["level"] as? String
        self.sizeScale = dict["size_scale"] as? String
        
        self.isEaten = dict["complete"] as? String
        
        if let dishId = dict["plan_id"] as? Int32 {
            self.id = dishId
        }
        if let proteins = dict["proteins"] as? Int32 {
            self.proteins = proteins
        }
        if let fats = dict["fats"] as? Int32 {
            self.fats = fats
        }
        if let carbohydrates = dict["carbohydrates"] as? Int32 {
            self.carbohydrates = carbohydrates
        }
        if let calories = dict["calories"] as? Int32 {
            self.calories = calories
        }
        if let planId = dict["plan_id"] as? Int32 {
            self.planId = planId
        }
        if let cookTime = dict["cook_time"] as? Int32 {
            self.cookTime = cookTime
        }
        if let activeCookTime = dict["active_cook_time"] as? Int32 {
            self.activeCookTime = activeCookTime
        }
        if let size = dict["size"] as? Int32 {
            self.size = size
        }
        
        if let sort = dict["sort"] as? Int32 {
            self.sort = sort
        }
        
        if let percProteins = dict["proteins_proc"] as? Float {
            self.percProteins = percProteins
        }
        if let percFats = dict["fats_proc"] as? Float {
            self.percFats = percFats
        }
        if let percCarbohydrates = dict["carbohydrates_proc"] as? Float {
            self.percCarbohydrates = percCarbohydrates
        }
        
        if let cookOnDays = dict["cook_on_days"] as? Int32 {
            self.cookOnDays = cookOnDays
        }
        
        if let dishId = dict["dish_id"] as? Int32 {
            self.dishId = dishId
        }

        
        NSManagedObject.attemptToSave(context: context)
        
    }
    
    func getReciept() -> Reciept? {
        
        let reciept: Reciept? = NSManagedObject.attemptToFetch(withId: Int(self.dishId), context: AppDelegate.currentContext)
        return reciept
    }
    
}
