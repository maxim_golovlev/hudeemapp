//
//  FeedMessageCell.swift
//  ElvisProject
//
//  Created by Admin on 04.02.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import UIKit

class FeedMessageCell: BaseTableCell {
    
    override var isHighlighted: Bool {
        didSet {
            backgroundColor = isHighlighted ? UIColor(red: 0, green: 134/255, blue: 249/255, alpha: 1) : UIColor.white
            senderName.textColor = isHighlighted ? UIColor.white : UIColor.black
            timeLabel.textColor = isHighlighted ? UIColor.white : UIColor.lightGray
            senderMessageLabel.textColor = isHighlighted ? UIColor.white : UIColor.lightGray
            borderLine.backgroundColor = isHighlighted ? UIColor.white : UIColor.lightGray
        }
    }
    
    var message: Message? {
        didSet {
            
            if let image = message?.friend?.profileImage {
                 avatar.image = UIImage(named: image)
            }
            
            if let name = message?.friend?.name {
                senderName.text = name
            }
            
            if let text = message?.text {
                senderMessageLabel.text = text
            }
            
            if let time = message?.date as Date? {
                timeLabel.text = time.stringFromDate()
            }
            
            if let isSender = message?.isSender {
                lastMessageAvatarWidthContraint?.constant = isSender ? 0 : 20
            }
        }
    }
    
    let avatar: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 34
        imageView.layer.masksToBounds = true
        imageView.image = #imageLiteral(resourceName: "avatar_icon")
        return imageView
    }()
    
    let lastMessageAvatar: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 10
        imageView.layer.masksToBounds = true
        imageView.image = #imageLiteral(resourceName: "avatar_icon")
        return imageView
    }()
    
    let senderName: UILabel = {
        let label = UILabel()
        label.font = AppFont.medium(size: 16)
        return label
    }()
    
    var senderMessageLabel: UILabel = {
        let label = UILabel()
        label.font = AppFont.light(size: 14)
        label.textColor = UIColor.lightGray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let timeLabel: UILabel = {
        let label = UILabel()
        label.font = AppFont.medium(size: 16)
        label.textColor = UIColor.lightGray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let borderLine: UIView = {
        let line = UIView()
        line.backgroundColor = UIColor.lightGray
        return line
    }()
    
    var lastMessageAvatarWidthContraint: NSLayoutConstraint?
    
    override func setupViews() {
        
        addSubview(avatar)
        addSubview(senderName)
        addSubview(timeLabel)
        
        addConstraintsWithFormat("H:|-8-[v0(64)]-8-[v1]-8-[v2]-8-|", views: avatar, senderName, timeLabel)
        addConstraintsWithFormat("V:|-8-[v0]", views: timeLabel)
        addConstraintsWithFormat("V:|-8-[v0]", views: senderName)
        addConstraintsWithFormat("V:|-8-[v0(64)]", views: avatar)
        
        addSubview(borderLine)
        addConstraintsWithFormat("H:|[v0]|", views: borderLine)
        addConstraintsWithFormat("V:[v0(1)]|", views: borderLine)
        
        addSubview(lastMessageAvatar)
        lastMessageAvatar.anchor(senderName.bottomAnchor, left: avatar.rightAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 20)
        lastMessageAvatarWidthContraint = lastMessageAvatar.widthAnchor.constraint(equalToConstant: 20)
        lastMessageAvatarWidthContraint?.isActive = true
        
        addSubview(senderMessageLabel)
        senderMessageLabel.anchor(nil, left: lastMessageAvatar.rightAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 0)
        senderMessageLabel.centerYAnchor.constraint(equalTo: lastMessageAvatar.centerYAnchor).isActive = true

    }
}
