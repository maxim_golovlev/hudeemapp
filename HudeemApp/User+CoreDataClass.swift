//
//  User+CoreDataClass.swift
//  HudeemApp
//
//  Created by Admin on 25.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import CoreData

typealias UserCompletion = (user: User?, error: Error?)

public class User: NSManagedObject {
    
    static func currentUser() -> User? {
        
        do {
            let fetchReques = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
            let objects = try AppDelegate.currentContext.fetch(fetchReques) as? [User]
            return objects?.first
            
        } catch let error {
            print(error)
        }
        
        return nil
    }
    
    static func createUser(withData data: Data?) -> UserCompletion {
        
        guard let data = data else { return (nil, ResponseError.withMessage("user data incorrect")) }
        
        do {
            if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject] {
                
                if let error = json["error"] {
                    print(error)
                    return (nil, ResponseError.withMessage(error as? String))
                }
                
                User.deleteOldUser()
                
                let user = NSEntityDescription.insertNewObject(forEntityName: "User", into: AppDelegate.currentContext) as! User
                user.initWithDict(dict: json)
                
                NSManagedObject.attemptToSave()
                
                return (user, nil)
            }
            
        } catch let error {
            print(error.localizedDescription)
            
            return (nil, error)
        }
        
        return (nil, nil)
    }
    
    private func initWithDict(dict: [String: AnyObject]) {
        
        if let id = dict["id"] as? Int32 {
            self.id = id
        }
        self.name = dict["name"] as? String
        self.token = dict["token"] as? String
        self.photoUrl = dict["photo"] as? String
    }
    
    private static func deleteOldUser() {
        
        do {
            let entities = ["User"]
            
            for entity in entities {
                
                let fetchReques = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
                let objects = try AppDelegate.currentContext.fetch(fetchReques) as? [NSManagedObject]
                for object in objects! {
                    AppDelegate.currentContext.delete(object)
                }
            }
            try AppDelegate.currentContext.save()
            
        } catch let error {
            print(error)
        }
    }
}
