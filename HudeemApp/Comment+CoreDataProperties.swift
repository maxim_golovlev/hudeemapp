//
//  Comment+CoreDataProperties.swift
//  HudeemApp
//
//  Created by Admin on 21.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData


extension Comment {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Comment> {
        return NSFetchRequest<Comment>(entityName: "Comment")
    }

    @NSManaged public var status: String?
    @NSManaged public var planId: Int32
    @NSManaged public var comment: String?

}
