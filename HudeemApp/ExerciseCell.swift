//
//  ExerciseCell.swift
//  HudeemApp
//
//  Created by Admin on 25.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import TableKit

class ExerciseCell : BaseTableCell, ConfigurableCell {
    
    lazy var exerciseImageView: CustomImageView = {
        let imageView = CustomImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showTecnic)))
        return imageView
    }()
    
    lazy var exerciseTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.isUserInteractionEnabled = true
        label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showTecnic)))
        return label
    }()
    
    lazy var showTecnicButton: UIButton = {
        let button = UIButton(type: .system)
        let yourAttributes : [String: Any] =
            [NSFontAttributeName : DishCellSizes.recieptTitleFont,
             NSForegroundColorAttributeName : purpleColor,
             NSUnderlineStyleAttributeName : NSUnderlineStyle.styleSingle.rawValue]
        let attributeString = NSMutableAttributedString(string: "Техника выполнения", attributes: yourAttributes)
        button.setAttributedTitle(attributeString, for: .normal)
        button.addTarget(self, action: #selector(showTecnic), for: .touchUpInside)
        return button
    }()
    
    static var estimatedHeight: CGFloat? {
        return 20
    }
    
    static var defaultHeight: CGFloat? {
        return UITableViewAutomaticDimension
    }
    
    var exerciseId: Int32?
    
    
    func configure(with exerciseId: Int32?) {
        
        self.exerciseId = exerciseId
        
        guard let id = exerciseId else { return }
        
        let exercise: Exercise? = Exercise.attemptToFetch(withId: Int(id), context: AppDelegate.currentContext)
        
        
        exerciseImageView.loadImageUsingUrlString(exercise?.photoUrl)
        exerciseTitleLabel.text = exercise?.title
        
    }
    
    override func setupViews() {
        
        addSubview(exerciseImageView)
        addSubview(exerciseTitleLabel)
        addSubview(showTecnicButton)

        addConstraintsWithFormat("H:|[v0]|", views: exerciseImageView)
        addConstraintsWithFormat("H:|-21-[v0]-21-|", views: exerciseTitleLabel)
        addConstraintsWithFormat("H:|-21-[v0]", views: showTecnicButton)
        
        addConstraintsWithFormat("V:|-20-[v0(250)]-20-[v1]-10-[v2]|", views: exerciseImageView, exerciseTitleLabel, showTecnicButton)
        
    }
    
    func showTecnic() {
        TableCellAction(key: TrainCellActions.openTecnicClicked, sender: self, userInfo: ["id": self.exerciseId ?? 0]).invoke()
    }
    
}
