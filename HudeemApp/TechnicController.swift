//
//  TechnicController.swift
//  HudeemApp
//
//  Created by Admin on 29.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import TableKit
import CoreData
import AVKit
import AVFoundation

class TechnicController: UIViewController, Alertable, Loadable {

    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .white
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.allowsSelection = false
        self.tableDirector = TableDirector(tableView: tableView)
        return tableView
    }()
    
    var tableDirector: TableDirector?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        
        view.addSubview(tableView)
        tableView.fillSuperview()
        
        updateTableView()
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_arrow"), style: .plain, target: self, action: #selector(popupHandle))
        
    }
    
    func popupHandle() {
        
        navigationController?.popViewController(animated: true)
    }
    
    private func playVideo() {
        guard let path = Bundle.main.path(forResource: "sample", ofType:"mp4") else {
            debugPrint("sample.mp4 not found")
            return
        }
        
        let player = AVPlayer(url: URL(fileURLWithPath: path))
        let playerController = AVPlayerViewController()
        playerController.player = player
        present(playerController, animated: true) {
            player.play()
        }
     
     }
    
    
    var exerciseId: Int32? {
        didSet {
            
        }
    }
    
    
    func updateTableView() {
        
        guard let exerciseId = exerciseId else { return }
        
        let exercise: Exercise? = Exercise.attemptToFetch(withId: Int(exerciseId), context: AppDelegate.currentContext)
        navigationItem.title = exercise?.title
        
        tableDirector?.clear()
        
        let section = TableSection()
        
        
        let buttonPressed = TableRowAction<TechnicCell>(.custom(TrainCellActions.openVideoClicked)) { (cellOption) -> Void in
            self.playVideo()
        }
        
        let imageRow = TableRow<ImageCell>(item: (imageUrl: exercise?.photoUrl, image: nil, withSeparator: true, height: 225))
        
        let headerRow = TableRow<SelfSizedCell>(item: (title: exercise?.title, font: AppFont.medium(size: 22), topInset: 25, botInset: 15, withSeparator: false))
        
        let breakRow = TableRow<BreakTimeCell>(item: (min: Int(exercise?.pauseTime ?? 0), sec: Int(exercise?.pauseTimeSec ?? 0)))
        
        let headerImageRow = TableRow<ImageCell>(item: (imageUrl: nil, image: #imageLiteral(resourceName: "muscule_pic"), withSeparator: true, height: 400))
        
        let technicItemRow1 = TableRow<TechnicCell>(item: (title: "Исходное положение", subtitle: "ноги на ширине таза параллельны друг другу, руки на поясе (при выполнении с гантелями руки свободно опущены вниз)", image: #imageLiteral(resourceName: "video_back_dummy"), withSeparator: true), actions: [buttonPressed])
        
        let technicItemRow2 = TableRow<TechnicCell>(item: (title: "Выполнение", subtitle: "1. Удерживая корпус в вертикальном положенделаем широкий скользящий шаг вперед (придвижении стопы остаются параллельны другдругу). Корпус должен оставаться в вертикаль-ном положении, угол сгиба в колене обеих ног90 градусов. Колено задней ноги достаточноблизко к полу, но не касается его.\n\n2. Оттолкнувшись передней ногой, ернитесь висходное положение.\n\n3. Затем проделайте тоже самое другой ногой.", image: nil, withSeparator: true))
        
        let technicItemRow3 = TableRow<TechnicCell>(item: (title: "Особенности Выпадов:", subtitle: "Для того, чтобы научиться выполнять выпадыправильно, потребуется определеннаясноровка, но уже через 2-3 занятия, выбудете делать их вполне уверенно. Главноеизначально прочувствовать и понять техникуи не делать типичных ошибок.", image: nil, withSeparator: true))
        
        let technicItemRow4 = TableRow<TechnicCell>(item: (title: "Часто встречающиеся ошибки привыполнения упражнения:", subtitle: "1. Потеря равновесия. Это происходит, когда в процессе движения стопы ставятся на одну линию, а не параллельно друг другу. Нужно внимательно за этим следить и отработать технику.\n\n2. Отрыв пятки передней ноги от пола.  Это происходит из-за наклона корпуса вперед в процессе движения и при этом угол сгиба в колене передней ноги меньше 90 градусов. Спина должна оставаться прямой и не наклоняться ни вперед, ни назад.", image: nil, withSeparator: false))
        
        let footerRow = TableRow<TechnicFooterCell>.init(item: (title: "Понравилось упражнение?", subtitle: "Поделитесь с друзьями упражнением"))
        
        section.append(rows: [imageRow, headerRow, breakRow, headerImageRow, technicItemRow1, technicItemRow2, technicItemRow3, technicItemRow4, footerRow])
        
        tableDirector?.append(section: section)
        
        tableDirector?.reload()
    }

}
