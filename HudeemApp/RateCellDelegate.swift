//
//  RateCellDelegate.swift
//  HudeemApp
//
//  Created by Admin on 19.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import CoreData

protocol RateCellDelegate: class {
    
    typealias StatusCompletion = (Bool) -> ()
    func eatingStatusChanged(status: CompleteStatus, planId: Int32?, dishId: Int32?, completion: @escaping StatusCompletion)
    func showRecieptDidTap(dishId: Int32?, planId: Int32?)
}

extension RateCellDelegate where Self: UIViewController {
    
    func updateDish(planId: Int32?, status: CompleteStatus) {
        
        guard let planId = planId else { return }
        
        let dish: Dish? = Dish.attemptToFetch(withId: Int(planId), context: AppDelegate.currentContext)
        dish?.isEaten = status.rawValue
        NSManagedObject.attemptToSave()
    }
}

extension RateCellDelegate where Self: RecieptController {
    
    
    func eatingStatusChanged(status: CompleteStatus, planId: Int32?, dishId: Int32?, completion: @escaping StatusCompletion) {
        
        if status == .yes {
            
            let alertView = CustomAlertView()
            alertView.showRateView()
            alertView.sendRatePressed = { comment in
                
                completion(true)
                
                self.updateDish(planId: planId, status: status)
                
                ApiService.sharedService.postSendStatus(status: status, planId: planId, comment: comment) { error in
                    
                    if let error = error {
                        CommentsManager.sharedManager.queueComment(status: status, planId: planId, comment: comment)
                        print(error)
                    }
                    
                    DispatchQueue.main.async {
                        alertView.hideRateView()
                    }
                }
            }
            
        } else if status == .no {
            
            let alertView = CustomAlertView()
            alertView.showAnswerView()
            alertView.sendRatePressed = { comment in
                
                completion(false)
                
                self.updateDish(planId: planId, status: status)
                
                ApiService.sharedService.postSendStatus(status: status, planId: planId, comment: comment) { error in
                    
                    if let error = error {
                        CommentsManager.sharedManager.queueComment(status: status, planId: planId, comment: comment)
                        print(error)
                    }
                    
                    DispatchQueue.main.async {
                        alertView.hideAnswerView()
                    }
                }
            }
        }
    }
    
    func showRecieptDidTap(dishId: Int32?, planId: Int32?) {
        
    }
}

extension RateCellDelegate where Self: DietController {
    
        
}

