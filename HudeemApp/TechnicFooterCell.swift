//
//  TechnicFooterCell.swift
//  HudeemApp
//
//  Created by Admin on 30.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import TableKit

class TechnicFooterCell: BaseTableCell, ConfigurableCell {
    
    let heartImageView: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "heart_icon"))
        return imageView
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = AppFont.bold(size: 15)
        label.textColor = purpleColor
        return label
    }()
    
    let subtitleLabel: UILabel = {
        let label = UILabel()
        label.font = AppFont.roman(size: 15)
        label.numberOfLines = 0
        return label
    }()
    
    lazy var fbButton: UIButton = {
        let button = UIButton(type: .system)
        button.setBackgroundImage(#imageLiteral(resourceName: "Fb_icon"), for: .normal)
        button.tag = 0
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return button
    }()
    
    lazy var vkButton: UIButton = {
        let button = UIButton(type: .system)
        button.setBackgroundImage(#imageLiteral(resourceName: "Vk_icon"), for: .normal)
        button.tag = 1
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return button
    }()
    
    lazy var instButton: UIButton = {
        let button = UIButton(type: .system)
        button.setBackgroundImage(#imageLiteral(resourceName: "Instagram_icon"), for: .normal)
        button.tag = 2
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return button
    }()
    
    lazy var odnButton: UIButton = {
        let button = UIButton(type: .system)
        button.setBackgroundImage(#imageLiteral(resourceName: "odnoklassniki_icon"), for: .normal)
        button.tag = 3
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return button
    }()
    
    lazy var whatsButton: UIButton = {
        let button = UIButton(type: .system)
        button.setBackgroundImage(#imageLiteral(resourceName: "WhatsApp_icon"), for: .normal)
        button.tag = 4
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return button
    }()
    
    lazy var twitButton: UIButton = {
        let button = UIButton(type: .system)
        button.setBackgroundImage(#imageLiteral(resourceName: "twitter_icon"), for: .normal)
        button.tag = 5
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return button
    }()
    
    let scrollView: UIScrollView = {
        let sv = UIScrollView()
        sv.showsHorizontalScrollIndicator = false
        return sv
    }()
    
    let horizontalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.spacing = 15
        return stackView
    }()
    
    static var estimatedHeight: CGFloat? {
        return 60
    }
    
    static var defaultHeight: CGFloat? {
        return UITableViewAutomaticDimension
    }
    
    func configure(with data: (title: String?, subtitle: String?)) {
        titleLabel.text = data.title
        subtitleLabel.text = data.subtitle
    }
    
    
    override func setupViews() {
        
        addSubview(heartImageView)
        heartImageView.anchor(topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 21, bottomConstant: 0, rightConstant: 0, widthConstant: 35, heightConstant: 35)
        
        addSubview(titleLabel)
        titleLabel.anchor(nil, left: heartImageView.rightAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 21, bottomConstant: 0, rightConstant: 21, widthConstant: 0, heightConstant: 0)
        titleLabel.centerYAnchor.constraint(equalTo: heartImageView.centerYAnchor).isActive = true
        
        addSubview(subtitleLabel)
        subtitleLabel.anchor(titleLabel.bottomAnchor, left: titleLabel.leftAnchor, bottom: nil, right: titleLabel.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        addSubview(scrollView)
        scrollView.anchor(subtitleLabel.bottomAnchor, left: subtitleLabel.leftAnchor, bottom: bottomAnchor, right: subtitleLabel.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 30, rightConstant: 10, widthConstant: 0, heightConstant: 42)
        
        scrollView.addSubview(horizontalStackView)
        horizontalStackView.fillSuperview()
        horizontalStackView.heightAnchor.constraint(equalToConstant: 42).isActive = true
        
        horizontalStackView.addArrangedSubview(fbButton)
        horizontalStackView.addArrangedSubview(vkButton)
        horizontalStackView.addArrangedSubview(instButton)
        horizontalStackView.addArrangedSubview(odnButton)
        horizontalStackView.addArrangedSubview(whatsButton)
        horizontalStackView.addArrangedSubview(twitButton)
        
        _ = horizontalStackView.arrangedSubviews.map { $0.translatesAutoresizingMaskIntoConstraints = false; $0.widthAnchor.constraint(equalToConstant: 42).isActive = true }
        
    }
    
    func buttonTapped() {
        
        
    }
    
}
