//
//  Alertable.swift
//  HudeemApp
//
//  Created by Admin on 21.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import UIKit

protocol Alertable {
    func showAlert(title: String?, message: String?)
    func showAlert(title: String?, message: String?, handler: @escaping ((UIAlertAction) -> Void))
}

extension Alertable where Self: UIViewController {
    func showAlert(title: String?, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let close = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(close)
        
        present(alert, animated: true, completion: nil)
    }
    
    func showAlert(title: String?, message: String?, handler: @escaping ((UIAlertAction) -> Void)) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let close = UIAlertAction(title: "OK", style: .cancel, handler: handler)
        alert.addAction(close)
        
        present(alert, animated: true, completion: nil)
    }
    
    func showErrorAlert(message: String?) {
        
        DispatchQueue.main.async {
            self.showAlert(title: "Error", message: message)
        }
    }
}
