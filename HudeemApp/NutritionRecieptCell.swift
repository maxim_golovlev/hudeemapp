//
//  NutritionRecieptCell.swift
//  HudeemApp
//
//  Created by Admin on 05.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import CoreData
import TableKit

struct NutritionRecieptCellSizes {
    
    static let dishTitleFont = AppFont.medium(size: 22)
    static let nutritionsTitleFont = AppFont.medium(size: 13)
    static let nutritionsValuesFont = AppFont.bold(size: 13)
    static let cookingTimeFont = AppFont.medium(size: 15)
    static let difficultFont = AppFont.medium(size: 15)
    static let greenLabelFont = AppFont.medium(size: 11)
}

class NutritionRecieptCell: BaseTableCell, ConfigurableCell {
    
    static var estimatedHeight: CGFloat? {
        return 60
    }
    
    static var defaultHeight: CGFloat? {
        return UITableViewAutomaticDimension
    }
    
    weak var delegate: RateCellDelegate?
    
    let dishTitleLabel: UILabel = {
        let label = UILabel()
        label.font = NutritionRecieptCellSizes.dishTitleFont
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 2
        return label
    }()
    
    let attentionView: AttentionPlate = {
        let plate = AttentionPlate()
        return plate
    }()
    
    let nutritionLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    let cookingTimeLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    let overalCookingTimeLabel: UILabel = {
        let label = UILabel()
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 2
        return label
    }()
    
    let activeCookingTimeLabel: UILabel = {
        let label = UILabel()
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 2
        return label
    }()
    
    let difficultyLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    lazy var eatenButton: RoundedButton = {
        let button = RoundedButton(type: .system)
        button.backgroundColor = purpleColor
        button.setTitle("Я это уже съела", for: .normal)
        button.titleLabel?.font = DishCellSizes.buttonTitleFont
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(eatenHandler), for: .touchUpInside)
        return button
    }()
    
    lazy var notEatenButton: RoundedButton = {
        let button = RoundedButton(type: .system)
        button.backgroundColor = turquoise
        button.setTitle("Я это не съела", for: .normal)
        button.titleLabel?.font = DishCellSizes.buttonTitleFont
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(notEatenHandler), for: .touchUpInside)
        return button
    }()
    
    lazy var doneEatenButton: RoundedButton = {
        let button = RoundedButton(type: .system)
        button.backgroundColor = grayColor
        button.titleLabel?.font = DishCellSizes.buttonTitleFont
        button.setTitleColor(.white, for: .normal)
        return button
    }()
    
    lazy var doneClearButton: RoundedButton = {
        let button = RoundedButton(type: .system)
        button.backgroundColor = .clear
        button.setTitle("Я это не съела", for: .normal)
        button.titleLabel?.font = DishCellSizes.buttonTitleFont
        button.setTitleColor(.clear, for: .normal)
        return button
    }()
    
    let horizontalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fillProportionally
        stackView.spacing = 8
        return stackView
    }()
    
    let separator: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.8745098039, green: 0.8745098039, blue: 0.8745098039, alpha: 1)
        return view
    }()
    
    var planId: Int32? {
        didSet {
            
            guard let planId = planId else { return }
            
            let dish: Dish? = Dish.attemptToFetch(withId: Int(planId), context: AppDelegate.currentContext)
            
            dishTitleLabel.text = dish?.title
            
            let headerAttr = [NSFontAttributeName : NutritionRecieptCellSizes.nutritionsTitleFont]
            let greenAttr = [NSForegroundColorAttributeName : lightGreen,
                             NSFontAttributeName : NutritionRecieptCellSizes.greenLabelFont]
            let valuesAttr = [NSForegroundColorAttributeName : purpleColor,
                              NSFontAttributeName : NutritionRecieptCellSizes.nutritionsValuesFont]
            var cookingAttr:[String: Any] = [NSFontAttributeName : NutritionRecieptCellSizes.cookingTimeFont]
            
            let combine1 = NSMutableAttributedString()
            combine1.append(NSAttributedString(string: "Ккал: ", attributes: headerAttr))
            combine1.append(NSAttributedString(string: "\(dish?.calories ?? 0)", attributes: valuesAttr))
            combine1.append(NSAttributedString(string: "   Б/Ж/У: ", attributes: headerAttr))
            combine1.append(NSAttributedString(string: "\(dish?.proteins ?? 0)/\(dish?.fats ?? 0)/\(dish?.carbohydrates ?? 0) г.", attributes: valuesAttr))
            combine1.append(NSAttributedString(string: "   Порция: ", attributes: headerAttr))
            let sizeString = dish?.size == 0 ? "По вкусу" : "\(dish?.size ?? 0)"
            combine1.append(NSAttributedString(string: "\(sizeString) г.", attributes: valuesAttr))
            nutritionLabel.attributedText = combine1
            
            let clockIcon = NSTextAttachment()
            clockIcon.image = #imageLiteral(resourceName: "clock_icon")
            let chiefHatIcon = NSTextAttachment()
            chiefHatIcon.image = #imageLiteral(resourceName: "chief_hat")
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.paragraphSpacing = 0.25 * DishCellSizes.nutritionLableTitleFont.lineHeight
            cookingAttr[NSParagraphStyleAttributeName] = paragraphStyle
            let combine2 = NSMutableAttributedString()
            combine2.append(NSAttributedString(attachment: clockIcon))
            combine2.append(NSAttributedString(string: " Время готовки:", attributes: cookingAttr))
            cookingTimeLabel.attributedText = combine2
            let combine3 = NSMutableAttributedString()
            combine3.append(NSAttributedString(string: "\(dish?.cookTime ?? 0) мин.\n", attributes: cookingAttr))
            combine3.append(NSAttributedString(string: "общее", attributes: greenAttr))
            overalCookingTimeLabel.attributedText = combine3
            let combine4 = NSMutableAttributedString()
            combine4.append(NSAttributedString(string: "\(dish?.activeCookTime ?? 0) мин.\n", attributes: cookingAttr))
            combine4.append(NSAttributedString(string: "активное", attributes: greenAttr))
            activeCookingTimeLabel.attributedText = combine4
            let combine5 = NSMutableAttributedString()
            combine5.append(NSAttributedString(attachment: chiefHatIcon))
            combine5.append(NSAttributedString(string: " Сложность: \(dish?.level ?? "")", attributes: cookingAttr))
            difficultyLabel.attributedText = combine5
            
            cookingTimeHeightConstraint?.isActive = Int(dish?.cookTime ?? 0) > 0 ? false : true
            cookingTimeTopConstraint?.constant = Int(dish?.cookTime ?? 0) > 0 ? 25 : 0
            
            attentionView.days = Int(dish?.cookOnDays ?? 0)
            attentionViewHeightConstraint?.constant = dish?.cookOnDays ?? 0 > 0 ? 38 : 0
            attentionViewTopConstraint?.constant = dish?.cookOnDays ?? 0 > 0 ? 15 : 0
            
            toggleEatButtons(status: dish?.isEaten)
        }
    }
    
    var dishId: Int32?
    
    func toggleEatButtons(status: CompleteStatus.RawValue?) {
        
        doneEatenButton.removeFromSuperview()
        doneClearButton.removeFromSuperview()
        eatenButton.removeFromSuperview()
        notEatenButton.removeFromSuperview()
        
        switch status {
        case .some(CompleteStatus.yes.rawValue):
            doneEatenButton.setTitle("Вы уже это съели", for: .normal)
            horizontalStackView.addArrangedSubview(doneEatenButton)
            horizontalStackView.addArrangedSubview(doneClearButton)
        case .some(CompleteStatus.no.rawValue):
            doneEatenButton.setTitle("Вы это не съели", for: .normal)
            horizontalStackView.addArrangedSubview(doneEatenButton)
            horizontalStackView.addArrangedSubview(doneClearButton)
        default:
            horizontalStackView.addArrangedSubview(eatenButton)
            horizontalStackView.addArrangedSubview(notEatenButton)
        }
    }

    var attentionViewHeightConstraint: NSLayoutConstraint?
    var attentionViewTopConstraint: NSLayoutConstraint?
    
    var activeCookingTimeHeigthConstraint: NSLayoutConstraint?
    var overallCookingTimeHeightConstraint: NSLayoutConstraint?
    var cookingTimeHeightConstraint: NSLayoutConstraint?
    var cookingTimeTopConstraint: NSLayoutConstraint?
    
    func configure(with data:(planId: Int32?, dishId: Int32?)) {
        
        self.planId = data.planId
        self.dishId = data.dishId
    }
    
    override func setupViews() {
        
        addSubview(dishTitleLabel)
        dishTitleLabel.anchor(topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 20, leftConstant: 16, bottomConstant: 0, rightConstant: 16, widthConstant: 0, heightConstant: 0)
        
        addSubview(attentionView)
        attentionView.anchor(nil, left: dishTitleLabel.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 250, heightConstant: 0)
        
        attentionViewHeightConstraint = attentionView.heightAnchor.constraint(equalToConstant: 38)
        attentionViewHeightConstraint?.isActive = true
        
        attentionViewTopConstraint = attentionView.topAnchor.constraint(equalTo: dishTitleLabel.bottomAnchor, constant: 8)
        attentionViewTopConstraint?.isActive = true
        
        addSubview(nutritionLabel)
        nutritionLabel.anchor(attentionView.bottomAnchor, left: dishTitleLabel.leftAnchor, bottom: nil, right: rightAnchor, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 0)
        
        addSubview(cookingTimeLabel)
        cookingTimeLabel.anchor(nil , left: dishTitleLabel.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        cookingTimeHeightConstraint = cookingTimeLabel.heightAnchor.constraint(equalToConstant: 0)
        cookingTimeHeightConstraint?.isActive = false
        
        cookingTimeTopConstraint = cookingTimeLabel.topAnchor.constraint(equalTo: nutritionLabel.bottomAnchor, constant: 15)
        cookingTimeTopConstraint?.isActive = true
        
        addSubview(overalCookingTimeLabel)
        overalCookingTimeLabel.anchor(cookingTimeLabel.topAnchor, left: cookingTimeLabel.rightAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 3, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        overallCookingTimeHeightConstraint = overalCookingTimeLabel.heightAnchor.constraint(equalTo: cookingTimeLabel.heightAnchor)
        overallCookingTimeHeightConstraint?.isActive = true
        
        addSubview(activeCookingTimeLabel)
        activeCookingTimeLabel.anchor(cookingTimeLabel.topAnchor, left: overalCookingTimeLabel.rightAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        activeCookingTimeHeigthConstraint = activeCookingTimeLabel.heightAnchor.constraint(equalTo: cookingTimeLabel.heightAnchor)
        activeCookingTimeHeigthConstraint?.isActive = true
        
        addSubview(difficultyLabel)
        difficultyLabel.anchor(cookingTimeLabel.bottomAnchor, left: dishTitleLabel.leftAnchor, bottom: nil, right: rightAnchor, topConstant: 22, leftConstant: 0, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 0)
        
        addSubview(horizontalStackView)
        horizontalStackView.anchor(difficultyLabel.bottomAnchor, left: dishTitleLabel.leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 16, leftConstant: 0, bottomConstant: 28, rightConstant: 60, widthConstant: 0, heightConstant: 37)
        
        addSubview(separator)
        separator.anchor(nil, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: Screen.pixel)
    }
    
    func eatenHandler() {
        TableCellAction(key: RecieptCellActions.rateBtnClicked, sender: self, userInfo: ["status": CompleteStatus.yes]).invoke()
    }
    
    func notEatenHandler() {
        TableCellAction(key: RecieptCellActions.rateBtnClicked, sender: self, userInfo: ["status": CompleteStatus.no]).invoke()
    }
}
