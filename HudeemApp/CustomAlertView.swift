//
//  CustomAlertView.swift
//  HudeemApp
//
//  Created by Admin on 11.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import KMPlaceholderTextView

fileprivate enum AnswerState {
    case positive
    case negative
}


class RateAlertView : AlertView {
    
    let cellId = "cellId"
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(AlertCell.self, forCellReuseIdentifier: self.cellId)
        
        tableView.backgroundColor = .white
        tableView.isScrollEnabled = false
        return tableView
    }()


    
}

class AlertView: CustomView {
    var placeholderTextView: KMPlaceholderTextView?
    
    override func setupViews() {
        backgroundColor = .white
        translatesAutoresizingMaskIntoConstraints = false
        layer.cornerRadius = 5
        layer.masksToBounds = true
        dropShadow()
    }
}

class CustomAlertView: NSObject {
    
   // static let sharedInstance = CustomAlertView()
    
    
    
    let lightEffect = UIVibrancyEffect(blurEffect: UIBlurEffect(style: .light))
    let darkEffect = UIVibrancyEffect(blurEffect: UIBlurEffect(style: .dark))
    
    fileprivate let answers: [AnswerState] = [.positive, .negative]
    
    var rootView: UIView? {
        
        var rootVC = UIApplication.shared.keyWindow?.rootViewController
        if let navigationController = rootVC as? UINavigationController {
            rootVC = navigationController.viewControllers.first
        }
        
        let rootView = rootVC?.view
        
        return rootView
    }
    
    var sendRatePressed: ( (String?) -> () )?
    var cancelPressed: (() -> ())?
    
    
    
    lazy var rateAlertView: RateAlertView = {
        
        let view = RateAlertView()
        let titleLabel = UILabel()
        titleLabel.text = "Оцените это блюдо:"
        titleLabel.font = .systemFont(ofSize: 17)
        
        view.tableView.dataSource = self
        view.tableView.delegate = self
        
        let separator = UIView()
        separator.backgroundColor = view.tableView.separatorColor
        
        var sendButton: RoundedButton = {
            let button = RoundedButton(type: .system)
            button.backgroundColor = purpleColor
            button.setTitle("Отправить оценку", for: .normal)
            button.titleLabel?.font = .boldSystemFont(ofSize: 12)
            button.setTitleColor(.white, for: .normal)
            button.addTarget(self, action: #selector(rateAlertViewRateHandler), for: .touchUpInside)
            return button
        }()
        
        var cancelButton: RoundedButton = {
            let button = RoundedButton(type: .system)
            button.backgroundColor = turquoise
            button.setTitle("Отмена", for: .normal)
            button.titleLabel?.font = .boldSystemFont(ofSize: 12)
            button.setTitleColor(.white, for: .normal)
            button.addTarget(self, action: #selector(cancelHandler), for: .touchUpInside)
            return button
        }()
        
        let horizontalStackView: UIStackView = {
            let stackView = UIStackView()
            stackView.translatesAutoresizingMaskIntoConstraints = false
            stackView.axis = .horizontal
            stackView.distribution = .fillProportionally
            stackView.spacing = 8
            return stackView
        }()
        
        view.addSubview(titleLabel)
        titleLabel.anchor(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 16, widthConstant: 0, heightConstant: 0)
        
        view.addSubview(view.tableView)
        view.tableView.anchor(titleLabel.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 16, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 100)
        
        view.addSubview(separator)
        separator.anchor(nil, left: view.tableView.leftAnchor, bottom: view.tableView.topAnchor, right: view.tableView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.5)
        
        view.addSubview(horizontalStackView)
        horizontalStackView.anchor(view.tableView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 25, widthConstant: 0, heightConstant: 37)
        
        horizontalStackView.addArrangedSubview(sendButton)
        horizontalStackView.addArrangedSubview(cancelButton)
        horizontalStackView.bottomAnchor.constraint(lessThanOrEqualTo: view.bottomAnchor, constant: 16).isActive = true
        
        return view
    }()
    
    lazy var answerAlertView: AlertView = {
        
        let view = AlertView()
        
        let titleLabel = UILabel()
        titleLabel.font = .systemFont(ofSize: 17)
        titleLabel.text = "Почему вы не съели это блюдо?"
        titleLabel.numberOfLines = 0
        titleLabel.lineBreakMode = .byWordWrapping
        
        let subTitleLabel = UILabel()
        subTitleLabel.font = .systemFont(ofSize: 11)
        subTitleLabel.text = "Ваше сообщение увидит ваш эксперт по питанию. Если вы съели что-то другое - укажите, что именно и сколько."
        subTitleLabel.numberOfLines = 0
        subTitleLabel.lineBreakMode = .byWordWrapping
        
        let placeholderTextView = KMPlaceholderTextView()
        placeholderTextView.font = .systemFont(ofSize: 11)
        placeholderTextView.placeholder = "Введите ваше сообщение"
        view.placeholderTextView = placeholderTextView
        placeholderTextView.backgroundColor = .clear

        let grayLayerView = UIView()
        grayLayerView.backgroundColor = UIColor.rgb(243, green: 243, blue: 243)
        
        let horizontalStackView: UIStackView = {
            let stackView = UIStackView()
            stackView.translatesAutoresizingMaskIntoConstraints = false
            stackView.axis = .horizontal
            stackView.distribution = .fillProportionally
            stackView.spacing = 8
            return stackView
        }()
        
        var sendButton: RoundedButton = {
            let button = RoundedButton(type: .system)
            button.backgroundColor = purpleColor
            button.setTitle("Отправить оценку", for: .normal)
            button.titleLabel?.font = .boldSystemFont(ofSize: 12)
            button.setTitleColor(.white, for: .normal)
            button.addTarget(self, action: #selector(answerAlertViewRateHandler), for: .touchUpInside)
            return button
        }()
        
        var hiddenButton: RoundedButton = {
            let button = RoundedButton(type: .system)
            button.backgroundColor = turquoise
            button.setTitle("Отмена", for: .normal)
            button.titleLabel?.font = .boldSystemFont(ofSize: 12)
            button.setTitleColor(.white, for: .normal)
            button.addTarget(self, action: #selector(cancelHandler), for: .touchUpInside)
            return button
        }()
        
        view.addSubview(titleLabel)
        view.addConstraintsWithFormat("H:|-16-[v0]-16-|", views: titleLabel)
        
        view.addSubview(subTitleLabel)
        view.addConstraintsWithFormat("H:|-16-[v0]-16-|", views: subTitleLabel)
        
        view.addSubview(grayLayerView)
        view.addConstraintsWithFormat("H:|[v0]|", views: grayLayerView)
        
        grayLayerView.addSubview(placeholderTextView)
        grayLayerView.addConstraintsWithFormat("H:|-16-[v0]-16-|", views: placeholderTextView)
        grayLayerView.addConstraintsWithFormat("V:|[v0]|", views: placeholderTextView)
        
        view.addSubview(horizontalStackView)
        view.addConstraintsWithFormat("H:|-16-[v0]-16-|", views: horizontalStackView)
        horizontalStackView.addArrangedSubview(sendButton)
        horizontalStackView.addArrangedSubview(hiddenButton)
        
        view.addConstraintsWithFormat("V:|-10-[v0][v1]-16-[v2(100)]-16-[v3(37)]-<=16-|", views: titleLabel, subTitleLabel, grayLayerView, horizontalStackView)
        
        return view
    }()
    
    func createVisualEffectView() -> UIVisualEffectView {
        
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        let vibrancyEffect = UIVibrancyEffect(blurEffect: blurEffect)
        let vibrancyEffectView = UIVisualEffectView(effect: vibrancyEffect)
        blurEffectView.contentView.addSubview(vibrancyEffectView)
        vibrancyEffectView.fillSuperview()
        
        return blurEffectView
    }
    
    var visualEffectView: UIVisualEffectView!
    
    private func show(view: UIView) {
        
        visualEffectView = createVisualEffectView()

        rootView?.addSubview(visualEffectView)
        visualEffectView.fillSuperview()
        
        rootView?.addSubview(view)
        view.anchorCenterSuperview()
        view.heightAnchor.constraint(equalToConstant: view.isEqual(rateAlertView) ? 220 : 290).isActive = true
        view.widthAnchor.constraint(equalToConstant: 270).isActive = true
        
        view.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        view.alpha = 0
        
        UIView.animate(withDuration: 0.4) {
            self.visualEffectView.effect = self.lightEffect
            view.alpha = 1
            view.transform = CGAffineTransform.identity
        }
    }
    
    private func hide(view: UIView) {
        
        view.endEditing(true)
        
        UIView.animate(withDuration: 0.3, animations: {
            view.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            view.alpha = 0
            
            self.visualEffectView.effect = nil
            
        }) { (success:Bool) in
            view.removeFromSuperview()
            self.visualEffectView.removeFromSuperview()
        }
    }
    
    func showRateView() {
        show(view: rateAlertView)
        
    }
    
    func showAnswerView() {
        show(view: answerAlertView as UIView)
    }
    
    func hideRateView() {
        hide(view: rateAlertView)
    }
    
    func hideAnswerView() {
        hide(view: answerAlertView as UIView)
    }
    
    func rateAlertViewRateHandler() {
        
        let selectedCells = self.rateAlertView.tableView.visibleCells.filter { $0.isSelected }
        
        if selectedCells.count <= 0 { return }
        
        if let sendRatePressed = sendRatePressed {
            sendRatePressed(nil)
        }
    }
    
    func answerAlertViewRateHandler() {
        if let sendRatePressed = sendRatePressed {
            sendRatePressed(answerAlertView.placeholderTextView?.text)
        }
    }
    
    func cancelHandler(button: UIView) {
        
        guard let alertView = button.superview?.superview as? AlertView else { return }
        hide(view: alertView)
    }
}

extension CustomAlertView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: rateAlertView.cellId, for: indexPath) as! AlertCell
        cell.state = answers[indexPath.row]
        return cell
    }
}

extension CustomAlertView: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let color = (cell as? AlertCell)?.label.text == AlertCell.positiveText ? AlertCell.lightGreen : AlertCell.lightRed
        let selectionColor = UIView()
        selectionColor.backgroundColor = color
        cell.selectedBackgroundView = selectionColor
        
        cell.separatorInset = .zero
        cell.preservesSuperviewLayoutMargins = false
        cell.layoutMargins = .zero
    }
}

private class AlertCell: BaseTableCell {
    
    static let positiveText = "Мне понравилось, давайте и дальше будем включать его в мой план питания"
    static let negativeText = "Мне совсем не понравилось, больше не включайте его в мой план питания"
    
    static let lightGreen = UIColor.rgb(243, green: 252, blue: 230)
    static let lightRed = UIColor.rgb(255, green: 239, blue: 239)
    
    
    fileprivate var state: AnswerState? {
        didSet {
            label.text = state == .positive ? AlertCell.positiveText : AlertCell.negativeText
            smile.image = state == .positive ? #imageLiteral(resourceName: "green_smile") : #imageLiteral(resourceName: "red_smile")
        }
    }
    
    let smile: UIImageView = {
        let view = UIImageView()
        return view
    }()
    
    let label: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 11)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        isSelected = false
    }
    
    
    override func setupViews() {
        
        addSubview(smile)
        addSubview(label)
        addConstraintsWithFormat("H:|-16-[v0(30)]-8-[v1]-16-|", views: smile, label)
        smile.heightAnchor.constraint(equalToConstant: 30).isActive = true
        smile.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
}
