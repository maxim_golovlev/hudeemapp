//
//  ProceedBar.swift
//  HudeemApp
//
//  Created by Admin on 30.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

protocol ProceedBarDeleagte: class {
    
    func didSelectetItem(atIndex index: Int)
}

class ProceedBar: CustomView {
    
    class StepItem: CustomView {
        var label = UILabel()
        var bottomLine = UIView()
        var selector: Selector
        var target: AnyObject
        
        var isSelected = false {
            didSet {
                label.textColor = isSelected ? purpleColor : .black
                bottomLine.backgroundColor = isSelected ? purpleColor : .black
            }
        }
        
        init(selector: Selector, target: AnyObject) {
            
            self.selector = selector
            self.target = target
            
            super.init(frame: .zero)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override func setupViews() {
            
            isUserInteractionEnabled = true
            addGestureRecognizer(UITapGestureRecognizer(target: selector, action: selector))
            
            addSubview(label)
            label.anchorCenterSuperview()
            
            addSubview(bottomLine)
            bottomLine.anchor(nil, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 1)
        }
    }
    
    weak var delegate: ProceedBarDeleagte?
    
    lazy var stepOne: StepItem = {
        let view = StepItem(selector: #selector(handleOneTap), target: self)
        view.label.text = ""
        return view
    }()
    
    lazy var stepTwo: StepItem = {
        let view = StepItem(selector: #selector(handleTwoTap), target: self)
        view.label.text = ""
        return view
    }()
    
    lazy var stepThree: StepItem = {
        let view = StepItem(selector: #selector(handleThreeTap), target: self)
        view.label.text = ""
        return view
    }()
    
    let verticalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.spacing = 0
        return stackView
    }()
    
    override func setupViews() {
        
        addSubview(verticalStackView)
        verticalStackView.fillSuperview()
        
        verticalStackView.addArrangedSubview(stepOne)
        verticalStackView.addArrangedSubview(stepTwo)
        verticalStackView.addArrangedSubview(stepThree)
        
    }
    
    func handleOneTap() {
        stepOne.isSelected = !stepOne.isSelected
        delegate?.didSelectetItem(atIndex: 0)
    }
    
    func handleTwoTap() {
        stepTwo.isSelected = !stepTwo.isSelected
        stepOne.isSelected = true
        delegate?.didSelectetItem(atIndex: 1)
    }
    
    func handleThreeTap() {
        stepThree.isSelected = !stepThree.isSelected
        stepOne.isSelected = true
        stepTwo.isSelected = true
        delegate?.didSelectetItem(atIndex: 2)
    }
    
}
