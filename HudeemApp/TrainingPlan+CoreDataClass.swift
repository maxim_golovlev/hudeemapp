//
//  TrainingPlan+CoreDataClass.swift
//  HudeemApp
//
//  Created by Admin on 23.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData


public class TrainingPlan: NSManagedObject {

    static func create(withData data: Data?, context: NSManagedObjectContext) -> TrainingPlan?  {
        
        guard let data = data else { return nil }
        
        do {
            if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                
                if let error = json["error"] {
                    print(error)
                    return nil
                }
                var id: Int?
                if let idString = (json["total"] as AnyObject)["date"] as? NSString  {
                    id = Int(idString.replacingOccurrences(of: ".", with: ""))
                }
                
                let trainingPlan: TrainingPlan = createOrReturn(withId: id, context: context)
                
                if let lastUpdated = json["last_updated"] as? String, let lastUpdatedDate = lastUpdated.dateFromString() as NSDate? {
                    if trainingPlan.lastUpdated != lastUpdatedDate {
                        trainingPlan.initWithDict(dict: json, context: context)
                    }
                } else {
                    trainingPlan.initWithDict(dict: json, context: context)
                }
                
                trainingPlan.createAndAddRelationObjects(dict: json, context: context)
                return trainingPlan
                
            }
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        return nil
    }
    
    func createAndAddRelationObjects(dict: [String: Any], context: NSManagedObjectContext) {
        
        if let planDict = dict["plan"] as? [[String: AnyObject]] {
            
            for trainingDict in planDict {
                
                let training = Training.create(withDict: trainingDict, context: context)
                training.plan = self
                
                if let exersicesDict = trainingDict["trainings"] as? [[String: AnyObject]] {
                    
                    for exersiceDict in exersicesDict {
                        let exercise = Exercise.create(withDict: exersiceDict, context: context)
                        exercise.training = training
                    }
                }
            }
        }
        
        NSManagedObject.attemptToSave(context: context)
    }
    
    func initWithDict(dict: [String: Any], context: NSManagedObjectContext) {
        
        if let total = dict["total"] as? [String: AnyObject] {
            
            if let idString = total["date"] as? String, let id = idString.dateFromString() {
                self.date = id as NSDate?
            }
            if let idString = total["date"] as? NSString, let id = Int32(idString.replacingOccurrences(of: ".", with: "")) {
                self.id = id
            }
        }
        
        if let lastUpdated = dict["last_updated"] as? String {
            self.lastUpdated = lastUpdated.dateFromString() as NSDate?
        }
        
        NSManagedObject.attemptToSave(context: context)
    }

}
