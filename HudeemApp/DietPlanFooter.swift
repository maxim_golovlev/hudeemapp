//
//  DietPlanFooter.swift
//  HudeemApp
//
//  Created by Admin on 02.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import TableKit

class DietPlanFooter: BaseTableCell, ConfigurableCell {
    
    static var estimatedHeight: CGFloat? {
        return 20
    }
    
    static var defaultHeight: CGFloat? {
        return UITableViewAutomaticDimension
    }
    
    let chatView: UIView = {
        let view = UIView()
        view.backgroundColor = purpleColor
        view.layer.cornerRadius = 5
        view.layer.masksToBounds = true
        return view
    }()
    
    let messageView: UITextView = {
        let text = UITextView()
        text.textColor = .white
        text.backgroundColor = .clear
        text.text = "Воду, чай, кофе (всё без сахара!) можно пить с любыми приёмами пищи и в любое другое время. Кофе в сумме не более 3-х чашек в день."
        text.font = .systemFont(ofSize: 11)
        text.isEditable = false
        return text
    }()
    
    let totalLabel: UILabel = {
        let label = UILabel()
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 2
        return label
    }()
    
    var createModels: (([PieSliceModel]) -> ())?

    lazy var pieChart: PieChart = {
        let chart = PieChart(frame: CGRect(x: 0, y: 0, width: Screen.width, height: 120))
        chart.innerRadius = 20
        chart.outerRadius = 60
        chart.selectedOffset = 30
        chart.animDuration = 0.5
        chart.referenceAngle = 270
        chart.layers = [self.createTextWithLinesLayer()]
        chart.delegate = self
        self.createModels = { models in
            chart.models = models
        }
        chart.isUserInteractionEnabled = false
        return chart
    }()
    
    func configure(with mealPlanId: Int?) {        
        self.mealPlanId = mealPlanId
    }
    
    var mealPlanId: Int? {
        didSet {
            
            var mealPlan: MealPlan? = nil
            if let id = mealPlanId {
                mealPlan = MealPlan.attemptToFetch(withId: id, context: AppDelegate.currentContext)
            }

            let models = [
                PieSliceModel(value: Double(mealPlan?.percFats ?? 0), color: ocherColor),
                PieSliceModel(value: Double(mealPlan?.percCarbohydrates ?? 0), color: saladGreen),
                PieSliceModel(value: Double(mealPlan?.percProteins ?? 0), color: paleColor),
                ]
            
            if let createModels = createModels {
                createModels(models)
            }
            
            let combine = NSMutableAttributedString()
            
            let titleFont = AppFont.bold(size: 17)
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.paragraphSpacing = 0.8 * titleFont.lineHeight
            
            let titleAttr = [NSFontAttributeName : titleFont,
                             NSParagraphStyleAttributeName:paragraphStyle,
                             NSForegroundColorAttributeName : purpleColor]
            let headerAttr = [NSFontAttributeName : AppFont.medium(size: 16)]
            let valuesAttr = [NSForegroundColorAttributeName : purpleColor,
                              NSFontAttributeName : AppFont.bold(size: 16)]
            
            combine.append(NSAttributedString(string: "Итого:\n", attributes: titleAttr ))
            combine.append(NSAttributedString(string: "Ккал: ", attributes: headerAttr))
            combine.append(NSAttributedString(string: "\(mealPlan?.calories ?? 0)", attributes: valuesAttr))
            combine.append(NSAttributedString(string: "   Б/Ж/У: ", attributes:headerAttr))
            combine.append(NSAttributedString(string: "\(mealPlan?.proteins ?? 0)/\(mealPlan?.fats ?? 0)/\(mealPlan?.carbohydrates ?? 0) г.", attributes: valuesAttr))
            totalLabel.attributedText = combine
        }
    }
    
    override func setupViews() {
        super.setupViews()
        
        addSubview(chatView)
        let size = CGSize(width: Screen.width - 18 - 8 - 8 - 18, height: 1000)
        var height: CGFloat = 76
        if let estimatedHeight = String.estimageTextSize(text: messageView.text, size: size, font: messageView.font!)?.height {
            height = estimatedHeight + 30
        }
        chatView.anchor(topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 18, bottomConstant: 0, rightConstant: 18, widthConstant: 0, heightConstant: height)
        
        chatView.addSubview(messageView)
        messageView.anchor(chatView.topAnchor, left: chatView.leftAnchor, bottom: chatView.bottomAnchor, right: chatView.rightAnchor, topConstant: 4, leftConstant: 8, bottomConstant: 8, rightConstant: 8, widthConstant: 0, heightConstant: 0)
        
        addSubview(totalLabel)
        totalLabel.anchor(chatView.bottomAnchor, left: leftAnchor, bottom: nil, right: chatView.rightAnchor, topConstant: 25, leftConstant: 18, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        addSubview(pieChart)
        pieChart.anchor(totalLabel.bottomAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 35, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 170)
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()

        pieChart.removeFromSuperview()
        
        pieChart = PieChart(frame: CGRect(x: 0, y: 0, width: Screen.width, height: 120))
        pieChart.innerRadius = 20
        pieChart.outerRadius = 60
        pieChart.selectedOffset = 30
        pieChart.animDuration = 0.5
        pieChart.referenceAngle = 270
        pieChart.layers = [self.createTextWithLinesLayer()]
        pieChart.delegate = self
        self.createModels = { models in
            self.pieChart.models = models
        }
        
        pieChart.isUserInteractionEnabled = false
        
        addSubview(pieChart)
        pieChart.anchor(totalLabel.bottomAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 35, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 170)
    }
    
    fileprivate func createTextWithLinesLayer() -> PieLineTextLayer {
        let lineTextLayer = PieLineTextLayer()
        
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 1
        
        var lineTextLayerSettings = PieLineTextLayerSettings()
        lineTextLayerSettings.label.font = AppFont.medium(size: 16)
        lineTextLayerSettings.lineColor = { slice in
            
            let prefix: UIColor
            
            switch slice.hashValue {
            case 2:
                prefix = paleColor
            case 0:
                prefix = ocherColor
            case 1:
                prefix = saladGreen
            default:
                prefix = .black
            }
            return prefix
            
        }
        lineTextLayerSettings.label.textGenerator = {slice in
            
            let prefix: String
            
            switch slice.hashValue {
            case 2:
                prefix = "Б: "
            case 0:
                prefix = "Ж: "
            case 1:
                prefix = "У: "
            default:
                prefix = ""
            }
            return formatter.string(from: slice.data.percentage * 100 as NSNumber).map{"\(prefix) \($0)%"} ?? ""
        }
        
        lineTextLayer.settings = lineTextLayerSettings
        return lineTextLayer
    }

}

extension DietPlanFooter: PieChartDelegate {
    
    func onSelected(slice: PieSlice, selected: Bool) {
        print("Selected: \(selected), slice: \(slice)")
    }
}
