//
//  CookingStepCell.swift
//  HudeemApp
//
//  Created by Admin on 16.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class CookingStepCell: BaseCollectionCell {
    
    class CounterImage: UIImageView {
        var counter: Int? {
            didSet {
                guard let counter = counter else { return }
                label.text = "\(counter + 1)"
            }
        }
        
        let label: UILabel = {
            let label = UILabel()
            label.textColor = .white
            label.textAlignment = .center
            label.font = AppFont.medium(size: 15)
            return label
        }()
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            self.addSubview(label)
            label.fillSuperview()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    var iconView: CounterImage = {
        let imageView = CounterImage(frame: .zero)
        imageView.image = #imageLiteral(resourceName: "empty_circle")
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    let verticalLine: UIView = {
        let view = UIView()
        view.backgroundColor = purpleColor
        return view
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = AppFont.roman(size: 15)
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        return label
    }()
    
    lazy var imageView: CustomImageView = {
        let imageView = CustomImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    
    var step: (Step?, Int)? {
        didSet {
            iconView.counter = step?.1
            titleLabel.text = step?.0?.text
        }
    }
    
    static let circleHeight: CGFloat = 35
    static let circleLeft: CGFloat = 23
    
    static let titleTop = 35/2 - AppFont.roman(size: 15).lineHeight/2
    static let titleLeft: CGFloat = 16
    static let titleRight:CGFloat = 17
    static let titleBot:CGFloat = 18
    
    static let lineTop: CGFloat = 5
    static let lineWidth: CGFloat = 1
    
    override func setupViews() {
        
        addSubview(iconView)
        addSubview(verticalLine)
        addSubview(titleLabel)
        
        iconView.anchor(topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: CookingStepCell.circleLeft, bottomConstant: 0, rightConstant: 0, widthConstant: CookingStepCell.circleHeight, heightConstant: CookingStepCell.circleHeight)
        
        verticalLine.anchor(iconView.bottomAnchor, left: nil, bottom: bottomAnchor, right: nil, topConstant: CookingStepCell.lineTop, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: CookingStepCell.lineWidth, heightConstant: 0)
        
        verticalLine.centerXAnchor.constraint(equalTo: iconView.centerXAnchor).isActive = true
        
        titleLabel.anchor(topAnchor, left: iconView.rightAnchor, bottom: nil, right: rightAnchor, topConstant: CookingStepCell.titleTop, leftConstant: CookingStepCell.titleLeft, bottomConstant: 0, rightConstant: CookingStepCell.titleRight, widthConstant: 0, heightConstant: 0)
        
        titleLabel.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor, constant: CookingStepCell.titleBot).isActive = true
    }
    
    static func getHeight(fromStep step: Step?) -> CGFloat {
        
        let textWidth = UIScreen.main.bounds.width - CookingStepCell.circleLeft - CookingStepCell.circleHeight - CookingStepCell.titleLeft - CookingStepCell.titleRight
        let textSize = CGSize(width: textWidth, height: 1000)
        let font = AppFont.roman(size: 15)
        let textHeight = String.estimageTextSize(text: step?.text, size: textSize, font: font)?.height ?? 0
        let totalHeight = titleTop + textHeight + CookingStepCell.titleBot
        
        return totalHeight
    }
}
