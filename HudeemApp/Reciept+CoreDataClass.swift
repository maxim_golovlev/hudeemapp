//
//  Reciept+CoreDataClass.swift
//  HudeemApp
//
//  Created by Admin on 26.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData


public class Reciept: NSManagedObject {
    
    static func create(withData data: Data?, context: NSManagedObjectContext) -> Reciept? {
        
        guard let data = data else { return nil }
        
        do {
            if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject] {
                
                if let error = json["error"] {
                    print(error)
                    return nil
                }
                
                let reciept = Reciept.create(withDict: json, context: context)
                return reciept
            }
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        return nil
    }
    
    static func create(withDict dict: [String: AnyObject], context: NSManagedObjectContext) -> Reciept {
        
        let reciept: Reciept = createOrReturn(withId: dict["dish_id"] as? Int, context: context)
        reciept.initWithDict(dict: dict, context: context)
        reciept.createAndAddRelationObjects(dict: dict, context: context)

        return reciept
    }
    
    func createAndAddRelationObjects(dict: [String: AnyObject], context: NSManagedObjectContext) {
        
        if let stepsDict = dict["steps"] as? [[String: AnyObject]] {
            for stepDict in stepsDict {
                let step = Step.create(withDict: stepDict, context: context)
                step.receipt = self
            }
        }
        
        if let ingredientsDict = dict["ingredients"] as? [[String: AnyObject]] {
            for ingredientDict in ingredientsDict {
                let ingridient = Ingredient.create(withDict: ingredientDict, context: context)
                ingridient.receipt = self
            }
        }
        
        if let toolsDict = dict["tools"] as? [[String: AnyObject]] {
            for toolDict in toolsDict {
                let tool = Tool.create(withDict: toolDict, context: context)
                tool.reciept = self
            }
        }
        
        NSManagedObject.attemptToSave(context: context)
    }

    func initWithDict(dict: [String: AnyObject], context: NSManagedObjectContext) {
            
        if let dishId = dict["dish_id"] as? Int32 {
            self.dishId = dishId
        }
        
        self.title = dict["title"] as? String
        self.photoUrl = dict["photo"] as? String
        self.level = dict["level"] as? String
        
        if let cookTime = dict["cook_time"] as? Int32 {
            self.cookTime = cookTime
        }
        if let activeCookTime = dict["active_cook_time"] as? Int32 {
            self.activeCookTime = activeCookTime
        }
        
        if let cookOnDays = dict["cook_on_days"] as? Int32 {
            self.cookOnDays = cookOnDays
        }
       
        if let proteins = dict["proteins"] as? Int32 {
            self.proteins = proteins
        }
        
        if let fats = dict["fats"] as? Int32 {
            self.fats = fats
        }
        if let carbohydrates = dict["carbohydrates"] as? Int32 {
            self.carbohydrates = carbohydrates
        }
        if let calories = dict["calories"] as? Int32 {
            self.calories = calories
        }
        
        if let size = dict["size"] as? Int32 {
            self.size = size
        }
        
        if let id = dict["dish_id"] as? Int32 {
            self.id = id
        }
        
        NSManagedObject.attemptToSave(context: context)
    }
}
