//
//  DishCookingTimeCell.swift
//  HudeemApp
//
//  Created by Admin on 03.05.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import TableKit

class DishCookingTimeCell: BaseTableCell, ConfigurableCell {
    
    let cookingTimeLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 11)
        return label
    }()
    
    let overalCookingTimeLabel: UILabel = {
        let label = UILabel()
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 2
        return label
    }()
    
    let activeCookingTimeLabel: UILabel = {
        let label = UILabel()
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 2
        return label
    }()
    
    static var estimatedHeight: CGFloat? {
        return 20
    }
    
    static var defaultHeight: CGFloat? {
        return UITableViewAutomaticDimension
    }
    
    
    func configure(with data:(cookTime: Int32?, activeCookTime: Int32? )) {
        
        let paragraphStyle2 = NSMutableParagraphStyle()
        paragraphStyle2.paragraphSpacing = 0.25 * DishCellSizes.nutritionLableTitleFont.lineHeight
        let headerAttr = [NSFontAttributeName : DishCellSizes.cookingTimeLabelFont,
                      NSParagraphStyleAttributeName:paragraphStyle2]
        let greenAttr = [NSForegroundColorAttributeName : lightGreen,
                         NSFontAttributeName : DishCellSizes.greenLabelsFont,
                         NSParagraphStyleAttributeName:paragraphStyle2]
        let clockIcon = CentredAttachment()
        clockIcon.image = #imageLiteral(resourceName: "clock_icon")
        let chiefHatIcon = CentredAttachment()
        chiefHatIcon.image = #imageLiteral(resourceName: "chief_hat")
        
        let combine4 = NSMutableAttributedString()
        combine4.append(NSAttributedString(attachment: clockIcon))
        combine4.append(NSAttributedString(string: " Время готовки:", attributes: headerAttr))
        cookingTimeLabel.attributedText = combine4
        
        let combine5 = NSMutableAttributedString()
        combine5.append(NSAttributedString(string: "\(data.cookTime ?? 0) мин.\n", attributes: headerAttr))
        combine5.append(NSAttributedString(string: "общее", attributes: greenAttr))
        overalCookingTimeLabel.attributedText = combine5
        
        let combine6 = NSMutableAttributedString()
        combine6.append(NSAttributedString(string: " - \(data.activeCookTime ?? 0) мин.\n", attributes: headerAttr))
        combine6.append(NSAttributedString(string: "активное", attributes: greenAttr))
        activeCookingTimeLabel.attributedText = combine6

    }
    
    override func setupViews() {
        
        addSubview(cookingTimeLabel)
        cookingTimeLabel.anchor( topAnchor, left: leftAnchor, bottom: bottomAnchor, right: nil, topConstant: 10, leftConstant: 18, bottomConstant: 15, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        addSubview(overalCookingTimeLabel)
        overalCookingTimeLabel.anchor(cookingTimeLabel.topAnchor, left: cookingTimeLabel.rightAnchor, bottom: nil, right: nil, topConstant: 3, leftConstant: 3, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)

        
        addSubview(activeCookingTimeLabel)
        activeCookingTimeLabel.anchor(cookingTimeLabel.topAnchor, left: overalCookingTimeLabel.rightAnchor, bottom: nil, right: nil, topConstant: 3, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
}
