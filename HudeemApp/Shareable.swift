//
//  Shareable.swift
//  HudeemApp
//
//  Created by Admin on 21.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import UIKit

protocol Shareable : class {
    func share(textToShare: String?)
}

extension Shareable where Self: RecieptController {
    
    func share(textToShare: String?) {
        
        guard let text = currentReciept?.title , let imageUrl = currentReciept?.photoUrl else { return }
       
        let activityVC = UIActivityViewController(activityItems: [MyStringItemSource(textToShare: text), MyImageItemSource(imageUrl: imageUrl)], applicationActivities: nil )
        
        self.present(activityVC, animated: true, completion: nil)
    }
}

class MyStringItemSource: NSObject, UIActivityItemSource {
    
    var textToShare: String!
    
    init(textToShare: String) {
        
        self.textToShare = textToShare

    }
    
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return "" as AnyObject
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivityType) -> Any? {
        
        if activityType == UIActivityType.postToTwitter {
            return String() as AnyObject?
            
        } else if activityType.rawValue == "com.vk.vkclient.shareextension" {
            return textToShare as AnyObject?
            
        } else {
            return textToShare as AnyObject?
        }
    }
}

class MyImageItemSource: NSObject, UIActivityItemSource {
    
    var imageUrl: String?
    
    init(imageUrl: String?) {
        self.imageUrl = imageUrl
    }
    
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return "" as AnyObject
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivityType) -> Any? {
        
        return imageUrl
    }
}
