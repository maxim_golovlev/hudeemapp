//
//  MessageController.swift
//  HudeemApp
//
//  Created by Admin on 21.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import CoreData

class MessagesFeedController: CenterController {
    
    let cellId = "cellId"
    var blockOperations = [BlockOperation]()
    
    lazy var fetchViewController: NSFetchedResultsController<NSFetchRequestResult> = {
        let currentContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Friend")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "lastMessage.date", ascending: true)]
        let fetchController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: currentContext, sectionNameKeyPath: nil, cacheName: nil)
        fetchController.delegate = self
        return fetchController
    }()
    
    lazy var tableView: UITableView = {
        
        let tableView = UITableView()
        tableView.register(FeedMessageCell.self, forCellReuseIdentifier: self.cellId)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = .white
        tableView.separatorStyle = .none
        return tableView
    }()
    
    lazy var searchBar: UISearchBar = {
        let bar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.width, height: 48))
        bar.delegate = self
        bar.placeholder = "Поиск в сообщениях"
        
        let textFieldInsideUISearchBar = bar.value(forKey: "searchField") as? UITextField
        textFieldInsideUISearchBar?.font = AppFont.medium(size: 15)
        
        let textFieldInsideUISearchBarLabel = textFieldInsideUISearchBar!.value(forKey: "placeholderLabel") as? UILabel
        textFieldInsideUISearchBarLabel?.font = AppFont.light(size: 15)
        
        return bar
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .lightGray
        navigationItem.title = "Сообщения"
        
        clearCoreData()
        fillCoredataWithFriends()
        
        do {
            try fetchViewController.performFetch()
            print(fetchViewController.sections![0].numberOfObjects)
        } catch let error {
            print(error)
        }
        
        setupViews()
    }
    
    func setupViews() {
        
        view.addSubview(tableView)
        tableView.fillSuperview()
        
        tableView.tableHeaderView = searchBar
        tableView.tableFooterView = UIView()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension MessagesFeedController: UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchViewController.sections![0].objects?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! FeedMessageCell
        let friend = fetchViewController.object(at: indexPath) as? Friend
        cell.message = friend?.lastMessage
        return cell
    }
}

extension MessagesFeedController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let chatVC = MessagesChatController(collectionViewLayout: UICollectionViewFlowLayout())
        chatVC.friend = fetchViewController.object(at: indexPath) as? Friend
        navigationController?.pushViewController(chatVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

extension MessagesFeedController: NSFetchedResultsControllerDelegate {
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        if type == .update {
            blockOperations.append(BlockOperation.init(block: {
                self.tableView.reloadRows(at: [indexPath!], with: .automatic)
                self.tableView.endUpdates()
            }))
            
        } else if type == .move {
            blockOperations.append(BlockOperation.init(block: {
                self.tableView.deleteRows(at: [indexPath!], with: .automatic)
                self.tableView.insertRows(at: [newIndexPath!], with: .automatic)
                self.tableView.endUpdates()
            }))
        }
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        
        for operation in self.blockOperations {
            operation.start()
        }
    }
}

extension MessagesFeedController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        performFetchWithPredicate(usingText: searchText)
    }
    
    func performFetchWithPredicate(usingText searchText: String) {
        
        NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: nil)
        
        let predicate = searchText == "" ? nil : NSPredicate(format: "name = %@", searchText)
        
        fetchViewController.fetchRequest.predicate = predicate
        
        do {
            try fetchViewController.performFetch()
            tableView.reloadData()
        } catch let error {
            print(error.localizedDescription)
        }
    }
}
