//
//  ModelManager.swift
//  HudeemApp
//
//  Created by Admin on 20.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData
import PromiseKit

enum ResponseError: Error {
    case withMessage(String?)
}

struct AppErrors {
    
    static let noTrainPlans = "План тренировок еще не составлен"
    static let noMealPlans = "План питания еще не составлен"
    
    static let noInternetTrainPlans = "Для загрузки плана тренировок необходимо подключение к интернету"
    static let noInternetMealPlans = "Для загрузки плана питания необходимо подключение к интернету"
}

typealias MealPlanCompletion = (Int32?, NSManagedObjectContext?)
typealias TrainingPlanCompletion = (Int32?, NSManagedObjectContext?)

class ModelManager: NSObject {
    
    static let sharedManager = ModelManager.init()
    
    override init() {
        super.init()
        
        //uploadDataForEntireWeek()
    }
    
    func getDietPlan(forSelectedDate date: Date?, initial: Bool? = false) -> Promise<MealPlanCompletion> {
        
        return Promise(resolvers: { (success, reject) in
            
            guard let idString = date?.stringFromDate(), let id = Int32(idString.replacingOccurrences(of: ".", with: "")) else { return }
            
            let savedPlan: MealPlan? = MealPlan.attemptToFetch(withId: Int(id), context: AppDelegate.currentContext)
            
            if let plan = savedPlan, initial == false {
                success((plan.id, AppDelegate.currentContext))
                return
            }
                
            ApiService.sharedService.fetchDietPlan(forSelectedDate: date, completion: { (plan, context, error) in
                
                if error != nil {
                    if savedPlan != nil {
                        success((savedPlan?.id, context))
                    } else {
                        reject(ResponseError.withMessage(AppErrors.noInternetMealPlans))
                    }
                    return
                }
                
                if plan == nil {
                    reject(ResponseError.withMessage(AppErrors.noMealPlans))
                }
                if plan?.id == id {
                    success((plan?.id, context))
                }
            })
        })
    }
    
    func getReciept(forDishId dishId: Int32?, planId: Int32?, initial: Bool? = false) -> Promise<Int32?> {
        
        return Promise(resolvers: { (success, reject) in
            
            guard let id = dishId else { return }
            
            let savedReciept: Reciept? = Reciept.attemptToFetch(withId: Int(id), context: AppDelegate.currentContext)
            
            if let reciept = savedReciept, initial == false {
                success(reciept.id)
                return
            }
                
            ApiService.sharedService.fetchReciept(forDishId: dishId, planId: planId, completion: { (reciept, error) in
               
                if let error = error {
                    
                    if savedReciept != nil {
                        success(savedReciept?.id)
                    } else {
                        reject(error)
                    }
                    return
                }
                if reciept?.id == id {
                    success(reciept?.id)
                }
            })
        })
    }
    
    func getTrainingPlan(forSelectedDate date: Date?, initial: Bool? = false) -> Promise<TrainingPlanCompletion> {
        
        return Promise(resolvers: { (success, reject) in
            
            guard let idString = date?.stringFromDate(), let id = Int32(idString.replacingOccurrences(of: ".", with: "")) else { return }
            
            let savedPlan: TrainingPlan? = TrainingPlan.attemptToFetch(withId: Int(id), context: AppDelegate.currentContext)
            
            ApiService.sharedService.fetchTrainingPlan(forSelectedDate: date, completion: { (plan, context, error) in
                
                if error != nil {
                    if savedPlan != nil {
                        success((savedPlan?.id, context))
                    } else {
                        reject(ResponseError.withMessage(AppErrors.noInternetTrainPlans))
                    }
                    return
                }
                
                if plan == nil {
                    reject(ResponseError.withMessage(AppErrors.noTrainPlans))
                }
                if plan?.id == id {
                    success((plan?.id, context))
                }
            })
        })
    }
    
    func uploadMealsForEntireWeek() {
        
        for i in 0...7 {
            
            let date = Calendar.current.date(byAdding: .day, value: i, to: Date())
            
            ModelManager.sharedManager.getDietPlan(forSelectedDate: date, initial: true)
                .then { (mealPlanId, context) -> Void in
                    
                    let predicate = NSPredicate(format: "meal.mealPlan.id = %d", mealPlanId ?? 0)
                    let optDishes: [Dish]? = Dish.fetch(predicate: predicate, sortDescriptors: [], context: AppDelegate.currentContext)
                    
                    guard let dishes = optDishes else { return }
                    
                    for dish in dishes {
                        
                        CustomImageView.sharedInstance.saveImageInCashe(urlString: dish.photoUrl)
                        
                        ModelManager.sharedManager.getReciept(forDishId: dish.dishId, planId: dish.planId, initial: true)
                            .then { (recieptId) -> Void in
                                
                                let predicate = NSPredicate(format: "receipt.id = %d", recieptId ?? 0)
                                let optIngridients: [Ingredient]? = Ingredient.fetch(predicate: predicate, sortDescriptors: [], context: AppDelegate.currentContext)
                                
                                guard let ingridients = optIngridients else { return }
                                
                                for ingridient in ingridients {
                                    CustomImageView.sharedInstance.saveImageInCashe(urlString: ingridient.photoUrl)
                                }
                            }
                            .always {
                                
                                
                            }.catch { (error) in
                        }
                    }
                }
                .always {
                    
                    
                }.catch { (error) in
            }
        }
    }
    
    func uploadTrainingsForEntireWeek() {
        
        for i in 0...7 {
            
            let date = Calendar.current.date(byAdding: .day, value: i, to: Date())
            
            ModelManager.sharedManager.getDietPlan(forSelectedDate: date, initial: true)
                .then { (mealPlanId, context) -> Void in
                    
                }
                .always {
                    
                }.catch { (error) in
            }
        }
    }
    
    func uploadDataForEntireWeek() {
        
        uploadMealsForEntireWeek()
        uploadTrainingsForEntireWeek()
    }
}

