//
//  CookingAppliancesRecieptCell.swift
//  HudeemApp
//
//  Created by Admin on 06.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import TableKit

class CookingToolsCell: BaseTableCell, ConfigurableCell {
    
    let cellId = "cellId"
    
    static var estimatedHeight: CGFloat? {
        return 60
    }
    
    static var defaultHeight: CGFloat? {
        return UITableViewAutomaticDimension
    }
    
    let separator: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.8745098039, green: 0.8745098039, blue: 0.8745098039, alpha: 1)
        return view
    }()
    
    static let titleText = "Необходимые кухонные принадлежности:"
    
    var titleLabel: UILabel = {
        let label = UILabel()
        label.font = AppFont.medium(size: 15)
        label.text = CookingToolsCell.titleText
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    lazy var tableView: UITableView = {
        
        let tableView = UITableView()
        tableView.register(ApplianceCell.self, forCellReuseIdentifier: self.cellId)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = .white
        tableView.separatorStyle = .none
        tableView.isScrollEnabled = false
        tableView.allowsSelection = false
        return tableView
    }()
    
    var tools: [Tool]?
    
    var recieptId: Int32? {
        didSet {
            guard let id = recieptId else { return }
            
            if let reciept: Reciept = Reciept.attemptToFetch(withId: Int(id), context: AppDelegate.currentContext) {
                
                if let tools = reciept.tools {
                    self.tools = Array(tools) as? [Tool]
                    
                    tableHeightConstraint?.constant = CookingToolsCell.getTableViewHeight(reciept: reciept)
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
    
    func configure(with recieptId: Int32?) {
        
        self.recieptId = recieptId
    }
    
    var tableHeightConstraint: NSLayoutConstraint?
    
    override func setupViews() {
        
        addSubview(titleLabel)
        addSubview(tableView)
        
        addConstraintsWithFormat("H:|-20-[v0]-20-|", views: titleLabel)
        addConstraintsWithFormat("H:|-20-[v0]-20-|", views: tableView)
        addConstraintsWithFormat("V:|-25-[v0]-20-[v1]-15-|", views: titleLabel, tableView)
        
        tableHeightConstraint = tableView.heightAnchor.constraint(equalToConstant: 0)
        tableHeightConstraint?.isActive = true
        
        addSubview(separator)
        separator.anchor(nil, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: Screen.pixel)
    }
    
    static func getTableViewHeight(reciept: Reciept) -> CGFloat {
        
        guard let tools = reciept.tools else { return 0 }
        guard let toolsArray = Array(tools) as? [Tool] else { return 0 }
        if toolsArray.count <= 0 { return 0 }
        
        return CGFloat(toolsArray.count * 20)
    }
}

extension CookingToolsCell: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tools?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! ApplianceCell
        cell.cellLabel.text = tools?[indexPath.item].title
        return cell
    }
}

extension CookingToolsCell: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 20
    }
}

fileprivate class ApplianceCell : BaseTableCell {
    
    private let staticImageView: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "minus"))
        return imageView
    }()
    
    var cellLabel: UILabel = {
        let label = UILabel()
        label.font = AppFont.medium(size: 15)
        return label
    }()
    
    override func setupViews() {
        
        addSubview(staticImageView)
        addSubview(cellLabel)
        addConstraintsWithFormat("H:|[v0(18)]-13-[v1]|", views: staticImageView, cellLabel)
        staticImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        cellLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
}
