//
//  IngridientsCell.swift
//  HudeemApp
//
//  Created by Admin on 06.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import TableKit

class IngridientsCell: BaseTableCell, ConfigurableCell {

    let cellId = "cellId"
    static let titleText = "Ингридиенты (на порцию 300 г.):"
    
    static var estimatedHeight: CGFloat? {
        return 60
    }
    
    static var defaultHeight: CGFloat? {
        return UITableViewAutomaticDimension
    }
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = AppFont.medium(size: 15)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.text = ""
        return label
    }()
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 18
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.register(IngridientCell.self, forCellWithReuseIdentifier: self.cellId)
        cv.dataSource = self
        cv.delegate = self
        cv.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        cv.backgroundColor = .white
        return cv
    }()
    
    fileprivate var ingridients: [Ingredient]?
    
    var recieptId: Int32? {
        didSet {
        guard let id = recieptId else { return }
        
            if let reciept: Reciept = Reciept.attemptToFetch(withId: Int(id), context: AppDelegate.currentContext) {

                titleLabel.text = "Ингридиенты (на порцию \(reciept.size) г.):"
            
                if let ingridients = reciept.ingredients {
                    self.ingridients = Array(ingridients) as? [Ingredient]
                
                    collectionViewHeightConstraint?.constant = getCollectionViewHeight(reciept: reciept)
                
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }
            }
        }
    }
    
    func configure(with recieptId: Int32?) {        
        self.recieptId = recieptId
    }
    
    let separator: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.8745098039, green: 0.8745098039, blue: 0.8745098039, alpha: 1)
        return view
    }()
    
    var dishSize: Int?
    
    var collectionViewHeightConstraint: NSLayoutConstraint?
    
    override func setupViews() {
        
        addSubview(titleLabel)
        addSubview(collectionView)
        
        addConstraintsWithFormat("H:|-18-[v0]-18-|", views: titleLabel)
        addConstraintsWithFormat("H:|-18-[v0]-18-|", views: collectionView)
        addConstraintsWithFormat("V:|-25-[v0]-24-[v1]-5-|", views: titleLabel, collectionView)
        
        collectionViewHeightConstraint = collectionView.heightAnchor.constraint(equalToConstant: 0)
        collectionViewHeightConstraint?.isActive = true
        
        addSubview(separator)
        separator.anchor(nil, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: Screen.pixel)
    }
    
    
    func getCollectionViewHeight(reciept: Reciept) -> CGFloat {

        guard let ingredients = reciept.ingredients else { return 0 }
        guard let ingredientsArray = Array(ingredients) as? [Ingredient] else { return 0 }
        if ingredientsArray.count <= 0 { return 0 }
        
        let rowsNumber = ingredientsArray.count % 3 > 0 ? abs(ingredientsArray.count / 3) + 1 : ingredientsArray.count / 3
        
        let collectionHeight: CGFloat = CGFloat(rowsNumber) * (140 + 5)
        
        return collectionHeight
    }
}

extension IngridientsCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ingridients?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! IngridientCell
        let ingridient = ingridients?[indexPath.row]
        cell.ingridient = ingridient
        return cell
    }
}

extension IngridientsCell: UICollectionViewDelegate {
    
}

extension IngridientsCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 85, height: 130)
    }
}

fileprivate class IngridientCell: BaseCollectionCell {
    
    let imageView: CustomImageView = {
        let imageView = CustomImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 35
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.layer.borderWidth = 1
        imageView.layer.borderColor = UIColor.lightGray.cgColor
        return imageView
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = AppFont.medium(size: 12)
        label.numberOfLines = 3
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        return label
    }()
    
    let sizeLabel: UILabel = {
        let label = UILabel()
        label.font = AppFont.medium(size: 11)
        label.numberOfLines = 1
        label.lineBreakMode = .byWordWrapping
        label.textColor = purpleColor
        label.textAlignment = .center
        return label
    }()
    
    var ingridient: Ingredient? {
        didSet {
            imageView.loadImageUsingUrlString(ingridient?.photoUrl)
            titleLabel.text = ingridient?.title
            sizeLabel.text = "Вес: \(ingridient?.size ?? 0) г."
        }
    }
    
    override func setupViews() {
        
        addSubview(imageView)
        addSubview(titleLabel)
        addSubview(sizeLabel)
        
        addConstraintsWithFormat("V:|[v0(70)]-13-[v1]-13-[v2]", views: imageView, titleLabel, sizeLabel)
        imageView.widthAnchor.constraint(equalToConstant: 70).isActive = true
        imageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        addConstraintsWithFormat("H:|[v0]|", views: titleLabel)
        addConstraintsWithFormat("H:|[v0]|", views: sizeLabel)
    }    
}

