//
//  MealPlan+CoreDataClass.swift
//  HudeemApp
//
//  Created by Admin on 26.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData


public class MealPlan: NSManagedObject {

    static func createMealPlan(withData data: Data?, context: NSManagedObjectContext) -> MealPlan?  {
        
        guard let data = data else { return nil }
        
        do {
            if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject] {
                
                if let error = json["error"] {
                    print(error)
                    return nil
                }
                var id: Int?
                if let idString = json["total"]?["date"] as? NSString  {
                    id = Int(idString.replacingOccurrences(of: ".", with: ""))
                }

                let mealPlan: MealPlan = createOrReturn(withId: id, context: context)
                
                if let lastUpdated = json["total"]?["last_updated"] as? String, let lastUpdatedDate = lastUpdated.dateFromString() as NSDate? {
                    if mealPlan.lastUpdated != lastUpdatedDate {
                        mealPlan.initWithDict(dict: json, context: context)
                    }
                } else {
                    mealPlan.initWithDict(dict: json, context: context)
                }
                
                mealPlan.createAndAddRelationObjects(dict: json, context: context)
                return mealPlan

            }
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        return nil
    }
    
    func createAndAddRelationObjects(dict: [String: Any], context: NSManagedObjectContext) {
        
        if let planDict = dict["plan"] as? [[String: AnyObject]] {
            
            for mealDict in planDict {
                
                let meal = Meal.create(withDict: mealDict, context: context)
                meal.mealPlan = self
                
                if let dishesDict = mealDict["dishes"] as? [[String: AnyObject]] {
                    
                    for dishDict in dishesDict {
                        let dish = Dish.create(withDict: dishDict, context: context)
                        dish.meal = meal
                    }
                }
            }
        }
        
        NSManagedObject.attemptToSave(context: context)
    }
    
    func removeOldConntections(context: NSManagedObjectContext) {
        
        let dishes: [Dish]? = Dish.fetch(context: context)
        if let dishes = dishes {
            for dish in dishes {
                dish.meal = nil
            }
        }
        
        let meals: [Meal]? = Meal.fetch(context: context)
        if let meals = meals {
            for meal in meals {
                meal.mealPlan = nil
            }
        }
        
        NSManagedObject.attemptToSave(context: context)
    }
    
    func initWithDict(dict: [String: Any], context: NSManagedObjectContext) {
        
        if let total = dict["total"] as? [String: AnyObject] {
            
            if let proteins = total["proteins"] as? Int32 {
                self.proteins = proteins
            }
            if let fats = total["fats"] as? Int32 {
                self.fats = fats
            }
            if let carbohydrates = total["carbohydrates"] as? Int32 {
                self.carbohydrates = carbohydrates
            }
            if let calories = total["calories"] as? Int32 {
                self.calories = calories
            }
            if let percProteins = total["proc_proteins"] as? Float {
                self.percProteins = percProteins
            }
            if let percFats = total["proc_fats"] as? Float {
                self.percFats = percFats
            }
            if let percCarbohydrates = total["proc_carbohydrates"] as? Float {
                self.percCarbohydrates = percCarbohydrates
            }
            if let idString = total["date"] as? String, let id = idString.dateFromString() {
                self.date = id as NSDate?
            }
            if let idString = total["date"] as? NSString, let id = Int32(idString.replacingOccurrences(of: ".", with: "")) {
                self.id = id
            }
            if let lastUpdated = dict["last_updated"] as? String {
                self.lastUpdated = lastUpdated.dateFromString() as NSDate?
            }
        }
        
        NSManagedObject.attemptToSave(context: context)
    }
}
