//
//  StoredImage+CoreDataProperties.swift
//  HudeemApp
//
//  Created by Admin on 27.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData


extension StoredImage {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<StoredImage> {
        return NSFetchRequest<StoredImage>(entityName: "StoredImage")
    }

    @NSManaged public var id: String?
    @NSManaged public var imageData: NSData?

}
