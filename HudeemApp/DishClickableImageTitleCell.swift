//
//  DishClickableImageTitle.swift
//  HudeemApp
//
//  Created by Admin on 03.05.17.
//  Copyright © 2017 Admin. All rights reserved.
//


import TableKit

class DishClickableImageTitleCell: BaseTableCell, ConfigurableCell {
    
    lazy var dishImageView: CustomImageView = {
        let imageView = CustomImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showRecieptHandler)))
        return imageView
    }()
    
    lazy var dishTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.isUserInteractionEnabled = true
        label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showRecieptHandler)))
        return label
    }()
    
    lazy var showRecieptButton: UIButton = {
        let button = UIButton(type: .system)
        let yourAttributes : [String: Any] =
            [NSFontAttributeName : DishCellSizes.recieptTitleFont,
             NSForegroundColorAttributeName : purpleColor,
             NSUnderlineStyleAttributeName : NSUnderlineStyle.styleSingle.rawValue]
        let attributeString = NSMutableAttributedString(string: "Смотреть рецепт", attributes: yourAttributes)
        button.setAttributedTitle(attributeString, for: .normal)
        button.addTarget(self, action: #selector(showRecieptHandler), for: .touchUpInside)
        return button
    }()

    
    static var estimatedHeight: CGFloat? {
        return 20
    }
    
    static var defaultHeight: CGFloat? {
        return UITableViewAutomaticDimension
    }
    
    func configure(with data:(imageUrl: String?, dishTitle: String?) ) {
        
        dishImageView.loadImageUsingUrlString(data.imageUrl)
        
        dishTitleLabel.attributedText = NSAttributedString(string: "\(data.dishTitle ?? "")",
            attributes:
            [NSFontAttributeName : DishCellSizes.dishTitleFont,
             NSForegroundColorAttributeName : UIColor.black,
             NSUnderlineStyleAttributeName : NSUnderlineStyle.styleSingle.rawValue,
             NSUnderlineColorAttributeName : purpleColor])
    }
    
    override func setupViews() {
        
        addSubview(dishImageView)
        dishImageView.anchor(topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 20, leftConstant: 18, bottomConstant: 0, rightConstant: 18, widthConstant: 0, heightConstant: 250)
        
        addSubview(dishTitleLabel)
        dishTitleLabel.anchor(dishImageView.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 20, leftConstant: 18, bottomConstant: 0, rightConstant: 18, widthConstant: 0, heightConstant: 0)
        
        addSubview(showRecieptButton)
        showRecieptButton.anchor(dishTitleLabel.bottomAnchor, left: leftAnchor, bottom: bottomAnchor, right: nil, topConstant: 0, leftConstant: 18, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    func showRecieptHandler() {
        TableCellAction(key: DishCellActions.openRecieptClicked, sender: self).invoke()
    }
    
    
}
