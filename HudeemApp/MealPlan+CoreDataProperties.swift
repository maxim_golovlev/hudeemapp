//
//  MealPlan+CoreDataProperties.swift
//  HudeemApp
//
//  Created by Admin on 19.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData


extension MealPlan {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MealPlan> {
        return NSFetchRequest<MealPlan>(entityName: "MealPlan")
    }

    @NSManaged public var calories: Int32
    @NSManaged public var carbohydrates: Int32
    @NSManaged public var date: NSDate?
    @NSManaged public var fats: Int32
    @NSManaged public var id: Int32
    @NSManaged public var percCarbohydrates: Float
    @NSManaged public var percFats: Float
    @NSManaged public var percProteins: Float
    @NSManaged public var proteins: Int32
    @NSManaged public var lastUpdated: NSDate?
    @NSManaged public var meals: NSSet?

}

// MARK: Generated accessors for meals
extension MealPlan {

    @objc(addMealsObject:)
    @NSManaged public func addToMeals(_ value: Meal)

    @objc(removeMealsObject:)
    @NSManaged public func removeFromMeals(_ value: Meal)

    @objc(addMeals:)
    @NSManaged public func addToMeals(_ values: NSSet)

    @objc(removeMeals:)
    @NSManaged public func removeFromMeals(_ values: NSSet)

}
