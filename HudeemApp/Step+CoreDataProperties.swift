//
//  Step+CoreDataProperties.swift
//  HudeemApp
//
//  Created by Admin on 07.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData


extension Step {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Step> {
        return NSFetchRequest<Step>(entityName: "Step")
    }

    @NSManaged public var id: Int32
    @NSManaged public var text: String?
    @NSManaged public var imageUrl: String?
    @NSManaged public var receipt: Reciept?

}
