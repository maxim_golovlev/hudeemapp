//
//  ExerciseParams+CoreDataProperties.swift
//  HudeemApp
//
//  Created by Admin on 29.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData


extension ExerciseParams {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ExerciseParams> {
        return NSFetchRequest<ExerciseParams>(entityName: "ExerciseParams")
    }

    @NSManaged public var count: Int32
    @NSManaged public var time: Int32
    @NSManaged public var weight: Int32
    @NSManaged public var id: String?
    @NSManaged public var sort: Int32
    @NSManaged public var exercise: Exercise?

}
