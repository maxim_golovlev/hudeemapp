//
//  ImageCell.swift
//  HudeemApp
//
//  Created by Admin on 30.04.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import TableKit

class ImageCell: BaseTableCell, ConfigurableCell {
    
    lazy var exerciseImageView: CustomImageView = {
        let imageView = CustomImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let separator: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        return view
    }()
    
    static var estimatedHeight: CGFloat? {
        return 60
    }
    
    static var defaultHeight: CGFloat? {
        return UITableViewAutomaticDimension
    }
    
    var imageHeightConstraint: NSLayoutConstraint?
    
    func configure(with data: (imageUrl: String?, image: UIImage?, withSeparator: Bool?, height: CGFloat?)) {
        
        if let url = data.imageUrl {
            exerciseImageView.loadImageUsingUrlString(url)
            
        } else if let image = data.image {
            exerciseImageView.image = image
        }
        
        if data.withSeparator == true {
            addSubview(separator)
            separator.anchor(nil, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: Screen.pixel)
        }
        
        if let height = data.height {
            
            imageHeightConstraint?.constant = height

        }
    }

    
    override func setupViews() {
        
        addSubview(exerciseImageView)
        exerciseImageView.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 12, leftConstant: 21, bottomConstant: 12, rightConstant: 21, widthConstant: 0, heightConstant: 0)
        
        imageHeightConstraint = exerciseImageView.heightAnchor.constraint(equalToConstant: 225)
        imageHeightConstraint?.isActive = true
    }
}
