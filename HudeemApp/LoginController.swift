//
//  LoginController.swift
//  HudeemApp
//
//  Created by Admin on 24.03.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import ActiveLabel

class LoginController: UIViewController {

    let imageView: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "app_header"))
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        return imageView
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "     Вход в личный кабинет"
        let font = AppFont.medium(size: 17)
        label.font = font
        return label
    }()
    
    let emailTextField: RoundTextFeld = {
        let tf = RoundTextFeld(frame: .zero)
        tf.font = AppFont.light(size: 16)
        tf.placeholder = "E-mail"
        return tf
    }()
    
    let passTextField: RoundTextFeld = {
        let tf = RoundTextFeld(frame: .zero)
        tf.placeholder = "Пароль"
        tf.font = AppFont.light(size: 16)
        tf.isSecureTextEntry = true
        return tf
    }()
    
    lazy var enterButton: RoundedButton = {
        let button = RoundedButton(type: .system)
        button.backgroundColor = turquoise
        button.layer.cornerRadius = 55/2
        button.setTitle("Войти", for: .normal)
        button.titleLabel?.font = AppFont.medium(size: 19)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(enterHandler), for: .touchUpInside)
        return button
    }()
    
    lazy var restorePassButton: UIButton = {
        let button = UIButton(type: .system)
        let string = NSAttributedString(string: "Забыли пароль?",
                                        attributes: [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue,
                                                     NSForegroundColorAttributeName: turquoise,
                                                     NSFontAttributeName: AppFont.roman(size: 15)])
        button.setAttributedTitle(string, for: .normal)
        button.addTarget(self, action: #selector(restorePasHandler), for: .touchUpInside)
        return button
    }()
    
  /*  let questionLabel: ActiveLabel = {
        let label = ActiveLabel()
        label.text = ""
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = AppFont.medium(size: 22)
        return label
    }()*/
    
    let discriptionLabel: ActiveLabel = {
        let label = ActiveLabel()
        
        let boldTitle = "Еще не начали худеть с нашим сервисом?"
        
        let customType = ActiveType.custom(pattern: "^\(boldTitle)\\b")
        
      /*  let attributeTitle = NSMutableAttributedString()
        attributeTitle.append(NSAttributedString(string: "Еще не начали худеть с нашим сервисом?\n", attributes: [NSFontAttributeName: AppFont.medium(size: 22)]))
        attributeTitle.append(NSAttributedString(string: "Тогда для начала, вам нужно зарегистрироваться и заполнить анкету на сайте \nwww.hudeem-s-profi.com \n", attributes: [NSFontAttributeName: AppFont.light(size: 22)]))*/
        
        
        
    //    label.attributedText = attributeTitle
        label.text = "Еще не начали худеть с нашим сервисом?\n Тогда для начала, вам нужно зарегистрироваться и заполнить анкету на сайте \nwww.hudeem-s-profi.com \n"
        label.enabledTypes = [.url, customType, .mention, .hashtag]
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = .center
        label.URLColor = AppColors.purpleColor
        label.font = AppFont.light(size: 22)
        label.handleURLTap({ (url) in
            print("url tapped")
        })
        label.configureLinkAttribute = { (type, attributes, isSelected) in
            var atts = attributes
            switch type {
            case .custom:
                atts[NSFontAttributeName] = AppFont.medium(size: 22)
            default: ()
            }
            
            return atts
        }
        return label
    }()
    
    lazy var regButton: RoundedButton = {
        let button = RoundedButton(type: .system)
        button.backgroundColor = purpleColor
        button.layer.cornerRadius = 55/2
        button.setTitle("Зарегистрироваться сейчас", for: .normal)
        button.titleLabel?.font = .boldSystemFont(ofSize: 18)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(regHandler), for: .touchUpInside)
        return button
    }()
    
    lazy var supportButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Написать в тех.поддержку", for: .normal)
        button.setTitleColor(turquoise, for: .normal)
        button.addTarget(self, action: #selector(restorePasHandler), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        let separator = UIView()
        separator.backgroundColor = .lightGray
        separator.translatesAutoresizingMaskIntoConstraints = false
        button.addSubview(separator)
        separator.anchor(button.topAnchor, left: button.leftAnchor, bottom: nil, right: button.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 1)
        
        return button
    }()
    
    let verticalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        //stackView.alignment = .center
        stackView.distribution = .equalSpacing
        stackView.spacing = 0
        stackView.backgroundColor = .green
        return stackView
    }()
    
    let horizontalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.spacing = 20
        return stackView
    }()
    
    let scrollView: UIScrollView = {
        let sv = UIScrollView(frame: .zero)
        return sv
    }()
    
    func spacerView(_ height: CGFloat) -> UIView {
        let spacerView = UIView()
        spacerView.heightAnchor.constraint(equalToConstant: height).isActive = true
        return spacerView
    }
    
    
    var dismissCompletion: (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        UIApplication.shared.statusBarStyle = .default
        
        view.addSubview(scrollView)
        scrollView.fillSuperview()
        
        scrollView.addSubview(verticalStackView)
        verticalStackView.anchor(scrollView.topAnchor, left: scrollView.leftAnchor, bottom: scrollView.bottomAnchor, right: scrollView.rightAnchor, topConstant: Screen.navbarHeight, leftConstant: 20, bottomConstant: 0, rightConstant: -40, widthConstant: 0, heightConstant: 0)
        verticalStackView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -40).isActive = true

        verticalStackView.addArrangedSubview(imageView)
        imageView.heightAnchor.constraint(equalToConstant: 201/2).isActive = true
        
        verticalStackView.addArrangedSubview(spacerView(25))
        
        verticalStackView.addArrangedSubview(titleLabel)
        titleLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        verticalStackView.addArrangedSubview(spacerView(30))
        
        verticalStackView.addArrangedSubview(emailTextField)
        emailTextField.heightAnchor.constraint(equalToConstant: 55).isActive = true
        
        verticalStackView.addArrangedSubview(spacerView(20))
        
        verticalStackView.addArrangedSubview(passTextField)
        passTextField.heightAnchor.constraint(equalToConstant: 55).isActive = true
        
        verticalStackView.addArrangedSubview(spacerView(20))
        
        verticalStackView.addArrangedSubview(horizontalStackView)
        horizontalStackView.heightAnchor.constraint(equalToConstant: 55).isActive = true
        horizontalStackView.addArrangedSubview(enterButton)
        horizontalStackView.addArrangedSubview(restorePassButton)
        
        verticalStackView.addArrangedSubview(spacerView(33))
        
     //   verticalStackView.addArrangedSubview(questionLabel)
       // questionLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        verticalStackView.addArrangedSubview(discriptionLabel)
        
    //    verticalStackView.addArrangedSubview(regButton)
    //    regButton.heightAnchor.constraint(equalToConstant: 55).isActive = true
        
    //    let spacerView2 = UIView()
    //    verticalStackView.addArrangedSubview(spacerView2)
    //    spacerView2.heightAnchor.constraint(equalToConstant: 55).isActive = true

    //    verticalStackView.addArrangedSubview(supportButton)
    //    supportButton.heightAnchor.constraint(equalToConstant: 55).isActive = true
    }

}
